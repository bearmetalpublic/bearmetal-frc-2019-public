/*
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.simulation;

import edu.wpi.first.networktables.EntryListenerFlags;
import edu.wpi.first.networktables.EntryNotification;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.tahomarobotics.sim.model.*;

import java.util.List;
import java.util.function.Consumer;

public class ChassisModel {


    private static final double FIRST_STAGE = 40d/11d;
    private static final double SECOND_STAGE = 42d/24d;
    private static final double GEAR_RATIO  = FIRST_STAGE * SECOND_STAGE;

    private static final double METERS_PER_INCH = 0.0254;
    private static final double KILOGRAM_PER_LBMASS = 0.453592;
    private static final double WHEEL_RADIUS = 2.0 * METERS_PER_INCH;
    private static final double TRACK_HALF_WIDTH = 12.5       * METERS_PER_INCH;
    public static final double MASS = 117.9 * KILOGRAM_PER_LBMASS;
    private static final double INTERIA = 20779.439  * KILOGRAM_PER_LBMASS * METERS_PER_INCH * METERS_PER_INCH;

    private final Motor leftRearMotor = Motor.createNeo();
    private final Motor leftFrontMotor = Motor.createNeo();
    private final Motor rightRearMotor = Motor.createNeo();
    private final Motor rightFrontMotor = Motor.createNeo();

    private final Encoder leftMotorEncoder = new Encoder();
    private final Encoder rightMotorEncoder = new Encoder();

    private final Transmission leftDrive = new Transmission(new Motor[] { leftRearMotor, leftFrontMotor },
            false, GEAR_RATIO, GEAR_RATIO);
    private final Transmission rightDrive = new Transmission(new Motor[] { rightRearMotor, rightFrontMotor },
            true, GEAR_RATIO, GEAR_RATIO);

    private final Solenoid solenoid = new Solenoid();

    private final IMU imu = new IMU();

    private double x;
    private double y;
    private double heading;
    private double fwdVelocity;
    private double rotVelocity;

    private final double[] pose = new double[3];

    public ChassisModel() {

        // this is the only coupling to the Simulation
        SmartDashboard.getEntry("SimResetRobotPoseCommand").addListener(new Consumer<EntryNotification>() {

            private double[] reset = new double[3];

            @Override
            public void accept(EntryNotification t) {
                NetworkTableEntry entry = t.getEntry();
                reset = entry.getDoubleArray(reset);

                // reset the robot pose
                x = reset[0] * 0.0254;
                y = reset[1] * 0.0254;
                heading = Math.toRadians(reset[2]);
            }

        }, EntryListenerFlags.kNew | EntryListenerFlags.kUpdate | EntryListenerFlags.kLocal);

    }

    public void initialize(SimRobot sim) {

        sim.registerMotor(SimModelRobotMap.LEFT_REAR_MOTOR, leftRearMotor);
        sim.registerMotor(SimModelRobotMap.LEFT_FRONT_MOTOR, leftFrontMotor);
        sim.registerMotor(SimModelRobotMap.RIGHT_REAR_MOTOR, rightRearMotor);
        sim.registerMotor(SimModelRobotMap.RIGHT_FRONT_MOTOR, rightFrontMotor);

        sim.registerEncoder(SimModelRobotMap.LEFT_FRONT_MOTOR, leftMotorEncoder);
        sim.registerEncoder(SimModelRobotMap.LEFT_REAR_MOTOR, leftMotorEncoder);

        sim.registerEncoder(SimModelRobotMap.RIGHT_FRONT_MOTOR, rightMotorEncoder);
        sim.registerEncoder(SimModelRobotMap.RIGHT_REAR_MOTOR, rightMotorEncoder);

        sim.registerImu(SimModelRobotMap.PIGEON_IMU, imu);

        leftDrive.shift(Transmission.Shift.LOW);
        rightDrive.shift(Transmission.Shift.LOW);
    }

    public void resetPosition(double x, double y, double heading) {
        this.x = x;
        this.y = y;
        this.heading = heading;

        // send Reset position command in case Robot is listening
        SmartDashboard.putNumberArray("ResetRobotPoseCommand",
                new double[] {x / 0.0254, y / 0.0254, Math.toDegrees(heading)});

    }

    public void update(double time, double dT) {

        // shift transmission
        Transmission.Shift shift = solenoid.isExtended() ? Transmission.Shift.HIGH : Transmission.Shift.LOW;
        leftDrive.shift(shift);
        rightDrive.shift(shift);

        // update transmission/motor models
        leftDrive.update();
        rightDrive.update();

        // determine forces
        double leftForce = leftDrive.getTorque() / WHEEL_RADIUS;
        double rightForce = rightDrive.getTorque() / WHEEL_RADIUS;
        double fwdForce = rightForce + leftForce;
        double rotTorque = (rightForce - leftForce) * TRACK_HALF_WIDTH;

        // access acceleration
        double fwdAccel = fwdForce / MASS;
        double rotAccel = rotTorque / INTERIA;

        // integrate to velocity
        fwdVelocity += fwdAccel * dT;
        rotVelocity += rotAccel * dT;

        //Fill IMU Z Rotation velocity
        imu.setZRotationalVelocity(rotVelocity);

        // integrate to heading
        double deltaHeading = rotVelocity * dT;
        double averageHeading = heading + deltaHeading / 2;
        heading += deltaHeading;

        // integrate position
        double fwdDeltaDistance = fwdVelocity * dT;
        x +=  fwdDeltaDistance * Math.cos(averageHeading);
        y +=  fwdDeltaDistance * Math.sin(averageHeading);

        // calculate wheel velocities
        double deltaVelocity = rotVelocity * TRACK_HALF_WIDTH;
        double leftVelocity  = (fwdVelocity - deltaVelocity) / WHEEL_RADIUS;
        double rightVelocity = (fwdVelocity + deltaVelocity) / WHEEL_RADIUS;

        leftDrive.setVelocity(leftVelocity);
        rightDrive.setVelocity(rightVelocity);

        leftMotorEncoder.setVelocity(leftVelocity * GEAR_RATIO);
        rightMotorEncoder.setVelocity(rightVelocity * GEAR_RATIO);

        getPose(pose);
        SmartDashboard.putNumberArray("SimRobotPose", pose);
    }

    public void getPose(double[] pose) {
        pose[0] = x;
        pose[1] = y;
        pose[2] = heading;
    }
}
