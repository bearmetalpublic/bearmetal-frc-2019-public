/*
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.simulation;

import org.tahomarobotics.sim.model.ModelIF;
import org.tahomarobotics.sim.model.SimRobot;

import java.util.List;

public class Model implements ModelIF {

    private final java.util.List<ModelIF.PositionPreset> presets = new java.util.ArrayList<>();

    private final ChassisModel chassisModel = new ChassisModel();
    private final UtilityArmModel utilityArmModel = new UtilityArmModel();
    private final BetaLiftModel betaLiftModel = new BetaLiftModel();
    private final AlphaArmModel alphaArmModel = new AlphaArmModel();
    private final AlphaLiftModel alphaLiftModel = new AlphaLiftModel();

    public Model() {
        presets.add(new ModelIF.PositionPreset("Blue Far Right",  65.5,  118, 0.0 ));
        presets.add(new ModelIF.PositionPreset("Blue Close Right",  66.5, 150.125,   0.0 ));
        presets.add(new ModelIF.PositionPreset("Blue Close Left",  66.5, 171.865, 0.0 ));
        presets.add(new ModelIF.PositionPreset("Blue Far Left", 66.5,  203.990,   0.0 ));
        presets.add(new ModelIF.PositionPreset("Red Far Left", 648 - 66.5, 118, 180.0 ));
        presets.add(new ModelIF.PositionPreset("Red Close Left", 648 - 66.5, 150.125,   180.0 ));
        presets.add(new ModelIF.PositionPreset("Red Close Right", 648 - 66.5, 171.865, 180.0 ));
        presets.add(new ModelIF.PositionPreset("Red Far Right", 648 - 66.5, 203.990,   180.0 ));
    }

    @Override
    public void initialize(SimRobot sim) {
        chassisModel.initialize(sim);
        utilityArmModel.initialize(sim);
        betaLiftModel.initialize(sim);
        alphaArmModel.initialize(sim);
        alphaLiftModel.initialize(sim);
    }

    @Override
    public boolean isConnected() {
        return true;
    }

    @Override
    public void resetPosition(double x, double y, double heading) {
       chassisModel.resetPosition(x, y, heading);
    }

    @Override
    public void setEnabled(boolean enabled) {
    }

    @Override
    public void update(double time, double dT) {
        chassisModel.update(time, dT);
        utilityArmModel.update(time, dT);
        betaLiftModel.update(time, dT);
        alphaArmModel.update(time, dT);
        alphaLiftModel.update(time, dT);
    }

    @Override
    public String getName() {
        return "2019 Bear-Metal Robot";
    }

    @Override
    public void getPose(double[] pose) {
       chassisModel.getPose(pose);
    }

    @Override
    public List<PositionPreset> getPositionPresets() {
        return presets;
    }

}
