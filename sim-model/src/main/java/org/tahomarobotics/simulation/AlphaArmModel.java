/*
 * Copyright 2019 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.simulation;

import org.tahomarobotics.sim.model.Encoder;
import org.tahomarobotics.sim.model.Motor;
import org.tahomarobotics.sim.model.SimRobot;
import org.tahomarobotics.sim.model.Transmission;

public class AlphaArmModel {

    private static final double MIN_ANGLE = Math.toRadians(-55);
    private static final double MAX_ANGLE = Math.toRadians(100);

    private static final double ACCEL_OF_GRAVITY = 9.80665;
    private static final double MASS = 10.0 * 0.45359237;
    private static final double ARM_LENGHT = 12 * 0.0254;
    private static final double GRAVITY_TORQUE = MASS * ACCEL_OF_GRAVITY * ARM_LENGHT;
    private static final double INTERIA = MASS * ARM_LENGHT * ARM_LENGHT;

    private static final double FIRST_STAGE = 54D/10D;
    private static final double SECOND_STAGE = 72D/24D;
    private static final double THIRD_STAGE = 60D/24D;
    private static final double GEAR_RATIO = FIRST_STAGE * SECOND_STAGE * THIRD_STAGE;

    private final Motor motor = Motor.createNeo();
    private final Transmission drive  = new Transmission(new Motor[] { motor }, true, GEAR_RATIO, GEAR_RATIO);
    private final Encoder neoEncoder = new Encoder(-GEAR_RATIO);
    private final Encoder magEncoder = new Encoder();

    public void initialize(SimRobot sim) {
        sim.registerMotor(SimModelRobotMap.ALPHA_ARM_MOTOR, motor);
        sim.registerEncoder(SimModelRobotMap.ALPHA_ARM_MOTOR, neoEncoder);
        //sim.registerEncoder(SimModelRobotMap.ALPHA_ARM_MAG_DIO, magEncoder);

        drive.shift(Transmission.Shift.LOW);
        drive.setPosition(MAX_ANGLE);
    }

    public void update(double time, double dT) {

        drive.update();
        double torque = drive.getTorque();

        double position = drive.getPosition();
        double velocity = drive.getVelocity();

        double acceleration = (torque - GRAVITY_TORQUE * Math.cos(position)) / INTERIA;

        // integrate to velocity
        velocity += acceleration * dT;

        // integrate position
        position += velocity * dT;

        // limit motion to hard stops
        if (position > MAX_ANGLE) {
            position = MAX_ANGLE;
            velocity = 0;
        } else if (position < MIN_ANGLE) {
            position = MIN_ANGLE;
            velocity = 0;
        }

        // update drive
        drive.setVelocity(velocity);
        drive.setPosition(position);

        // feed back neo motor encoder
        neoEncoder.setVelocity(velocity);
        neoEncoder.setPosition(position);
        //if (Math.abs(velocity) > 0.01) {
         //   System.out.format("%8.3f %8.3f \n", Math.toDegrees(position), Math.toDegrees(velocity));
       // }
        // feed back magnetic encoder
        magEncoder.setVelocity(velocity);
        magEncoder.setPosition(position);
    }

}
