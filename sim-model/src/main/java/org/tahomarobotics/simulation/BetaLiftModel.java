/*
 * Copyright 2019 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.simulation;

import org.tahomarobotics.sim.model.Encoder;
import org.tahomarobotics.sim.model.Motor;
import org.tahomarobotics.sim.model.SimRobot;
import org.tahomarobotics.sim.model.Transmission;

public class BetaLiftModel {

    private static final double INCHES_TO_METERS = 0.0254;
    private static final double LBMASS_TO_KGS = 0.453592;

    private static final double CABLE_WINDER_RADIUS = 1.2 / 2.0 * INCHES_TO_METERS;
    private static final double LIFT_RANGE = (24.005 - 2.130) * INCHES_TO_METERS;
    private static final double CONTACT_DISTANCE = 2.788 * INCHES_TO_METERS;

    private static final double FIRST_STAGE = 52.0 / 10.0;
    private static final double SECOND_STAGE = 56.0 / 24.0;
    private static final double GEAR_RATIO = FIRST_STAGE * SECOND_STAGE;

    private static final double GRAVITY_ACCELERATION = 9.80665;
    private static final double BETA_LIFT_MASS = 1.378 * LBMASS_TO_KGS;
    private static final double BETA_FULL_LIFT_MASS = ChassisModel.MASS / 2.0 - BETA_LIFT_MASS;
    private static final double WEIGHT = ChassisModel.MASS / 2.0 * GRAVITY_ACCELERATION;

    private final Motor topMotor = Motor.createNeo();
    private final Motor bottomMotor = Motor.createNeo();
    private final Motor rearMotor = Motor.createNeo();

    private final Encoder winderEncoder = new Encoder();
    private final Encoder motorEncoder = new Encoder(GEAR_RATIO);

    private final Transmission transmission = new Transmission(new Motor[] { topMotor, bottomMotor, rearMotor }, false,  GEAR_RATIO, GEAR_RATIO);

    private final static double MIN_POSITION = 0;
    private final static double MAX_POSITION =  LIFT_RANGE / CABLE_WINDER_RADIUS;

    public void initialize(SimRobot sim) {

        sim.registerMotor(SimModelRobotMap.TOP_BETA_LIFT_MOTOR, topMotor);
        sim.registerMotor(SimModelRobotMap.BOTTOM_BETA_LIFT_MOTOR, bottomMotor);
        sim.registerMotor(SimModelRobotMap.REAR_BETA_LIFT_MOTOR, rearMotor);

        sim.registerEncoder(SimModelRobotMap.TOP_BETA_LIFT_MOTOR, motorEncoder);
        sim.registerEncoder(SimModelRobotMap.BOTTOM_BETA_LIFT_MOTOR, motorEncoder);
        sim.registerEncoder(SimModelRobotMap.REAR_BETA_LIFT_MOTOR, motorEncoder);

        sim.registerEncoder(SimModelRobotMap.BETA_LIFT_ENCODER, winderEncoder);

        transmission.shift(Transmission.Shift.LOW);
    }

    public void update(double time, double dT) {

        // update transmission/motor models
        transmission.update();


        double cableForce = transmission.getTorque() / CABLE_WINDER_RADIUS;

        double position = transmission.getPosition();
        double velocity = transmission.getVelocity();

        // angular acceleration = N / kg / m = kg m/sec2 / kg / m = rad/sec2
        //double verticalAcceleration = position < CONTACT_DISTANCE ? cableForce / BETA_LIFT_MASS + GRAVITY_ACCELERATION : cableForce / BETA_FULL_LIFT_MASS - GRAVITY_ACCELERATION;
        double verticalAcceleration = cableForce / BETA_FULL_LIFT_MASS - GRAVITY_ACCELERATION;
        double angularAcceleration = verticalAcceleration  / CABLE_WINDER_RADIUS;

        velocity += angularAcceleration * dT;
        position += velocity * dT;

        if (position < MIN_POSITION) {
            position = MIN_POSITION;
            velocity = 0;
        } else if (position > MAX_POSITION) {
            position = MAX_POSITION;
            velocity = 0;
        }

        transmission.setPosition(position);
        transmission.setVelocity(velocity);

        winderEncoder.setPosition(position);
        winderEncoder.setVelocity(velocity);

        motorEncoder.setPosition(position);
        motorEncoder.setVelocity(velocity);

      //  if (Math.abs(cableForce) > 0.001) {
       //     System.out.format("bat:%8.3f cur:%8.3f force:%8.3f pos:%8.3f vel:%8.3f\n", SimRobot.getInstance().getBatteryVoltage(), SimRobot.getInstance().getBatteryCurrent(), cableForce, position, velocity);
        //}
    }

}
