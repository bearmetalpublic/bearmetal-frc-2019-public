/*
 * Copyright 2019 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.simulation;

import org.tahomarobotics.sim.model.Encoder;
import org.tahomarobotics.sim.model.Motor;
import org.tahomarobotics.sim.model.SimRobot;
import org.tahomarobotics.sim.model.Transmission;

public class    UtilityArmModel {

    private static final double INCHES_TO_METERS = 0.0254;
    private static final double ARM_BEND_ANGLE = Math.toRadians(21.77);
    private static final double VERTICAL_ANGLE = ARM_BEND_ANGLE - Math.toRadians(90);
    private static final double PLATFORM_HEIGHT = INCHES_TO_METERS * 19.0;
    private static final double PIVOT_HEIGHT = INCHES_TO_METERS * 10.675;
    private static final double WHEEL_RADIUS = INCHES_TO_METERS * 1.25;
    private static final double ARM_HEIGHT = PLATFORM_HEIGHT - PIVOT_HEIGHT + WHEEL_RADIUS;
    private static final double ARM_LENGTH = INCHES_TO_METERS* 12.106;
    private static final double ACCEL_OF_GRAVITY = 9.80665;


    private static final double MASS = 5.903 * 0.45359237;
    private static final double ARM_RADIUS = 9.0 * 0.0254;
    private static final double ARM_INTERIA = MASS * ARM_RADIUS * ARM_RADIUS;
    private static final double CG_ANGLE_OFFSET = Math.toRadians(17.0);
    private static final double FLOOR_IMPACT_ANGLE = Math.toRadians(188);
    private static final double HAB_IMPACT_ANGLE = calculateAngle(0);
    private static final double HAB_IMPACT_OFFSET = Math.toRadians(45);

    private static final double MIN_ANGLE = HAB_IMPACT_ANGLE;
    private static final double MAX_ANGLE = Math.toRadians(210);

    private static final double FIRST_STAGE = 60/10D;
    private static final double SECOND_STAGE = 56/24D;
    private static final double THIRD_STAGE = 60/16D;
    private static final double GEAR_RATIO = FIRST_STAGE * SECOND_STAGE * THIRD_STAGE;

    private final Motor leftMotor = Motor.createNeo();
    private final Motor rightMotor = Motor.createNeo();

    private final Transmission leftDrive  = new Transmission(new Motor[] { leftMotor  }, true, GEAR_RATIO, GEAR_RATIO);
    private final Transmission rightDrive = new Transmission(new Motor[] { rightMotor }, false,  GEAR_RATIO, GEAR_RATIO);

    private final Encoder leftEncoder = new Encoder(-GEAR_RATIO);
    private final Encoder rightEncoder = new Encoder(GEAR_RATIO);

    public void initialize(SimRobot sim) {
        sim.registerMotor(SimModelRobotMap.LEFT_UTILITY_ARM_MOTOR, leftMotor);
        sim.registerMotor(SimModelRobotMap.RIGHT_UTILITY_ARM_MOTOR, rightMotor);

        sim.registerEncoder(SimModelRobotMap.LEFT_UTILITY_ARM_MOTOR, leftEncoder);
        sim.registerEncoder(SimModelRobotMap.RIGHT_UTILITY_ARM_MOTOR, rightEncoder);

        leftDrive.shift(Transmission.Shift.LOW);
        rightDrive.shift(Transmission.Shift.LOW);

        leftDrive.setPosition(MIN_ANGLE);
        rightDrive.setPosition(MIN_ANGLE);
    }

    private boolean isClimbing() {
        return true;
    }

    public void update(double time, double dT) {

        // update transmission/motor models
        leftDrive.update();
        rightDrive.update();

        double lArmTorque = leftDrive.getTorque();
        double rArmTorque = rightDrive.getTorque();
        double armTorque = rArmTorque + lArmTorque;

        double armAngle = rightDrive.getPosition();
        double cgAngle = armAngle + CG_ANGLE_OFFSET;

        double acceleration = 0;
        double sinAngle = 0;
        if (isClimbing()) {
            sinAngle = Math.sin(armAngle + VERTICAL_ANGLE);
            acceleration = (armTorque / ARM_LENGTH / (ChassisModel.MASS/2) - ACCEL_OF_GRAVITY / sinAngle) / ARM_LENGTH;
        }


        // integrate to velocity
        double velocity = rightDrive.getVelocity() + acceleration * dT;

        // integrate position
        double position = rightDrive.getPosition() + velocity * dT;

//        if (Math.abs(armTorque) > 0.01) {
//            System.out.format("%8.3f %8.3f %8.3f %8.3f \n",
//                    armTorque, acceleration, velocity, position);
//        }

        if (position > MAX_ANGLE) {
            position = MAX_ANGLE;
            velocity = 0;
        } else if (position < MIN_ANGLE) {
            position = MIN_ANGLE;
            velocity = 0;
        }

        leftDrive.setVelocity(velocity);
        rightDrive.setVelocity(velocity);

        leftDrive.setPosition(position);
        rightDrive.setPosition(position);

        leftEncoder.setVelocity(velocity);
        rightEncoder.setVelocity(velocity);

        leftEncoder.setPosition(position);
        rightEncoder.setPosition(position);
    }


    private static double calculateAngle(double height) {
        return Math.PI - ARM_BEND_ANGLE - Math.asin((ARM_HEIGHT - height)/ARM_LENGTH);
    }
}
