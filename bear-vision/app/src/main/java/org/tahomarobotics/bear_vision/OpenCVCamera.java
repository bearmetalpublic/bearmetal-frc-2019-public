package org.tahomarobotics.bear_vision;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Surface;
import android.view.WindowManager;
import android.app.Activity;
import android.widget.ImageView;
import android.widget.TextView;


import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfPoint3f;
import org.opencv.core.Point;
import org.opencv.core.Point3;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.tahomarobotics.robot.vision.VisionCommandListener;
import org.opencv.imgproc.Moments;
import org.tahomarobotics.robot.vision.VisionCommandListener;
import org.tahomarobotics.robot.vision.VisionCommandMessage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.LinkedList;
import java.util.Locale;


import io.rpng.calibration.utils.ImageUtils;

public class OpenCVCamera extends Activity implements VisionCommandListener {

    //TODO: Currently used in the findYaw, but unclear what it is for...
    private static double CENTER_X = 320;

    //Values for calculating distance
    //Calculation: F = (RT x D)/W
    //RT = Reference Target perceived Width in pixels
    //D = Distance from Target in inches
    //W = Actual Width of HiViz tape in inches
    //HiViz Tape reference: Width-2" x Height-5.5"
    //
    // Calibration exercise results for Focal Length:
    // F = (60 x 48)/2
    //F = 1440
    private static double FOCAL_LENGTH_NEXUS5X = 1440;
    private static double VISION_TAPE_WIDTH_INCHES = 2;

    //Camera Resolution settings
    private static int CAM_MAX_WIDTH = 640;
    private static int CAM_MAX_HEIGHT = 480;

    //TODO: Variables used to adjust Exposure and Focus Distance
    //TODO: Change these values as needed
    //TODO: Values are used in the CaptureRequest.Builder captureRequest
    private static long CAM_EXPOSURE_TIME = 1000000L;
    private static float CAM_FOCUS_DISTANCE = .2f;

    //TODO: Variables used to adjust Camera Calibration detail
    //TODO: Change these values as needed
    //TODO: These values are from calibration using Bear-Vision 3/26/2019
    //TODO: Nexus 5X used during 2019 season
            /*

                Average re-projection error: 0.630618

                Camera Matrix:
                [972.202909012218, 0, 640;
                0, 972.202909012218, 384;
                0, 0, 1]

                Distortion coefficients Matrix:
                [0.01552888520031677;
                -0.1006793254897132;
                 0;
                 0;
                 0]

            */

    //TODO: What is this fixed value used for, isn't field of view calculated, and not fixed?
    private static final double FOV_X_ANGLE = 59.1498;

    private static float AVG_REPROJECTION_ERROR = 0.630618f;
    private static Mat CAMERA_MATRIX;
    private static MatOfDouble DISTORTION_COEFFICIENTS_MATRIX;

    private static final String TAG = "Bear-Vision::OpenCVCamera";
    private CameraBridgeViewBase cameraBridgeViewBase;


    //The following are MIN/MAX values
    //for HUE, SATURATION, and VALUE (HSV)
    /*
    private static int CAM_HUE_MIN = 40;
    private static int CAM_HUE_MAX = 90;

    private static int CAM_SATURATION_MIN = 100;
    private static int CAM_SATURATION_MAX = 255;

    private static int CAM_VALUE_MIN = 30;
    private static int CAM_VALUE_MAX = 255;
    */
    //These Scalar values represent the above MIN/MAX values
    //for Hue, Saturation, and Value
    //Scalar(H, S, V)
    private static Scalar lowerBound = new Scalar(40, 100, 30);
    private static Scalar upperBound = new Scalar(80, 255, 255);

    private static MatOfPoint3f objectPoints;

    private CameraDevice camera;

    private VisionCommandMessage.Target[] target;
    private SocketHandler socket;

    private ImageReader imageReader = ImageReader.newInstance(CAM_MAX_WIDTH, CAM_MAX_HEIGHT, ImageFormat.YUV_420_888, 3);

    private ImageView imageView;

    //Display values for distance, FPS, sorting algorithm, and yaw
    private TextView distance_and_fps;
    private TextView sortText;
    private TextView yawText;

    //Values for determining the Frames per Second (FPS)
    private static long ONE_SECOND = 1000000L * 1000L; //1 second is 1000ms which is 1000000ns
    LinkedList<Long> frames = new LinkedList<>(); //List of frames within 1 second

    private VisionCommandMessage.Sorting sort = VisionCommandMessage.Sorting.SCREEN_CENTER;

    private Point3 rot;
    private Point3 tran;
    private Point centroid;
    private class TargetData {
        public MatOfPoint left;
        public MatOfPoint right;
        public double angle;
        public String toString(){
            return "";
        }
    }


    Mat hierarchy;

    private BaseLoaderCallback baseLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                    hierarchy = new Mat();
                    objectPoints = new MatOfPoint3f(
                            //Left Target:
                            //Top point, Left Point, Bottom  point, right point
                            //Right Target:
                            //...^^
                            new Point3(-5.9365, 31.621, 0.0),
                            new Point3(-7.3115, 26.296, 0.0),
                            new Point3(-5.375, 25.796, 0.0),
                            new Point3(-4.0, 31.121, 0.0),
                            new Point3(5.9365, 31.621, 0.0),
                            new Point3(4.0, 31.121, 0.0),
                            new Point3(5.375, 25.796, 0.0),
                            new Point3(7.3115, 26.296, 0.0)
                    );

                    //Camera Matrix values from Bear-Vision calibration 3/26/2019
                    CAMERA_MATRIX = Mat.zeros(3,3, CvType.CV_64F);
                    CAMERA_MATRIX.put(0,0, 972);
                    CAMERA_MATRIX.put(0,1, 0);
                    CAMERA_MATRIX.put(0,2,640);
                    CAMERA_MATRIX.put(1,0,0);
                    CAMERA_MATRIX.put(1,1,972);
                    CAMERA_MATRIX.put(1,2,384);
                    CAMERA_MATRIX.put(2,0,0);
                    CAMERA_MATRIX.put(2,1,0);
                    CAMERA_MATRIX.put(2,2,1);

                    rvec = new Mat();
                    tvec = new Mat();

                    //Distortion Coefficients Matrix
                    //Updated values from calibration on 40 images for Nexus 5X on 3/26/2019 using Bear-Vision
                    DISTORTION_COEFFICIENTS_MATRIX = new MatOfDouble(0.01552888520031677, -0.1006793254897132, 0, 0, 0);

                    imageView = findViewById(R.id.camera_image_view);

                    imageReader.setOnImageAvailableListener((ImageReader imageReader) -> {
                        Image image = imageReader.acquireNextImage();

                        Mat input = ImageUtils.imageToMat(image);

                        //Change from YUV which the ImageUtil makes, and change it to RGB
                        Imgproc.cvtColor(input, input, Imgproc.COLOR_YUV2RGB_I420);

                        Mat output = processFrame(input, input.clone());

                        //TODO: This code block needs additional optimization, this is slowing down the processing
                        //Update Image
                        final Bitmap bitmap = Bitmap.createBitmap(output.cols(), output.rows(), Bitmap.Config.ARGB_8888);
                        Utils.matToBitmap(output, bitmap);
                        imageView.setImageBitmap(bitmap);

                        // Make sure we close the image
                        image.close();
                    }, null);


                    //TODO: Investigate using threads only if performance optimization results
                    //TODO: make a case for more threads.
                    //TODO: Right now this is expensive, and has been subsequently disabled
                    //Core.setNumThreads(4);

                    openCamera();

                    break;

                default:

                    super.onManagerConnected(status);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "loading onCreate method...");


        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_opencv_camera);

        //Set the values for the string output to the screen
        //Distance to target in inches
        //Frames per Second (FPS)
        //Sorting algorithm
        distance_and_fps = findViewById(R.id.distance_and_fps);
        yawText = findViewById(R.id.yaw);
        sortText = findViewById(R.id.sortView);

        Log.d(TAG,"Instantiating SocketHandler class...");
        socket = SocketHandler.getInstance();
        socket.registerListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (cameraBridgeViewBase != null)
            cameraBridgeViewBase.disableView();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Bear-Vision::Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, baseLoaderCallback);
        } else {
            Log.d(TAG, "Bear-Vision::OpenCV library found inside package. Using it!");
            baseLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (cameraBridgeViewBase != null)
            cameraBridgeViewBase.disableView();
    }

    private void openCamera() {
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);

        Log.d(TAG, "Attempting to open camera");

        try {

            String cameraId = manager.getCameraIdList()[0];
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);

            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                Log.e(TAG, "Does not have CAMERA permission!");
                return;
            }
            manager.openCamera(cameraId, mCallback, null);

        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    private final CameraDevice.StateCallback mCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(CameraDevice cameraDevice) {
            camera = cameraDevice;
            setSettings();
        }


        @Override
        public void onDisconnected(CameraDevice cameraDevice) {

        }

        @Override
        public void onError(CameraDevice cameraDevice, int i) {

        }
    };

    private void setSettings(){
        try {

            Surface surface = imageReader.getSurface();
            final CaptureRequest.Builder captureRequest = camera.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequest.addTarget(surface);

            //Create session which allows us to create a request
            camera.createCaptureSession(Arrays.asList(surface), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(final CameraCaptureSession cameraCaptureSession) {

                    captureRequest.set(CaptureRequest.CONTROL_MODE, CaptureRequest.CONTROL_MODE_OFF);
                    captureRequest.set(CaptureRequest.CONTROL_VIDEO_STABILIZATION_MODE, CaptureRequest.CONTROL_VIDEO_STABILIZATION_MODE_OFF);
                    captureRequest.set(CaptureRequest.LENS_OPTICAL_STABILIZATION_MODE, CaptureRequest.LENS_OPTICAL_STABILIZATION_MODE_OFF);

                    captureRequest.set(CaptureRequest.SENSOR_EXPOSURE_TIME, CAM_EXPOSURE_TIME);
                    captureRequest.set(CaptureRequest.LENS_FOCUS_DISTANCE, CAM_FOCUS_DISTANCE);

                    //Put in capture request
                    try {
                        cameraCaptureSession.setRepeatingRequest(captureRequest.build(), new CameraCaptureSession.CaptureCallback() {
                            @Override
                            public void onCaptureStarted(CameraCaptureSession session, CaptureRequest request, long timestamp, long frameNumber) {
                                super.onCaptureStarted(session, request, timestamp, frameNumber);

                                //When this is called, an image has been pushed to the image reader, triggering ImageProcess to be called
                                Log.d(TAG, "onCaptureStarted - Processing Frame!");
                            }
                        }, null);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onConfigureFailed(CameraCaptureSession cameraCaptureSession) {

                }
            }, null);


        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private Mat rvec, tvec;
    private MatOfPoint2f imagePoints;

    private boolean isRotated(){
        return ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation() == Surface.ROTATION_270;
    }

    private Mat processFrame(Mat input, Mat display){


        double timestamp = System.currentTimeMillis() * 0.001;
        ArrayList<MatOfPoint> contours = new ArrayList<>();
        ArrayList<TargetData> targetData;

        if(!isRotated()) {
            Core.flip(input, input, -1);
            Core.flip(display, display, -1);
        }

        long time = System.currentTimeMillis();

        //Imgproc.cvtColor(input, input, Imgproc.COLOR_GRAY2RGB);
        Imgproc.cvtColor(input, input, Imgproc.COLOR_RGB2HSV);

        long time1 = System.currentTimeMillis();

        Core.inRange(input, lowerBound, upperBound, input);

        long time2 = System.currentTimeMillis();

        Imgproc.findContours(input, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

        Log.d(TAG, String.format("%6d %6d %6d", time1 - time, time2 - time1, System.currentTimeMillis() - time2));
        Log.d(TAG, String.format("Size of Contour Array: %d\n", contours.size()));

        for (int i = 0; i < contours.size(); i++) {
            MatOfPoint contour = contours.get(i);
            MatOfInt output = new MatOfInt();

            Point top = contour.toArray()[0];
            Point left = contour.toArray()[0];
            Point right = contour.toArray()[0];
            Point bottom = contour.toArray()[0];

            for (Point point : contour.toList()) {
                // Origin is in the top left, and y+ is down
                // thus we say that the top point has LESS y than the current point.
                if (point.y < top.y) top = point;
                if (point.y > bottom.y) bottom = point;
                if (point.x < left.x) left = point;
                if (point.x > right.x) right = point;
            }

            contours.set(i, new MatOfPoint(top, left, bottom, right));
        }

        targetData = sortAndFilter(contours);

        //Define colors to be used for contour lines
        Scalar colorGold = new Scalar(255, 215, 0);     //Gold
        Scalar colorRed = new Scalar(255,0,0);          //Red
        Scalar colorRandom = new Scalar(Math.random() * 150 + 50,Math.random() * 150 + 50,Math.random() * 150 + 50);    //Random

        //Draw contours around the vision targets by finding the convexHull using
        //the sorted targetData
        //TODO: Find a way to get a bounded rectangle that surrounds both vision targets
        //TODO: This will allow getting a centroid directly without doing calculations to find it
        for(TargetData tgt: targetData)
        {

            //ArrayList<MatOfPoint> cnts = new ArrayList<>();
            //if(tgt.left != null) cnts.add(tgt.left);
            //if(tgt.right != null) cnts.add(tgt.right);

            //Get the convexHull of the current targets
            MatOfPoint matOfPointIn = new MatOfPoint();
            List<MatOfInt> hull = new ArrayList<MatOfInt>();

            //Add to the arrayList for the hull
            for(int i=0; i < contours.size(); i++){
                hull.add(new MatOfInt());
            }

            //Call the convexHull openCV method
            for(int i=0; i < contours.size(); i++){
                Imgproc.convexHull(contours.get(i), hull.get(i));
            }

            List<Point[]> hullpoints = new ArrayList<Point[]>();
            for(int i=0; i < hull.size(); i++){
                Point[] points = new Point[hull.get(i).rows()];

                // Loop over all points that need to be hulled in current contour
                for(int j=0; j < hull.get(i).rows(); j++){
                    int index = (int)hull.get(i).get(j, 0)[0];
                    points[j] = new Point(contours.get(i).get(index, 0)[0], contours.get(i).get(index, 0)[1]);
                }

                hullpoints.add(points);
            }

            // Convert Point arrays into MatOfPoint
            List<MatOfPoint> hullmop = new ArrayList<MatOfPoint>();
            for(int i=0; i < hullpoints.size(); i++){
                MatOfPoint mop = new MatOfPoint();
                mop.fromArray(hullpoints.get(i));
                hullmop.add(mop);
            }

            // Draw the contours and hull results in different colors
            for(int i=0; i < contours.size(); i++){
//                Imgproc.drawContours(display, contours, i, colorGold, 3);
//                Imgproc.drawContours(display, hullmop, i, colorRed, 3);
            }

        }

        //Put additional checks in here
        //TODO: Checks for what?  What could be added that will be useful?
        //Maybe check if our targets are full, idk. : Already done by sortAndFilter in the sorting method.
        boolean dataValid = (targetData.size() != 0);
        imagePoints = new MatOfPoint2f();
        if(!dataValid) {
            return display;
        }

        //Variables for the centroid X,Y coordinates
        float centroidSelected_X = 0;
        float centroidSelected_Y = 0;

        //Variable for the distance to the vision target in inches
        double distanceToTargetInInches = 0;

        //This switch statement will look at all of the vision targets in the Field of View
        //and sort them to only choose 1.
        //
        // Get the distance from the target using the
        // perceived width of the vision tape target
        //D' = (W x F)/RT
        // Values to use:
        // FOCAL_LENGTH_NEXUS5X = 1440;
        // VISION_TAPE_WIDTH_INCHES = 2;
        // D': Distance in inches (Solve for this)
        // RT: Reflective Tape perceived width in pixels

        //Once a pair of vision targets is chosen, it will get the centroid of the left/right
        //vision targets (left X,Y and right X,Y)
        //The left/right X,Y coordinates will be used to get the centroid between the left/right targets
        //
        //Calculate centroid of left/right vision targets
        //Calculation for X coordinate: ((Right Centroid-X - Left Centroid-X)/2 + Left Centroid-X)
        //Calculation for Y coordinate: (Right Centroid-Y - Left Centroid-Y)/2 + Left Centroid-Y)


        switch(sort){
            case LEFTMOST:

                MatOfPoint2f LMpts = new MatOfPoint2f();
                LMpts.fromArray(targetData.get(0).left.toArray());
                double perceivedLMWidthOfTapeInPixels = 0;
                perceivedLMWidthOfTapeInPixels = Imgproc.minAreaRect(LMpts).size.width;

                Log.d(TAG, "Perceived Width of LEFTMOST target in pixels "+ Imgproc.minAreaRect(LMpts).size.width);

                //Get the distance in inches
                distanceToTargetInInches = getDistanceToTargetInInches(perceivedLMWidthOfTapeInPixels);

                Log.d(TAG, "The LEFTMOST distance in inches is: " + distanceToTargetInInches);

                imagePoints = new MatOfPoint2f(targetData.get(0).left.toArray());

                //Get the centroid of the left vision target
                Moments momentLeftMostL = Imgproc.moments(imagePoints);
                Point centroidLMLeft = new Point();
                centroidLMLeft.x = momentLeftMostL.get_m10()/momentLeftMostL.get_m00();
                centroidLMLeft.y = momentLeftMostL.get_m01()/momentLeftMostL.get_m00();
                Log.d(TAG,"The LEFTMOST left target centroid is {X,Y}:" + centroidLMLeft.toString());

                imagePoints = new MatOfPoint2f(targetData.get(0).right.toArray());
                //Get the centroid of the right vision target
                Moments momentLeftMostR = Imgproc.moments(imagePoints);
                Point centroidLMRight = new Point();
                centroidLMRight.x = momentLeftMostR.get_m10()/momentLeftMostR.get_m00();
                centroidLMRight.y = momentLeftMostR.get_m01()/momentLeftMostR.get_m00();
                Log.d(TAG,"The LEFTMOST right target centroid is {X,Y}:" + centroidLMRight.toString());


                //Calculate centroid of left/right vision targets
                //Calculation for X coordinate: ((Right Centroid-X - Left Centroid-X)/2 + Left Centroid-X)
                centroidSelected_X = (((float)centroidLMRight.x - (float)centroidLMLeft.x)/2 + (float)centroidLMLeft.x);
                //Calculation for Y coordinate: (Right Centroid-Y - Left Centroid-Y)/2 + Left Centroid-Y)
                centroidSelected_Y = (((float)centroidLMRight.y - (float)centroidLMLeft.y)/2 + (float)centroidLMLeft.y);

                break;

            case RIGHTMOST:

                int index = targetData.size() - 1;

                MatOfPoint2f RMpts = new MatOfPoint2f();
                RMpts.fromArray(targetData.get(index).left.toArray());
                double perceivedRMWidthOfTapeInPixels = 0;
                perceivedRMWidthOfTapeInPixels = Imgproc.minAreaRect(RMpts).size.width;
                Log.d(TAG, "Perceived Width of RIGHTMOST target in pixels "+ Imgproc.minAreaRect(RMpts).size.width);

                //Get the distance in inches
                distanceToTargetInInches = getDistanceToTargetInInches(perceivedRMWidthOfTapeInPixels);
                Log.d(TAG, "The RIGHTMOST distance in inches is: " + distanceToTargetInInches);

                imagePoints = new MatOfPoint2f(targetData.get(index).left.toArray());

                //Get the centroid of the left vision target
                Moments momentRightMostL = Imgproc.moments(imagePoints);
                Point centroidRMLeft = new Point();
                centroidRMLeft.x = momentRightMostL.get_m10()/momentRightMostL.get_m00();
                centroidRMLeft.y = momentRightMostL.get_m01()/momentRightMostL.get_m00();
                Log.d(TAG,"The RIGHTMOST left target centroid is {X,Y}:" + centroidRMLeft.toString());

                imagePoints = new MatOfPoint2f(targetData.get(index).right.toArray());
                //Get the centroid of the right vision target
                Moments momentRightMostR = Imgproc.moments(imagePoints);
                Point centroidRMRight = new Point();
                centroidRMRight.x = momentRightMostR.get_m10()/momentRightMostR.get_m00();
                centroidRMRight.y = momentRightMostR.get_m01()/momentRightMostR.get_m00();
                Log.d(TAG,"The RIGHTMOST right target centroid is {X,Y}:" + centroidRMRight.toString());

                //Calculate centroid of left/right vision targets
                //Calculation for X coordinate: ((Right Centroid-X - Left Centroid-X)/2 + Left Centroid-X)
                centroidSelected_X = (((float)centroidRMRight.x - (float)centroidRMLeft.x)/2 + (float)centroidRMLeft.x);
                //Calculation for Y coordinate: (Right Centroid-Y - Left Centroid-Y)/2 + Left Centroid-Y)
                centroidSelected_Y = (((float)centroidRMRight.y - (float)centroidRMLeft.y)/2 + (float)centroidRMRight.y);

                break;

            case CENTERMOST:

                int centerIndex = targetData.size() / 2;

                MatOfPoint2f CMpts = new MatOfPoint2f();
                CMpts.fromArray(targetData.get(centerIndex).left.toArray());
                double perceivedCMWidthOfTapeInPixels = 0;
                perceivedCMWidthOfTapeInPixels = Imgproc.minAreaRect(CMpts).size.width;
                Log.d(TAG, "Perceived Width of CENTERMOST target in pixels "+ Imgproc.minAreaRect(CMpts).size.width);

                //Get the distance in inches
                distanceToTargetInInches = getDistanceToTargetInInches(perceivedCMWidthOfTapeInPixels);
                Log.d(TAG, "The CENTERMOST distance in inches is: " + distanceToTargetInInches);

                imagePoints = new MatOfPoint2f(targetData.get(centerIndex).left.toArray());
                //Get the centroid of the left vision target
                Moments momentCenterMostL = Imgproc.moments(imagePoints);
                Point centroidCMLeft = new Point();
                centroidCMLeft.x = momentCenterMostL.get_m10()/momentCenterMostL.get_m00();
                centroidCMLeft.y = momentCenterMostL.get_m01()/momentCenterMostL.get_m00();
                Log.d(TAG,"The CENTERMOST left target centroid is {X,Y}:" + centroidCMLeft.toString());

                imagePoints = new MatOfPoint2f(targetData.get(centerIndex).right.toArray());
                //Get the centroid of the right vision target
                Moments momentCenterMostR = Imgproc.moments(imagePoints);
                Point centroidCMRight = new Point();
                centroidCMRight.x = momentCenterMostR.get_m10()/momentCenterMostR.get_m00();
                centroidCMRight.y = momentCenterMostR.get_m01()/momentCenterMostR.get_m00();
                Log.d(TAG,"The CENTERMOST right target centroid is {X,Y}:" + centroidCMRight.toString());

                //Calculate centroid of left/right vision targets
                //Calculation for X coordinate: ((Right Centroid-X - Left Centroid-X)/2 + Left Centroid-X)
                centroidSelected_X = (((float)centroidCMRight.x - (float)centroidCMLeft.x)/2 + (float)centroidCMLeft.x);
                //Calculation for Y coordinate: (Right Centroid-Y - Left Centroid-Y)/2 + Left Centroid-Y)
                centroidSelected_Y = (((float)centroidCMRight.y - (float)centroidCMLeft.y)/2 + (float)centroidCMLeft.y);

                break;

            case CLOSEST:

                MatOfPoint2f CLpts = new MatOfPoint2f();
                MatOfPoint2f CLpts2 = new MatOfPoint2f();

                TargetData closest = targetData.get(0);
                double lastDistance = 0;

                for(TargetData data : targetData){
                    CLpts.fromArray(data.left.toArray());
                    CLpts2.fromArray(data.right.toArray());
//                    double perceivedCLWidthOfTapeInPixels;
//                    perceivedCLWidthOfTapeInPixels = Imgproc.minAreaRect(CLpts).size.width;
//                    Log.d(TAG, "Perceived Width of Closest vision target in pixels: "+ Imgproc.minAreaRect(CLpts).size.width);
//
//                    //Get the distance in inches
//                    distanceToTargetInInches = getDistanceToTargetInInches(perceivedCLWidthOfTapeInPixels);
//                    Log.d(TAG,"The distance to the closest vision target is: " + distanceToTargetInInches + " inches away.");

                    double distance = Imgproc.minAreaRect(CLpts).size.area() + Imgproc.minAreaRect(CLpts2).size.area();

                    if(distance > lastDistance){
                        closest = data;
                        lastDistance = distance;
                    }
                }

                MatOfPoint2f pts = new MatOfPoint2f();
                pts.fromArray(closest.right.toArray());
                pts.fromArray(closest.left.toArray());
                perceivedLMWidthOfTapeInPixels = Imgproc.minAreaRect(pts).size.width;

                Log.d(TAG, "Perceived Width of LEFTMOST target in pixels "+ Imgproc.minAreaRect(pts).size.width);

                //Get the distance in inches
                distanceToTargetInInches = getDistanceToTargetInInches(perceivedLMWidthOfTapeInPixels);

                centroid = getCentroid(closest);

                ArrayList<MatOfPoint> cntsa = new ArrayList<>();
                cntsa.add(closest.left);
                cntsa.add(closest.right);
                Imgproc.drawContours(display, cntsa, -1, colorRed);
                //Calculate centroid of left/right vision targets
                //Calculation for X coordinate: ((Right Centroid-X - Left Centroid-X)/2 + Left Centroid-X)
                //TODO: Use Closest to find centroid and return distance
                //
                //Centroid X,Y variables
                // Use centroidSelected_X, centroidSelected_Y
                //
                //Distance to target in inches
                //Use distanceToTargetInInches

                break;
            case SCREEN_CENTER:
                TargetData center = targetData.get(0);
                for(TargetData t:targetData){
                    if(Math.abs(t.left.get(0,0)[0] - CENTER_X) < Math.abs(center.left.get(0,0)[0] - CENTER_X)){
                        center = t;
                    }
                }
                MatOfPoint2f cpts = new MatOfPoint2f();
                cpts.fromArray(center.right.toArray());
                cpts.fromArray(center.left.toArray());
                perceivedLMWidthOfTapeInPixels = Imgproc.minAreaRect(cpts).size.width;

                Log.d(TAG, "Perceived Width of LEFTMOST target in pixels "+ Imgproc.minAreaRect(cpts).size.width);

                //Get the distance in inches
                distanceToTargetInInches = getDistanceToTargetInInches(perceivedLMWidthOfTapeInPixels);
                centroid = getCentroid(center);

                ArrayList<MatOfPoint> cnts = new ArrayList<>();
                cnts.add(center.left);
                cnts.add(center.right);
                Imgproc.drawContours(display, cnts, -1, colorRed);
                break;


            default:

                return display;
        }

        Log.d(TAG,"The " + sort.toString() + " final centroid is {X,Y}" + centroid.x + "," + centroid.y);

        MatOfPoint pts = new MatOfPoint();
        pts.fromArray(imagePoints.toArray());
        double yaw = findYaw(centroid.x);

        if(socket.isConnected()){
            socket.sendMessage(timestamp, yaw, distanceToTargetInInches, centroidSelected_X, centroidSelected_X);
        }

        //TODO: Need to publish the centroid values {X,Y} and distance to Target
        //TODO: Understand what specific values need to be consumed by the robot to adjust the heading

        //Display the on-screen values for Distance to Target, FPS, and Sorting Algorithm
        final double dist = distanceToTargetInInches;
        final double current_fps = calculateFramesPerSecond();

        new Handler(Looper.getMainLooper()).post(() -> {

            distance_and_fps.setText(String.format(Locale.US, "Distance: %6.3f\nFPS: %6.3f", dist,current_fps));
            yawText.setText(String.format(Locale.US, "Yaw: %6.3f",yaw));
            sortText.setText(String.format(Locale.US, "Sorting Algorithm: %s", sort.toString()));

        });


        Log.d(
                TAG, String.format("Distance: %6.3f \n FPS: %6f \n Yaw: %6.3f \n Sort: %s",
                dist, current_fps, yaw, sort.toString())
        );

        return display;
    }

    private Point getCentroid(TargetData target) {


        imagePoints = new MatOfPoint2f(target.left.toArray());
        //Get the centroid of the left vision target
        Moments momentClosestLeft = Imgproc.moments(imagePoints);
        Point centroidClosestLeft = new Point();
        centroidClosestLeft.x = momentClosestLeft.get_m10()/momentClosestLeft.get_m00();
        centroidClosestLeft.y = momentClosestLeft.get_m01()/momentClosestLeft.get_m00();
        Log.d(TAG,"The Closest left target centroid is {X,Y}:" + centroidClosestLeft.toString());

        imagePoints = new MatOfPoint2f(target.right.toArray());
        //Get the centroid of the right vision target
        Moments momentClosestRight = Imgproc.moments(imagePoints);
        Point centroidClosestRight = new Point();
        centroidClosestRight.x = momentClosestRight.get_m10()/momentClosestRight.get_m00();
        centroidClosestRight.y = momentClosestRight.get_m01()/momentClosestRight.get_m00();
        Log.d(TAG,"The Closest right target centroid is {X,Y}:" + centroidClosestRight.toString());

        //Calculate centroid of left/right vision targets
        //Calculation for X coordinate: ((Right Centroid-X - Left Centroid-X)/2 + Left Centroid-X)
        return new Point((((float)centroidClosestRight.x - (float)centroidClosestLeft.x)/2 + (float)centroidClosestLeft.x),
        //Calculation for Y coordinate: (Right Centroid-Y - Left Centroid-Y)/2 + Left Centroid-Y)
        (((float)centroidClosestRight.y - (float)centroidClosestLeft.y)/2 + (float)centroidClosestLeft.y));
    }

    //Calculate the distance to the vision target
    //Inputs: perceivedWidthOfTapeInPixels
    //This is the perceived width of the vision tape in pixels
    //
    //Returns:
    //distance
    //This is the distance to the vision target in inches
    private double getDistanceToTargetInInches(double perceivedWidthOfTapeInPixels)
    {
        double distance = 0;
        distance = ((VISION_TAPE_WIDTH_INCHES * FOCAL_LENGTH_NEXUS5X)/perceivedWidthOfTapeInPixels);

        return distance;

    }

    //TODO: Review the calculation done here for findYam, the CENTER_X
    //TODO: is a fixed value and, and so is FOV_X_ANGLE and should
    //TODO: be calculated
    private double findYaw(double xCentroid) {
        double xTrans = xCentroid - CENTER_X;
        double yaw = pixelToYaw(xTrans);
        Log.d(TAG, "Yaw is: "+ yaw);
        return yaw;
    }

    private double pixelToYaw(double xTrans) {
        Log.d(TAG, "X offset of centroid:" +xTrans);
        return xTrans * FOV_X_ANGLE / (2 * CENTER_X);
    }

    private ArrayList<TargetData> sortAndFilter(ArrayList<MatOfPoint> contours){
        ArrayList<MatOfPoint> filterContoursContours = contours;
        ArrayList<MatOfPoint> output = new ArrayList<>();
        double filterContoursMinArea = 3.0;
        double filterContoursMinPerimeter = 10.0;
        double filterContoursMinWidth = 2.5;
        double filterContoursMaxWidth = 200.0;
        double filterContoursMinHeight = 6.0;
        double filterContoursMaxHeight = 200.0;
        double filterContoursMaxVertices = 1000000;
        double filterContoursMinVertices = 0;
        double filterContoursMinRatio = 0.5;
        double filterContoursMaxRatio = 2.0;
        filterContours(filterContoursContours, filterContoursMinArea, filterContoursMinPerimeter, filterContoursMinWidth, filterContoursMaxWidth, filterContoursMinHeight, filterContoursMaxHeight, filterContoursMaxVertices, filterContoursMinVertices, filterContoursMinRatio, filterContoursMaxRatio, output);
        // Sort
        insertionSort(output);
        return groupTargets(output);
    }

    private ArrayList<TargetData> groupTargets(ArrayList<MatOfPoint> contours) {
        ArrayList<TargetData> targetData = new ArrayList<>();
        for (int i = 0; i < contours.size(); i++) {
            MatOfPoint contour = contours.get(i);
            MatOfPoint next = null;
            if(i + 1 < contours.size()){
                next = contours.get(i + 1);
            }
            RotatedRect rotatedRect = Imgproc.minAreaRect(new MatOfPoint2f(contour.toArray()));
            if(next == null){
                continue;
            }
            RotatedRect rightRect = Imgproc.minAreaRect(new MatOfPoint2f(next.toArray()));
            Log.d(TAG, String.format("Left angle: %6.3f Right angle: %6.3f", rotatedRect.angle, rightRect.angle));
            if(Math.abs(rotatedRect.angle) - Math.abs(rightRect.angle) > 0.0){
                TargetData target = new TargetData();
                target.left = contour;
                target.right = next;
                target.angle = rotatedRect.angle;
                targetData.add(target);
                i++;
            }
        }
        return targetData;
    }

    void insertionSort(ArrayList<MatOfPoint> arr) {
        int i, j;
        MatOfPoint newValue;
        for (i = 1; i < arr.size(); i++) {
            newValue = arr.get(i);
            j = i;
            while (j > 0 && greaterThan(arr.get(j - 1) , newValue)) {
                arr.set(j, arr.get(j - 1));
                j--;
            }
            arr.set(j,newValue);
        }
    }

    private boolean greaterThan(MatOfPoint a, MatOfPoint b){
        return (a.get(0,0)[0] > b.get(0,0)[0]);
    }

    private void filterContours(List<MatOfPoint> inputContours, double minArea,
                                double minPerimeter, double minWidth, double maxWidth, double minHeight, double
                                        maxHeight, double maxVertexCount, double minVertexCount, double
                                        minRatio, double maxRatio, List<MatOfPoint> output) {
        final MatOfInt hull = new MatOfInt();
        output.clear();
        //operation
        for (int i = 0; i < inputContours.size(); i++) {
            final MatOfPoint contour = inputContours.get(i);
            final Rect bb = Imgproc.boundingRect(contour);
            final double area = Imgproc.contourArea(contour);
            if (area < minArea) {
                Log.d(TAG, "Contour failed for min area: " + area);
                continue;
            }
            if (Imgproc.arcLength(new MatOfPoint2f(contour.toArray()), true) < minPerimeter) {
                Log.d(TAG, "Contour failed for min perimeter");
                continue;
            }
            Imgproc.convexHull(contour, hull);
            final double ratio = bb.width / (double)bb.height;
            output.add(contour);
        }
    }

    @Override
    public void handleMessage(VisionCommandMessage msg) {
        sort = msg.sortingIndex;
        Log.d(TAG, "Received Commmand for Sort!");
    }

    //This method calculates the Frames per Second
    //Overview: It gets the current time in nano seconds,
    //and adds each frame to a LinkedList.
    //It then inspects the time, and removes any frames not
    //within the last second.
    //Using the time with nano seconds enables highly precise
    //frame counts.
    public double calculateFramesPerSecond(){
        long time = System.nanoTime(); //Current time in nano seconds
        frames.add(time); //Add this frame to the list
        while(true){
            long f = frames.getFirst(); //Look at the first element in frames
            if(time - f > ONE_SECOND){ //If it was more than 1 second ago
                frames.remove(); //Remove it from the list of frames
            } else break;
            /*If the frame was within 1 second, then all other frames
             * are also within 1 second
             */
        }
        return frames.size(); //Return the size of the list which is FPS value
    }


}