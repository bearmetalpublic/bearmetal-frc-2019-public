package org.tahomarobotics.bear_vision;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.content.Intent;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.File;

public class MainActivity extends Activity  {

    private static final String TAG = "Bear-Vision::MainActivity";
    static final int READ_BLOCK_SIZE = 4096;
    static final String OPENCV_FILE_NAME = "openCVCamCalibData.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createOpenCVCalibrationFile(OPENCV_FILE_NAME);
        readOpenCVCalibrationFile(OPENCV_FILE_NAME);

        // Button to call OpenCVCamera
        Button cameraInit = (Button) findViewById(R.id.cameraInit);

        cameraInit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent iCam = new Intent(getApplicationContext(),OpenCVCamera.class);
                startActivity(iCam);
            }

        } );

        // Button to call ColorBlobDetectionActivity
        Button colorBlobInit = (Button) findViewById(R.id.colorBlobInit);

        colorBlobInit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent iColorBlob = new Intent(getApplicationContext(),ColorBlobDetectionActivity.class);
                startActivity(iColorBlob);
            }
        } );


        // Button to call CameraCalibrationActivity
        Button cameraCalibrateInit = (Button) findViewById(R.id.cameraCalibrateInit);

        cameraCalibrateInit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent iCameraCalibrate = new Intent(getApplicationContext(),CameraCalibrationActivity.class);
                startActivity(iCameraCalibrate);
            }

        } );

        PowerManager powerSucker = (PowerManager) getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock powerSuckerLock = powerSucker.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
        powerSuckerLock.acquire();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.opencv_camera:
                Intent iCam = new Intent(getApplicationContext(),OpenCVCamera.class);
                startActivity(iCam);
                item.setChecked(true);

                return true;

            case R.id.calibratecamera:
                Intent iCameraCalibrate = new Intent(getApplicationContext(),CameraCalibrationActivity.class);
                startActivity(iCameraCalibrate);
                item.setChecked(true);

                return true;

            case R.id.colorblob_detect:
                Intent iColorBlob = new Intent(getApplicationContext(),ColorBlobDetectionActivity.class);
                startActivity(iColorBlob);
                item.setChecked(true);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //This will create the openCV calibration data file if it doesn't exist
    //This file is private to the application, and cannot be read by anything
    //but the bear-vision app
    public void createOpenCVCalibrationFile(String filename) {

        String openCVFileHeader = "openCV Camera Calibration Data";
        File file = new File(getApplicationContext().getFilesDir(),OPENCV_FILE_NAME);

        if (file.exists()) {
            //don't need to create it again
        }

        else {

            try {
                    FileOutputStream fileout = openFileOutput(OPENCV_FILE_NAME, MODE_PRIVATE);
                    OutputStreamWriter outputWriter = new OutputStreamWriter(fileout);
                    outputWriter.write(openCVFileHeader.toString());
                    outputWriter.close();

                    //display file saved message
                    //TODO: remove/comment out - this is only for debugging to confirm file creation
                    // Toast.makeText(getBaseContext(), "openCV camera calibration file saved successfully!",
                    //  Toast.LENGTH_SHORT).show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //This reads data from the openCV camera calibration file
    public void readOpenCVCalibrationFile(String filename) {

        try {
            FileInputStream fileIn=openFileInput(filename);
            InputStreamReader InputRead= new InputStreamReader(fileIn);

            char[] inputBuffer= new char[READ_BLOCK_SIZE];
            String s="";
            int charRead;

            while ((charRead=InputRead.read(inputBuffer))>0) {
                // char to string conversion
                String readstring=String.copyValueOf(inputBuffer,0,charRead);
                s +=readstring;
            }
            InputRead.close();
            //TODO: remove/comment out - this is only for debugging to confirm file creation
            //s = "Contents of Camera Calibration file: " + s;
            //Toast.makeText(getBaseContext(), s,Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

