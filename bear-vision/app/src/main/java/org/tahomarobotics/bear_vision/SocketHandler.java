package org.tahomarobotics.bear_vision;

import android.util.Log;

import org.opencv.core.Point3;
import org.tahomarobotics.robot.vision.VisionClient;
import org.tahomarobotics.robot.vision.VisionCommandListener;
import org.tahomarobotics.robot.vision.VisionCommandMessage;
import org.tahomarobotics.robot.vision.VisionStatusMessage;

public class SocketHandler implements VisionCommandListener {

    private static String TAG = "Bear Vision::SocketHandler";

    //TODO: Validate this hardcoded IP address.  The roboRIO has DHCP and assigns
    //a network address on a specific subnet when directly connected via USB
    //IP: 172.22.11.1
    //Subnet Mask: 255.255.255.248
    private VisionClient client = new VisionClient("localhost", 8254);

    private static SocketHandler instance = new SocketHandler();

    public static SocketHandler getInstance(){
        return instance;
    }

    private SocketHandler() {

        Log.d(TAG,"In the SocketHandler constructor...");
        client.addListener(this);
    }

    @Override
    public void handleMessage(VisionCommandMessage msg) {
        Log.d(TAG, "In the handleMessage method...");
        Log.d(TAG, msg.requestId + "");
    }

    public void sendMessage(double timestamp, double yaw, double dist, double cx, double cy){

        VisionStatusMessage msg = new VisionStatusMessage();

        msg.time = timestamp;
        msg.cameraPose.yaw = yaw;
        msg.cameraPose.distance = dist;
        msg.cameraPose.centroidX = cx;
        msg.cameraPose.centroidY = cy;

        msg.calibrationMode = VisionStatusMessage.CalibrationMode.CALIBRATED;

        Log.d(TAG, "Sending a message with requestId: " + msg.requestId);
        client.sendMessage(msg);
    }

    public boolean isConnected(){

        Log.d(TAG, "Checking the isConnected status...");
        return client.isConnected();
    }

    public void registerListener(VisionCommandListener listener){
        client.addListener(listener);
    }
}
