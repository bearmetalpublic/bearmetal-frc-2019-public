#include <jni.h>
#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>


extern "C"
jstring
Java_org_tahomarobotics_bear_1vision_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Placeholder for C++ libs";
    return env->NewStringUTF(hello.c_str());
}
