package org.tahomarobotics.robot.alphacollector;

import edu.wpi.first.wpilibj.command.Command;
import org.tahomarobotics.robot.path.ActionIF;

public class PowerCollector extends Command implements ActionIF {

	private double speed;

	private static final AlphaCollector collector = AlphaCollector.getInstance();

	public PowerCollector(double speed){
		this(speed, 1.0);
	}

	public PowerCollector(double speed, double timeout){
		requires(collector);
		this.speed = speed;
		setTimeout(timeout);
	}

	@Override
	protected void initialize() {
		collector.setPower(-speed);
	}

	@Override
	protected boolean isFinished() {
		return isTimedOut();
	}

	@Override
	protected void end() {
		collector.setPower(0.0);
	}

}
