package org.tahomarobotics.robot.alphacollector;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.tahomarobotics.robot.*;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.state.SwitchPlaceMode;

import java.util.ArrayList;

public class AlphaCollector extends Subsystem implements MotorInfo.SubsystemMotors {

    private final ArrayList<CANSparkMax> motors = new ArrayList<>();
    private final CANSparkMax collector = new CANSparkMax(RobotMap.ALPHA_COLLECTOR_MOTOR, CANSparkMaxLowLevel.MotorType.kBrushless);
    private final Solenoid solenoid = new Solenoid(RobotMap.PCM_MODULE, RobotMap.COLLECTOR_SOLENOID);

    public static final boolean IS_CENTER_COLLECTOR = RobotConfiguration.getConfigurationValue(true, true);

    private static final AlphaCollector instance = new AlphaCollector();

	private static final int MAX_CURRENT = 10;

	private AlphaCollector(){

        if (IS_CENTER_COLLECTOR) {
            collector.setSmartCurrentLimit(80, 20, 10000);
            collector.setInverted(true);
        } else {
            collector.setSmartCurrentLimit(80, 20, 10000);
        }

        collector.setIdleMode(CANSparkMax.IdleMode.kBrake);
        motors.add(collector);
    }

    public static AlphaCollector getInstance(){
        return instance;
    }

    public void setPower(double pow){
        collector.set(pow);
    }

    @Override
    protected void initDefaultCommand() {
        setDefaultCommand(new AlphaCollectorTeleopCommand());
    }

    public void openController(boolean open) {
        solenoid.set(open);
    }

    @Override
    public ArrayList<CANSparkMax> getMotorInfo() {
        return motors;
    }

    @Override
    public String getSubsystemName() {
        return this.getClass().getSimpleName();
    }

    private class AlphaCollectorTeleopCommand extends Command{

        public AlphaCollectorTeleopCommand(){
            requires(AlphaCollector.getInstance());
        }

        @Override
        protected boolean isFinished() {
            double power = (OI.getInstance().getManipLeftTrigger())
                    - (OI.getInstance().getManipRightTrigger() / 2);

            if(IS_CENTER_COLLECTOR){
                if(power == 0.0){
                    setPower(0.1 * (RobotState.getInstance().getWantedPlaceMode() == SwitchPlaceMode.PlaceMode.HATCH ? 1 : -1));
                } else
                    setPower(power * (RobotState.getInstance().getWantedPlaceMode() == SwitchPlaceMode.PlaceMode.HATCH ? 1 : -1));

            } else{

                setPower(power);
            }
            return false;
        }


    }
}
