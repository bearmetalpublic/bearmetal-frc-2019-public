package org.tahomarobotics.robot.alphacollector;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.OI;
import org.tahomarobotics.robot.alphaarm.AlphaArm;
import org.tahomarobotics.robot.path.ActionIF;

public class OpenCollectorCommand extends Command implements ActionIF {

    private static Logger LOGGER = LoggerFactory.getLogger(OpenCollectorCommand.class);

    private AlphaCollector collector = AlphaCollector.getInstance();

    public enum State {
        OPEN, CLOSE
    }

    private State state;

    public OpenCollectorCommand(State state){
        this(state, 0.0);
    }

    public OpenCollectorCommand(State state, double timeout){
        //TODO: remove require
        requires(collector);
        this.state = state;
        setTimeout(timeout);
    }

    @Override
    protected boolean isFinished() {
        LOGGER.info("Ended for: " + state.toString());
        collector.openController(State.OPEN != state);
//        if(State.CLOSE == state){
//            OI.getInstance().vibrate(.5 + Timer.getFPGATimestamp());
//        }
        OI.getInstance().vibrateTime(0.100);
        return isTimedOut();
    }


}
