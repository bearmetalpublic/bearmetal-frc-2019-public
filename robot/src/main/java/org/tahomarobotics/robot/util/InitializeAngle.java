package org.tahomarobotics.robot.util;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Subsystem;
import org.tahomarobotics.robot.MagEncoder;
import org.tahomarobotics.robot.utilityarm.UtilityArm;

public abstract class InitializeAngle extends Command {

	private int goodCount, totalCount;
	private double prev = Double.NaN;
	private final MagEncoder magEncoder;

	public InitializeAngle(MagEncoder magEncoder, Subsystem subsystem) {
		this.magEncoder = magEncoder;
		requires(subsystem);
		setRunWhenDisabled(true);
	}

	@Override
	protected void initialize() {
		goodCount = 0;
	}

	@Override
	protected boolean isFinished() {
		totalCount++;
		double position = magEncoder.getPosition();

		if (!Double.isNaN(prev) && Math.abs(position - prev) < 0.5) {
			goodCount++;
		} else {
			goodCount = 0;
		}
		prev = position;

		if (goodCount > 5) {

			System.out.println("getPosition took " + totalCount + " value was " + position);
			setOffset(position);
			return true;
		}

		return false;
	}

	@Override
	protected void interrupted(){
		System.out.println("InitializeAngle interrupted");
	}

	protected abstract void setOffset(double offset);
}
