package org.tahomarobotics.robot.util;

import edu.wpi.first.wpilibj.Timer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.OI;
import org.tahomarobotics.robot.chassis.TeleopDriveCommand;
import org.tahomarobotics.robot.motion.MotionController;
import org.tahomarobotics.robot.motion.MotionState;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.vision.Limelight;

public class VisionAllign {

	private static final RobotState robotState = RobotState.getInstance();

	private double kP = 0.008;
	private double kV = 0.00;
	private double kI = 0.00;

	private final MotionController motionController = new MotionController(kP, kV, kI,
			0,0, 0);

	private static Logger LOGGER = LoggerFactory.getLogger(TeleopDriveCommand.class);


	public double[] getForwardAndRotational(MotionState motionSetpoint, MotionState currentMotionState){

		double wantedAngle = Limelight.getX();

		double elapsedTime = Timer.getFPGATimestamp();
		double distance = Limelight.getDistance();
		if(distance < 100){
			distance /= 200;
		}else {
			distance /= 100;
		}

		double forward = OI.getInstance().getDriverRightTrigger() * (distance);
		double robotHeading = robotState.getHeading();

		double delta = robotHeading - wantedAngle;

		currentMotionState.setPosition(delta);
		currentMotionState.setVelocity(robotState.getRotVel());

		motionSetpoint.setPosition(robotHeading);
		motionSetpoint.setVelocity(0.0);

		double rotPower = -motionController.update(elapsedTime, currentMotionState, motionSetpoint);

		double[] forRot = {forward, rotPower};

		return forRot;
	}
}
