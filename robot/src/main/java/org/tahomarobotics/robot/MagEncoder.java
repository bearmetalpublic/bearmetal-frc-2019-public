/*
 * Copyright 2019 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot;

import edu.wpi.first.wpilibj.Counter;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.Timer;
import org.tahomarobotics.robot.util.MathUtil;

public class MagEncoder {
    Counter pwmCounter;

    private final static double DEGREES_PER_PULSE_SECOND=360.0/4096.0*1000000.0;

    private final double calibratedZeroPoint;
    private final double normalizationPoint;
    private final double multiplier;
    private final double defaultValue;

    public MagEncoder(int pwm, boolean inverted, double calibratedZeroPoint, double normalizationPoint, double defaultValue) {
        pwmCounter = new Counter(pwm);
        pwmCounter.setSemiPeriodMode(true);
        this.calibratedZeroPoint = calibratedZeroPoint;
        this.normalizationPoint = normalizationPoint;
        multiplier = inverted ? -1.0 : 1.0;
        this.defaultValue = defaultValue;
    }
    double initTime;
    boolean first = true;
    /**
     *
     * @return position in degrees
     */
    public double getPosition() {

        double period;
        double position;
        if(first){
            initTime = Timer.getFPGATimestamp();
            first = false;
        }
        do {
            period = pwmCounter.getPeriod();
            position = period * DEGREES_PER_PULSE_SECOND;

            position = MathUtil.normalizeAngleDegrees(multiplier * position + calibratedZeroPoint, normalizationPoint);

            if(initTime + 1 < Timer.getFPGATimestamp()){
                position = defaultValue;
                period = 1;
            }
        } while (Double.isNaN(position) || period == 0);



        return position;
    }

    public double getRaw() {
        return pwmCounter.getPeriod() * DEGREES_PER_PULSE_SECOND;
    }
}
