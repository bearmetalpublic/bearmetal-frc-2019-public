/*
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.state;

import java.util.LinkedList;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.RobotMap;
import org.tahomarobotics.robot.UpdateIF;
import org.tahomarobotics.robot.chassis.Chassis;
import org.tahomarobotics.robot.motion.MotionState;
import org.tahomarobotics.robot.util.MathUtil;

import com.ctre.phoenix.sensors.PigeonIMU;

import edu.wpi.first.networktables.EntryListenerFlags;
import edu.wpi.first.networktables.EntryNotification;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.Notifier;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * Responsible for updating the state of the Robot with respect to position and movements
 * on the field.  This is where the position, pose and velocities are estimated.  Estimation
 * can be simple or more complex.  The simple is done by integrating encoder odometry and IMU
 * gyroscope rates.  A more complex add vision target to create a more accurate pose eliminating
 * errors with wheel slip, gyroscope drift and integration.
 *
 * Thread safety: The estimation task is running in a Notifier thread.  Remote position update are
 * coming through another thread and the update (publish) is the main robot thread.  For thread safety,
 * synchronize on the <code>INSTANCE</code> object.  Use the <code>updateRobotPose</code> for changing
 * the state.
 *
 */
@SuppressWarnings("unused")
public class RobotState implements UpdateIF {

    private static final Logger LOGGER = LoggerFactory.getLogger(RobotState.class);

    private static final RobotState INSTANCE = new RobotState();

    // used for setting the cyclic rate of the state estimate loop
    private static final double UPDATE_PERIOD = 0.01;
    private static final double RUNNING_AVERAGE_SECONDS = 10.0;
    private static final int RUNNING_AVERAGE_SIZE = (int) (RUNNING_AVERAGE_SECONDS / UPDATE_PERIOD);

    // robot state variable
    private final Pose2D robotPose = new Pose2D();
    private final RobotSpeed robotSpeed = new RobotSpeed();

    // used to calculate delta time
    private double time = Timer.getFPGATimestamp();

    // used to determine rotational velocity and heading
    private static final int ANGULAR_RATE_AXIS_X = 0;
    private static final int ANGULAR_RATE_AXIS_Y = 1;
    private static final int ANGULAR_RATE_AXIS_Z = 2;
    private static final int ANGULAR_RATE_AXES = 3;

    //Value from 0.0 - 1.0 dictating if 0% - 100% of array needs to be filled
    //private static final double PERCENT_FOR_VALID_DRIFT = 0.9;

    private final PigeonIMU imu = new PigeonIMU(RobotMap.PIGEON_IMU);
    private final double[] angularRate = new double[ANGULAR_RATE_AXES];
    private final Notifier timer;

    private LinkedList<Double> runningList = new LinkedList<>();
    private boolean calculatingAverage;
    private double averageDrift, timeout, averageDriftSum;
    private int runningAverageCounter;

    // private singleton constructor
    private RobotState() {

        // Estimate state at relative fast periodic rate
        timer = new Notifier(this::estimateState);
        timer.startPeriodic(UPDATE_PERIOD);

        // Used
        configureRemoteReset();
    }

    protected void finalize() {
    	timer.close();
    }

    /**
     * Return the RobotState singleton instance
     */
    public static RobotState getInstance() {
        return INSTANCE;
    }

    /**
     * This is only used to facilitate reseting or moving the robot
     * to known position which can be called from the simulation or the dash-board
     */
    private void configureRemoteReset() {

        // this is the only coupling to the Simulation
        SmartDashboard.getEntry("ResetRobotPoseCommand").addListener(new Consumer<EntryNotification>() {

            private double[] reset = new double[3];

            @Override
            public void accept(EntryNotification t) {
                NetworkTableEntry entry = t.getEntry();
                reset = entry.getDoubleArray(reset);

                // reset the robot pose
                resetRobotPose(reset[0], reset[1], reset[2]);
            }

        }, EntryListenerFlags.kNew | EntryListenerFlags.kUpdate | EntryListenerFlags.kLocal);
    }

    private SwitchPlaceMode.PlaceMode mode = SwitchPlaceMode.PlaceMode.HATCH;

    public void setWantedPlaceMode(SwitchPlaceMode.PlaceMode mode){
        this.mode = mode;
    }

    public SwitchPlaceMode.PlaceMode getWantedPlaceMode(){
        return mode;
    }

    private ClimbIntentMode.ClimbIntent climbIntent = ClimbIntentMode.ClimbIntent.LEVEL3;

    public void setClimbIntent(ClimbIntentMode.ClimbIntent intent){
        climbIntent = intent;
    }

    public ClimbIntentMode.ClimbIntent getClimbIntent() {
        return climbIntent;
    }

    /**
     * Reset robot pose to a known position
     *
     * @param x - inches
     * @param y - inches
     * @param heading - degrees
     */
    public void resetRobotPose(double x, double y, double heading) {
        LOGGER.info(String.format("Robot Position Reset: %4.1f, %4.1f, %4.1f", x, y, heading));
        updateRobotPose(x, y, heading, 0, 0);
        SmartDashboard.putNumberArray("SimResetRobotPoseCommand", new double[] {x, y, heading});
    }


    /**
     * Robot pose should only be updated through this method for thread safety
     *
     * @param x - X position in inches (starting at 0 on blue and ending at 648 on red end)
     * @param y - Y position in inches (starting at 0 on scoring table side and ending at 324)
     * @param heading - robot heading (0 degrees along y axis as it is increasing, counter clockwise increases
     * @param fwd - forward velocity (in/sec)
     * @param rot - rotational velocity (degrees/sec)
     */
    private void updateRobotPose(double x, double y, double heading, double fwd, double rot) {

        // different threads can be calling this position update method so it must be thread safe
        synchronized(INSTANCE) {
            robotPose.x = x;
            robotPose.y = y;
            robotPose.heading = heading;
            robotSpeed.forward = fwd;
            robotSpeed.rotational = rot;
        }
    }

    public double getHeading(){
        return robotPose.heading;
    }

    public double getRotVel(){
        return robotSpeed.rotational;
    }
    /**
     * Return the rotational velocity in degrees/second
     *
     * @return rotation velocity (degrees/second)
     */
    private double getRotationalVelocity() {
        imu.getRawGyro(angularRate);
        return angularRate[ANGULAR_RATE_AXIS_Z];
    }

    /**
     * Estimate robot state (pose and velocities)
     */
    private void estimateState() {

        // update times
        double time = Timer.getFPGATimestamp();
        double dT = time - this.time;
        this.time = time;

        // sample odometry and gyroscope
        double forwardVelocity = Chassis.getInstance().getForwardVelocity();
        double rotationalVelocity = getRotationalVelocity() - averageDrift;
        double deltaForward = forwardVelocity * dT;
        double deltaHeading = rotationalVelocity * dT;

        // update state, reading and writing state in thread consistent manner
        synchronized(INSTANCE) {

            timeout = Math.max(timeout - dT, 0);

            //Check if the robot move. If it did, reset calculations.
            if(calculatingAverage && Math.abs(forwardVelocity) > 0.0001 && timeout == 0){
                LOGGER.info("Robot moved! Restarting calibrating after 1 second timeout");
                resetDriftCalc();
                timeout = 1.0;

                //If calculating average, add to the array
            } else if(calculatingAverage && timeout == 0){
                //Add the current velocity to the average sum & add it to the beginning of the list
                averageDriftSum += rotationalVelocity;
                runningList.addFirst(rotationalVelocity);

                //Increment Counter
                runningAverageCounter++;

                //If the counter reaches over the size we want, start removing the last value
                if(runningAverageCounter > RUNNING_AVERAGE_SIZE)
                    averageDriftSum -= runningList.getLast();

                //Tell the robot we are in fact not rotating in place
                rotationalVelocity = 0;
                deltaHeading = 0;
            }

            // update velocities
            robotSpeed.forward = forwardVelocity;
            robotSpeed.rotational = rotationalVelocity;

            // using average heading, update position components using trigonometry
            double averageHeading = Math.toRadians(robotPose.heading + deltaHeading/2);
            robotPose.x += deltaForward * Math.cos(averageHeading);
            robotPose.y += deltaForward * Math.sin(averageHeading);

            // update heading
            robotPose.heading =  robotPose.heading + deltaHeading;
            //LOGGER.info(String.format("X: %6.3f Y: %6.3f HDG: %6.3f", robotPose.x, robotPose.y, robotPose.heading));
        }
    }

    @Override
    public void update(boolean isEnabled) {

        // publish estimated robot pose to dash-board with thread safety in mind
        synchronized(INSTANCE) {
            SmartDashboard.putNumberArray("RobotPose", new double[] {robotPose.x, robotPose.y, robotPose.heading});
        }
    }

    public void cancelAverage(){
        LOGGER.info("Running Average canceled");
        calculatingAverage = false;
    }

    public void lockInDrift(){
        synchronized (INSTANCE){
            calculatingAverage = false;

            averageDrift = averageDriftSum / Math.min(runningAverageCounter, RUNNING_AVERAGE_SIZE);

            LOGGER.info("Drift offset set to: " + averageDrift);

            if(runningAverageCounter < RUNNING_AVERAGE_SIZE)
                LOGGER.warn("Drift calculations interrupted! Using " + runningAverageCounter + " samples.");
        }
    }

    public void resetDriftCalc(){
        synchronized (INSTANCE){
            runningAverageCounter = 0;
            calculatingAverage = true;
            runningList.clear();
        }
    }

    /**
     * Updates the provided MotionState with the current forward velocity and updates the current position
     * with an integrated averaged velocity.  The reverse flag determines if the state is used for going forward or
     * reverse.
     *
     * @param robotPose  - position and heading of robot to be updated
     * @param motionState - state to be updated
     * @param elapsedTime - time provided by the user
     * @param reverse - true if reversing
     */
    public void getRobotState(Pose2D robotPose, MotionState motionState, double elapsedTime, boolean reverse) {

        synchronized(INSTANCE) {
            double prevTime = motionState.time;
            double prevVelocity = motionState.velocity;
            motionState.time = elapsedTime;
            motionState.velocity = robotSpeed.forward * (reverse ? -1 : 1);
            motionState.position += (prevVelocity + motionState.velocity) / 2 * (motionState.time - prevTime);
            robotPose.x = this.robotPose.x;
            robotPose.y = this.robotPose.y;
            robotPose.heading = this.robotPose.heading + (reverse ? 180 : 0);
        }
    }
}
