package org.tahomarobotics.robot.state;

import edu.wpi.first.wpilibj.command.Command;
import org.tahomarobotics.robot.OI;
import org.tahomarobotics.robot.alphacollector.AlphaCollector;
import org.tahomarobotics.robot.alphacollector.OpenCollectorCommand;

public class SwitchPlaceMode extends Command {

	private final RobotState state = RobotState.getInstance();

	private PlaceMode mode;

	public enum PlaceMode{
		BALL,
		HATCH
	}

	public SwitchPlaceMode(PlaceMode pos){
		mode = pos;
	}

	@Override
	protected boolean isFinished() {
		if(mode != state.getWantedPlaceMode()) {
			state.setWantedPlaceMode(mode);
		}

		if (AlphaCollector.IS_CENTER_COLLECTOR) {
			if (mode == PlaceMode.HATCH) {
				new OpenCollectorCommand(OpenCollectorCommand.State.OPEN).start();
			} else if (mode == PlaceMode.BALL) {
				new OpenCollectorCommand(OpenCollectorCommand.State.CLOSE).start();
			}
		}
		OI.getInstance().vibrateTime(100);
		return true;
	}
}
