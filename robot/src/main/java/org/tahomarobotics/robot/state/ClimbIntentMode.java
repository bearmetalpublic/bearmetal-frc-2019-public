package org.tahomarobotics.robot.state;

import edu.wpi.first.wpilibj.command.Command;

public class ClimbIntentMode extends Command {

    private ClimbIntent intent;

    public ClimbIntentMode(ClimbIntent intent){
        this.intent = intent;
    }

    public enum ClimbIntent {
        LEVEL3(19, 107),
        LEVEL2(6, 173);

        public double height;
        public double utilArmAngle;

        ClimbIntent(double height, double utilArmAngle){
            this.height = height;
            this.utilArmAngle = utilArmAngle;
        }
    }

    @Override
    protected boolean isFinished() {
        RobotState.getInstance().setClimbIntent(intent);
        return true;
    }

}
