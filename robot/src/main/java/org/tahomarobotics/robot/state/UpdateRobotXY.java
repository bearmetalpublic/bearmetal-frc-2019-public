package org.tahomarobotics.robot.state;

import edu.wpi.first.wpilibj.command.Command;

public class UpdateRobotXY extends Command {

	private Pose2D pose;

	public UpdateRobotXY(Pose2D pose){
		this.pose = new Pose2D(pose);
	}

	@Override
	protected boolean isFinished() {
		RobotState.getInstance().resetRobotPose(pose.x, pose.y, RobotState.getInstance().getHeading());
		return true;
	}
}