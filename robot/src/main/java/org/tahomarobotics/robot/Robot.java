/*
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot;

import com.revrobotics.CANSparkMax;
import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.*;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.alphaarm.AlphaArm;
import org.tahomarobotics.robot.alphacollector.AlphaCollector;
import org.tahomarobotics.robot.alphacollector.PowerCollector;
import org.tahomarobotics.robot.alphalift.AlphaLift;
import org.tahomarobotics.robot.auto.Autonomous;
import org.tahomarobotics.robot.betalift.BetaLift;
import org.tahomarobotics.robot.chassis.Chassis;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.utilityarm.UtilityArm;
import org.tahomarobotics.robot.vision.*;
import org.tahomarobotics.robot.utilityarm.UtilityArmCollector;

import java.util.ArrayList;
import java.util.List;

public class Robot extends TimedRobot {

    private static final Logger LOGGER = LoggerFactory.getLogger(Robot.class);
    private final MotorInfo motorInfo = new MotorInfo();

    // collection of sub-systems added to the robot
    private final List<UpdateIF> update = new ArrayList<>();
    private final List<InitIF> init = new ArrayList<>();

    // used in autonomous mode
    private final Autonomous autonomous = Autonomous.getInstance();
    private final UtilityArmCollector collector = UtilityArmCollector.getInstance();

    /**
     * robotInit is called once on start of the robot code.  Create all
     * sub-systems and add to collection
     */
    @Override
    public void robotInit() {
        LOGGER.info("robotInit()");

        init.add(autonomous);
        init.add(UtilityArm.getInstance());
        init.add(AlphaArm.getInstance());

        update.add(new Limelight());
        update.add(OI.getInstance());
        update.add(RobotState.getInstance());
        update.add(UtilityArm.getInstance());
        update.add(AlphaLift.getInstance());
        update.add(AlphaArm.getInstance());
        update.add(BetaLift.getInstance());
        update.add(autonomous);

        motorInfo.addSubsystem(Chassis.getInstance());
        motorInfo.addSubsystem(AlphaLift.getInstance());
        motorInfo.addSubsystem(AlphaArm.getInstance());
        motorInfo.addSubsystem(AlphaCollector.getInstance());
        motorInfo.addSubsystem(BetaLift.getInstance());
        motorInfo.addSubsystem(UtilityArm.getInstance());

        for(InitIF subsystem : init){
            subsystem.init();
        }

        motorInfo.start();
        Limelight.drivingMode(true);

    }



    /**
     * robotPeriodic is called periodically every 20 milliseconds unless the
     * period is set to something different via <code>setPeriod</code>
     */
    @Override
    public void robotPeriodic() {
        // run scheduler in every mode
        Scheduler.getInstance().run();

        double time = Timer.getFPGATimestamp();

        // update each sub-system
        for (UpdateIF subsystem : update) {
            subsystem.update(DriverStation.getInstance().isEnabled());
        }

        double newTime = Timer.getFPGATimestamp() - time;

        SmartDashboard.putNumber("Update Time", newTime * 1000);
        SmartDashboard.putBoolean("Phone Connected", VisionHandler.getInstance().isConnected());

        if(!motorInfo.isAlive()){
            motorInfo.start();
        }
    }

    @Override
    public void disabledInit() {
        LOGGER.info("disabledInit()");

        Chassis.getInstance().setIdleMode(CANSparkMax.IdleMode.kCoast);
        AlphaLift.getInstance().setIdleMode(CANSparkMax.IdleMode.kCoast);
        UtilityArm.getInstance().setIdleMode(CANSparkMax.IdleMode.kCoast);
        BetaLift.getInstance().breakMode();
        LOGGER.info("Calibrating GYRO");
        AlphaLift.getInstance().setIdleMode(CANSparkMax.IdleMode.kBrake);

        OI.getInstance().endVibrate();
    }

    @Override
    public void autonomousInit() {
        AlphaCollector.getInstance().openController(false);
        LOGGER.info("autonomousInit()");
        UtilityArm.getInstance().setHold();
        AlphaArm.getInstance().setHold();
        AlphaLift.getInstance().setHold();
        BetaLift.getInstance().setHold();
        Chassis.getInstance().setIdleMode(CANSparkMax.IdleMode.kBrake);
        AlphaLift.getInstance().setIdleMode(CANSparkMax.IdleMode.kBrake);
        UtilityArm.getInstance().setIdleMode(CANSparkMax.IdleMode.kBrake);
        BetaLift.getInstance().zero();
        autonomous.start();
    }

    @Override
    public void teleopInit() {
        AlphaCollector.getInstance().openController(false);
        LOGGER.info("teleopInit()");

        UtilityArm.getInstance().setHold();
        AlphaArm.getInstance().setHold();
        AlphaLift.getInstance().setHold();
        BetaLift.getInstance().setHold();
        //RobotState.getInstance().lockInDrift();
        Chassis.getInstance().setIdleMode(CANSparkMax.IdleMode.kBrake);
        AlphaLift.getInstance().setIdleMode(CANSparkMax.IdleMode.kBrake);
        UtilityArm.getInstance().setIdleMode(CANSparkMax.IdleMode.kBrake);
        BetaLift.getInstance().zero();

        //We want the sorting to be Centermost for Driver to drive with.
        new SendPhoneTargetCommand(VisionCommandMessage.Sorting.SCREEN_CENTER).start();

        autonomous.stop();

    }

    @Override
    public void disabledPeriodic() {
    }

    @Override
    public void autonomousPeriodic() {
    }

    @Override
    public void teleopPeriodic() {
    }

    public static void main(String[] args) {
        Robot.startRobot(Robot::new);
    }

}
