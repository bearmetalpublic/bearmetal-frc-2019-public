/*
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot;

import edu.wpi.first.wpilibj.DigitalInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RobotConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(Robot.class);

    public static enum RobotIdentity {
        COMPETITION, PRACTICE, UNKNOWN;
    }

    //checks what robot it is connected to and sets constants
    private static DigitalInput comp = new DigitalInput(9);
    private static DigitalInput practice = new DigitalInput(8);

    public final static RobotIdentity robotIndentity = getRobotIdentity();

    private static RobotIdentity getRobotIdentity() {

        if (comp.get() && !practice.get()) {
            LOGGER.info("competition robot connected");
            return RobotIdentity.COMPETITION;

        } else if (!comp.get() && practice.get()) {
            LOGGER.info("practice robot connected");
            return RobotIdentity.PRACTICE;

        } else if (comp.get() && practice.get()){
            LOGGER.error("Both inputs are connected, unable to determine robot, using competition values");
            return RobotIdentity.COMPETITION;

        } else {
            LOGGER.error("Neither input is connected, unable to determine robot, using competition values");
            return RobotIdentity.COMPETITION;
        }
    }


    public static double getConfigurationValue(double competition, double practice ) {
        if (robotIndentity == RobotIdentity.PRACTICE) {
            return practice;
        } else {
            return competition;
        }
    }

    public static int getConfigurationValue(int competition, int practice ) {
        if (robotIndentity == RobotIdentity.PRACTICE) {
            return practice;
        } else {
            return competition;
        }
    }

    public static boolean getConfigurationValue(boolean competition, boolean practice ) {
        if (robotIndentity == RobotIdentity.PRACTICE) {
            return practice;
        } else {
            return competition;
        }
    }

    public static String getConfigurationValue(String competition, String practice ) {
        if (robotIndentity == RobotIdentity.PRACTICE) {
            return practice;
        } else {
            return competition;
        }
    }

    public static <T> T getConfigurationValue(T competition, T practice ) {
        if (robotIndentity == RobotIdentity.PRACTICE) {
            return practice;
        } else {
            return competition;
        }
    }

}
