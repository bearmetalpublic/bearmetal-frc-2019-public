package org.tahomarobotics.robot.betalift;

import edu.wpi.first.wpilibj.command.Command;

public class BetaLiftZero extends Command{
	private final double maxCurrent = 1.0;
	private final BetaLift betaLift = BetaLift.getInstance();
	private int count = 0;
	private int countButTwo = 0;

	public BetaLiftZero(){
		requires(betaLift);
	}

	@Override
	protected void initialize(){
		System.out.println("Began Beta Zeroing");
		betaLift.setPublicPower(-.1);
		betaLift.limit();
		setTimeout(3.0);
	}

	@Override
	protected boolean isFinished() {
		betaLift.setOffset(0.0);
		return isTimedOut();
	}

	@Override
	protected void end(){
		betaLift.setPublicPower(0.0);
		System.out.println("finished beta zeroing");
		betaLift.zeroed();
		betaLift.setHold(1);
		betaLift.unLimit();
	}
}
