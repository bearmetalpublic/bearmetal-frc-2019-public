/*
 * Copyright 2019 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.betalift;

import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.tahomarobotics.robot.motion.MotionController;
import org.tahomarobotics.robot.motion.MotionProfile;
import org.tahomarobotics.robot.motion.MotionProfileFactory;
import org.tahomarobotics.robot.motion.MotionState;
import org.tahomarobotics.robot.util.ChartData;
import org.tahomarobotics.robot.util.Constants;

public class BetaLiftCommand extends Command {
	private double target;
	private double startTime;
	private boolean isCompleted = false;

	private final double maxVelocity = 50;
	private final double maxAccel = 300;

	private final double P = 0.1;
	private final double I = 0.1;
	private final double v = 0.0;
	private final double kffv = BetaLiftConstants.kFFV;
	private final double kffa = BetaLiftConstants.kFFA;
	private final double tolerance = 0.25;
	private final double TIMEOUT = 1;

	private final MotionController controller = new MotionController(P, v, I, kffv, kffa, tolerance);;
	private MotionProfile profile;
	private MotionProfileFactory.Profile profileType = MotionProfileFactory.Profile.Trapezoid;
	private MotionState setpoint = new MotionState();
	private MotionState currentState = new MotionState();

	private final BetaLift betaLift = BetaLift.getInstance();

	private final ChartData chartData = new ChartData("Beta Lift Motion", "Time (Sec)", "Velocity (in/sec)",
			new String[] {"expected-vel", "actual-vel", "voltage", "expected-pos", "actual-pos"});


	public BetaLiftCommand(double target){
		requires(betaLift);

		this.target = target;


	}

	@Override
	protected void initialize() {
		profile = MotionProfileFactory.createMotionProfile(profileType, 0.0, betaLift.getPosition(), target,
				0.0, 0.0, maxVelocity, maxAccel, 0.0);

		startTime = Timer.getFPGATimestamp();

		chartData.clear();

		controller.reset();

		setpoint.setPosition(0).setVelocity(0).setAcceleration(0);

		setTimeout(profile.getEndTime() + TIMEOUT);
	}

	@Override
	protected void execute(){

		double time = Timer.getFPGATimestamp();

		double deltaTime = time - startTime;

		currentState.setTime(deltaTime);
		currentState.setPosition(betaLift.getPosition());
		currentState.setVelocity(betaLift.getVelocity());

		isCompleted = !profile.getSetpoint(deltaTime, setpoint);

		setpoint.acceleration = Constants.METERS_PER_INCH * setpoint.acceleration + Constants.GRAVITIONAL_ACCELERATION;

		double voltage = controller.update(deltaTime, currentState, setpoint);
		double percent = voltage / RobotController.getBatteryVoltage();

		betaLift.setPublicPower(percent);

		chartData.addData(new double[]{deltaTime, setpoint.velocity, currentState.velocity, voltage, setpoint.position, currentState.position});
	}

	@Override
	protected boolean isFinished() {
		return isCompleted && (isTimedOut() || controller.onTarget());
	}

	@Override
	protected void end() {
		SmartDashboard.putRaw("BetaLiftChartData", chartData.serialize());
		betaLift.setPublicPower(0.0);
		betaLift.setHold();
	}
}
