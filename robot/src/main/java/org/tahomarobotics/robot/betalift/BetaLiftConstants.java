/*
 * Copyright 2019 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.betalift;

import org.tahomarobotics.robot.util.Constants;

public class BetaLiftConstants {

	private static final double STAGE_1 = 72.0 / 12.0;
	private static final double OUTPUT_DIAMETER = 1.48 * 21.0/23.0;//1.275
	private static final double OUTPUT_RADIUS = OUTPUT_DIAMETER / 2;
	private static final double OUTPUT_CIRCUMFERENCE = OUTPUT_DIAMETER * Math.PI;

	private static final double MOTOR_COUNT = 2.0;

	public static final double GEARBOX_RATIO = STAGE_1;
	public static final double PULSES_PER_REVOLUTION = 1.0;
	public static final double INCHES_PER_PULSE = (GEARBOX_RATIO * OUTPUT_CIRCUMFERENCE) / PULSES_PER_REVOLUTION;

	public static final double REV_TO_INCHES = OUTPUT_CIRCUMFERENCE / GEARBOX_RATIO;
	public static final double RPMS_TO_INCHES_PER_SEC = REV_TO_INCHES / 60;

	private static final double ROBOT_MASS = 106.744 * Constants.KILOGRAM_PER_LBMASS;
	private static final double BETA_LIFT_MASS = 7.667 * Constants.KILOGRAM_PER_LBMASS;
	private static final double CLIMB_MASS = (ROBOT_MASS - BETA_LIFT_MASS) / 2.0;

	public static final double SPEC_VOLTAGE = 12.0;
	public static final double FREE_SPEED = 5676; // RPM
	public static final double FREE_ANGULAR_VELOCITY = FREE_SPEED * 2 * Math.PI / 60; // radian/sec
	public static final double FREE_CURRENT = 1.8; // Amps
	public static final double STALL_TORQUE = 2.6; // N-m
	public static final double STALL_CURRENT = 105; // Amps
	public static final double RESISTANCE = SPEC_VOLTAGE / STALL_CURRENT;
	public static final double kT = STALL_TORQUE / (STALL_CURRENT - FREE_CURRENT); // Nm/Amp
	public static final double kV = FREE_ANGULAR_VELOCITY / (SPEC_VOLTAGE - RESISTANCE * FREE_CURRENT); // rad/s per volt

	public static final double kFFV =  GEARBOX_RATIO / kV / OUTPUT_RADIUS * 1.3;

	public static final double kFFA_CLIMB = Constants.METERS_PER_INCH * RESISTANCE * Constants.METERS_PER_INCH * OUTPUT_RADIUS * CLIMB_MASS
			/ (kT * GEARBOX_RATIO) / MOTOR_COUNT;

	public static final double kFFA = Constants.METERS_PER_INCH * RESISTANCE * Constants.METERS_PER_INCH * OUTPUT_RADIUS * -BETA_LIFT_MASS
			/ (kT * GEARBOX_RATIO) / MOTOR_COUNT;
}
