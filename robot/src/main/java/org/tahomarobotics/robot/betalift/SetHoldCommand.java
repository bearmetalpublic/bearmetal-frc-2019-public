package org.tahomarobotics.robot.betalift;

import edu.wpi.first.wpilibj.command.Command;

public class SetHoldCommand extends Command {
    private final BetaLift lift = BetaLift.getInstance();
    private final double holdHeight;

    public SetHoldCommand(double holdHeight){
        this.holdHeight = holdHeight;
    }
    @Override
    protected void initialize() {
        lift.setHold(holdHeight);
    }

    @Override
    protected boolean isFinished() {
        return lift.holdOnTarget();
    }
}
