/*
 * Copyright 2019 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.betalift;

import com.revrobotics.CANEncoder;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.InitIF;
import org.tahomarobotics.robot.MotorInfo;
import org.tahomarobotics.robot.RobotMap;
import org.tahomarobotics.robot.UpdateIF;
import org.tahomarobotics.robot.motion.MotionController;
import org.tahomarobotics.robot.motion.MotionState;

import java.util.ArrayList;


public class BetaLift extends Subsystem implements UpdateIF, MotorInfo.SubsystemMotors {

    private final ArrayList<CANSparkMax> motors = new ArrayList<>();

    private static final BetaLift INSTANCE = new BetaLift();

    private static Logger LOGGER = LoggerFactory.getLogger(BetaLift.class);

    private final CANEncoder encoder;
    private final double INCHES_PER_PULSE = BetaLiftConstants.INCHES_PER_PULSE;


    private final CANSparkMax motorTop = new CANSparkMax(RobotMap.BETALIFT_MOTOR_TOP, CANSparkMaxLowLevel.MotorType.kBrushless);
    private final CANSparkMax motorBottomBack = new CANSparkMax(RobotMap.BETALIFT_MOTOR_BOTTOM_BACK, CANSparkMaxLowLevel.MotorType.kBrushless);

    private final AnalogInput sonicSensor = new AnalogInput(0);
    private State state = State.HOLD;

    private double startTime;
    private final MotionState setpoint = new MotionState();
    private final MotionState currentState = new MotionState();
    private final MotionController controller = new MotionController(0.8, 0.0, 0.08, 0.0, 0.0, 0.25);

    private double offset = 0.0;
    private boolean zeroed = false;

    enum State {
        HOLD,
        ACTIVE,
        DISABLED
    }

    private static final double MIN = 0;
    private static final double MAX = 25;

    private BetaLift() {
        motorTop.setSmartCurrentLimit(80, 20, 10000);
        motorBottomBack.setSmartCurrentLimit(80, 20, 10000);

        motorTop.setInverted(true);
        motorBottomBack.setInverted(true);

        motorTop.getEncoder().setPosition(0.0);
        motorBottomBack.getEncoder().setPosition(0.0);

        motorBottomBack.follow(motorTop);

        motorBottomBack.setPeriodicFramePeriod(CANSparkMaxLowLevel.PeriodicFrame.kStatus1, 5);
        motorTop.setPeriodicFramePeriod(CANSparkMaxLowLevel.PeriodicFrame.kStatus1, 5);
        motorTop.setPeriodicFramePeriod(CANSparkMaxLowLevel.PeriodicFrame.kStatus2, 5);
        motorBottomBack.setPeriodicFramePeriod(CANSparkMaxLowLevel.PeriodicFrame.kStatus2, 5);

        motorBottomBack.setIdleMode(CANSparkMax.IdleMode.kCoast);
        motorTop.setIdleMode(CANSparkMax.IdleMode.kCoast);

        encoder = motorTop.getEncoder();

        motors.add(motorBottomBack);
        motors.add(motorTop);

        setpoint.setVelocity(0.0);
        setpoint.setPosition(1.0);
        setpoint.setAcceleration(0.0);
        sonicSensor.setOversampleBits(2);
    }

    public static BetaLift getInstance() {
        return INSTANCE;
    }

    @Override
    protected void initDefaultCommand() {
    }

    public void limit(){
        motorTop.setSmartCurrentLimit(2);
        motorBottomBack.setSmartCurrentLimit(2);
    }

    public void unLimit(){
        motorTop.setSmartCurrentLimit(80, 20, 10000);
        motorBottomBack.setSmartCurrentLimit(80, 20, 10000);
    }

    public void breakMode(){
        motorTop.setIdleMode(CANSparkMax.IdleMode.kBrake);
    }

    public void setOffset(double offset){
        motorTop.getEncoder().setPosition(offset);
	}

    public void setPublicPower(double power) {
        state = State.ACTIVE;
        setPrivatePower(power);
    }

    private void setPrivatePower(double power) {
        motorTop.set(power);
    }

    public double getVelocity() {
        return encoder.getVelocity() * BetaLiftConstants.RPMS_TO_INCHES_PER_SEC;
    }

    protected double getRawPos() {
        return encoder.getPosition() * BetaLiftConstants.REV_TO_INCHES;
    }
    public double getPosition() {
        double height = getRawPos();
        if(height > MAX){
            setOffset(MAX);
        }else if(height < MIN){
            setOffset(0.0);
        }
        return height;
    }

    public void setHold() {
    	setHold(getPosition());
	}

	public void setHold(double height){
        startTime = Timer.getFPGATimestamp();
        state = State.HOLD;
        setpoint.setPosition(height);
    }

	public double getCurrent() {
    	return (motorTop.getOutputCurrent() + motorBottomBack.getOutputCurrent()) / 3.0;
	}

    @Override
    public ArrayList<CANSparkMax> getMotorInfo() {
        return motors;
    }

    @Override
    public String getSubsystemName() {
        return this.getClass().getSimpleName();
    }

    public void zero() {
        if(!zeroed){
            new BetaLiftZero().start();
        }
    }
    public void zeroed() {
        zeroed = true;
    }

    public boolean holdOnTarget(){
        return controller.onTarget();
    }

    public boolean sonicPassed(){
        return sonicSensor.getVoltage() < 1.0;
    }
	@Override
	public void update(boolean isEnabled) {
        SmartDashboard.putNumber("Sonic", sonicSensor.getVoltage());
        SmartDashboard.putNumber("Beta lift height", getPosition());
        if(isEnabled) {
            if (state == State.HOLD) {
                double time = Timer.getFPGATimestamp() - startTime;
                currentState.setPosition(getPosition());
                currentState.setVelocity(getVelocity());
                setPrivatePower(controller.update(time, currentState, setpoint) / RobotController.getBatteryVoltage());
            }
        } else {
            state = State.DISABLED;
            setPrivatePower(0.0);
        }
	}
}
