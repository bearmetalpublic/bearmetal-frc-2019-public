package org.tahomarobotics.robot.betalift;//package org.tahomarobotics.robot.betalift;
//
//import edu.wpi.first.wpilibj.RobotController;
//import edu.wpi.first.wpilibj.Timer;
//import edu.wpi.first.wpilibj.command.Command;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.tahomarobotics.robot.Robot;
//import org.tahomarobotics.robot.motion.MotionController;
//import org.tahomarobotics.robot.motion.MotionState;
//
//public class BetaLiftHold extends Command {
//
//	private final BetaLiftZero zeroCommand;
//	private final BetaLift betaLift = BetaLift.getInstance();
//	private final MotionState setpoint = new MotionState();
//	private final MotionState currentState = new MotionState();
//	private final MotionController controller = new MotionController(0.8, 0.0, 0.08, 0.0, 0.0, 0.0);
//	private final static Logger LOGGER = LoggerFactory.getLogger(BetaLiftHold.class);
//	private double startTime;
//
//	public BetaLiftHold(){
//		requires(betaLift);
//		zeroCommand = new BetaLiftZero();
//		setpoint.setVelocity(0.0);
//		setpoint.setPosition(1.0);
//		setpoint.setAcceleration(0.0);
//	}
//
//	public void setHoldHeight(double holdHeight){
//		setpoint.setPosition(holdHeight);
//	}
//
//	@Override
//	protected void initialize(){
//		startTime = Timer.getFPGATimestamp();
//		if(!betaLift.getZeroed()){
//			zeroCommand.start();
//		}
//	}
//
//	@Override
//	protected void execute(){
//		double time = Timer.getFPGATimestamp() - startTime;
//		currentState.setPosition(betaLift.getPosition());
//		currentState.setVelocity(betaLift.getVelocity());
//		//LOGGER.info("Tried to set power to: " + controller.update(time, currentState, setpoint));
//		betaLift.setPower(controller.update(time, currentState, setpoint) / RobotController.getBatteryVoltage());
//	}
//
//	@Override
//	protected boolean isFinished() {
//		return false;
//	}
//
//	@Override
//	protected void end(){
//		betaLift.setPower(0.0);
//	}
//}
