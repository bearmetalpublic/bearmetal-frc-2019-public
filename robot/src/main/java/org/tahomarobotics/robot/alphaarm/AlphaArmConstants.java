/*
 * Copyright 2019 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.alphaarm;

import org.tahomarobotics.robot.util.Constants;

public class AlphaArmConstants {
    private static final double STAGE_1 = 54.0 / 10.0;
    private static final double STAGE_2 = 72.0 / 24.0;
    private static final double STAGE_3 = 60.0/ 24.0;
    public static final double GEARBOX_RATIO = STAGE_1 * STAGE_2 * STAGE_3;

    public static final double DEGREES_PER_REV = 360d/GEARBOX_RATIO;
    public static final double DPS_FROM_RPM = DEGREES_PER_REV / 60;

    public static final double CLAW_MASS = 7.0 * Constants.KILOGRAM_PER_LBMASS;
    public static final double CLAW_LENGTH = 10.157 * Constants.METERS_PER_INCH;
    private static final double CLAW_INTERIA = CLAW_MASS * CLAW_LENGTH * CLAW_LENGTH;

    public static final double SPEC_VOLTAGE = 12.0;
    public static final double FREE_SPEED = 5676; // RPM
    public static final double FREE_ANGULAR_VELOCITY = FREE_SPEED * 2 * Math.PI / 60; // radian/sec
    public static final double FREE_CURRENT = 1.8; // Amps
    public static final double STALL_TORQUE = 2.6; // N-m
    public static final double STALL_CURRENT = 105; // Amps
    public static final double RESISTANCE = SPEC_VOLTAGE / STALL_CURRENT;
    public static final double kT = STALL_TORQUE / (STALL_CURRENT - FREE_CURRENT); // Nm/Amp
    public static final double kV = FREE_ANGULAR_VELOCITY / (SPEC_VOLTAGE - RESISTANCE * FREE_CURRENT); // rad/s per volt

    public static final double kFFV =  GEARBOX_RATIO / Math.toDegrees(kV);
    //.0187 .81
    private static final double FUDGE_FACTOR = 1.0 / Math.PI;
    public static final double kFFA =  CLAW_INTERIA * RESISTANCE / (GEARBOX_RATIO * kT) * Math.PI / 180 * FUDGE_FACTOR;

    public static final double MAX_GRAVITY_ANGULAR_ACCLERATION = Math.toDegrees(Constants.GRAVITIONAL_ACCELERATION / AlphaArmConstants.CLAW_LENGTH);
}
