/*
 * Copyright 2019 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.alphaarm;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.*;
import org.tahomarobotics.robot.alphacollector.AlphaCollector;
import org.tahomarobotics.robot.motion.MotionController;
import org.tahomarobotics.robot.motion.MotionState;
import org.tahomarobotics.robot.util.InitializeAngle;
import java.util.ArrayList;


public class AlphaArm extends Subsystem implements UpdateIF, InitIF, MotorInfo.SubsystemMotors {

    private static Logger LOGGER = LoggerFactory.getLogger(AlphaArm.class);

    private static final double MAX_MANUAL_VELOCITY = 60;

    public enum AlphaArmPosition {
        BALL_PLACEMENT(45, true),
        HATCH(0, true),
        BALL_COLLECT(AlphaCollector.IS_CENTER_COLLECTOR ? -47 : -47, true),
        CARGO(AlphaCollector.IS_CENTER_COLLECTOR ? -19 : -18, true),
        SAFE_MOVEMENT(45, false),
        TOP_ROCKET(21.5, true),
        BOTTOM_HATCH(3, false);

        public double angle;
        private double offset;
        private boolean allowsOffset;


        AlphaArmPosition(double angle, boolean allowsOffset){
            this.angle = angle;
            this.allowsOffset = allowsOffset;
        }

        public void updateOffset(double deltaTheta){
//            if(!allowsOffset) return;
//            offset += deltaTheta;
        }

        public double getOffset(){
            return offset;
        }
    }

    public enum State {
        HOLD,
        ACTIVE,
        DISABLED
    }

    private State state = State.ACTIVE;
    private AlphaArmPosition lastPosition = AlphaArmPosition.BALL_COLLECT;

    private static final AlphaArm INSTANCE = new AlphaArm();

    private static double MAX_OFFSET = 30.0;

    private final ArrayList<CANSparkMax> motors = new ArrayList<>();
    private final CANSparkMax alphaArmMotor = new CANSparkMax(RobotMap.ALPHA_ARM_MOTOR,  CANSparkMaxLowLevel.MotorType.kBrushless);

    // magnetic encoder
    private double magOffset = RobotConfiguration.getConfigurationValue(-206.3, -53.5);
    private final MagEncoder encoder = new MagEncoder(2, false, magOffset, 0, RobotConfiguration.getConfigurationValue(83, 83));

    private double encoderOffset;

    private MotionState setpoint = new MotionState();
    private MotionState currentState = new MotionState();


    private double p = AlphaCollector.IS_CENTER_COLLECTOR ?
            RobotConfiguration.getConfigurationValue(.1, 0.1) :
            RobotConfiguration.getConfigurationValue(.1, 0.1);

    private double i = AlphaCollector.IS_CENTER_COLLECTOR ?
            RobotConfiguration.getConfigurationValue(.06, .01) :
            RobotConfiguration.getConfigurationValue(.005, .01);

    private double v = RobotConfiguration.getConfigurationValue(.00, .00);

    private final MotionController holdMotionController = new MotionController(p, v, i, 0, AlphaArmConstants.kFFA, 0.1);

    public static AlphaArm getInstance(){
        return INSTANCE;
    }

    private static final double MIN = -74;
    private static final double MAX = 83;

    private AlphaArm(){
        alphaArmMotor.setInverted(false);
        alphaArmMotor.setIdleMode(CANSparkMax.IdleMode.kBrake);
        alphaArmMotor.getEncoder().setPosition(0.0);
        motors.add(alphaArmMotor);
    }

    public double getSetAngle(AlphaArmPosition target){
        lastPosition = target;
        return lastPosition.angle + lastPosition.getOffset();
    }

    //set power
    public void setPower(double power){
        setActive();
        updatePower(power);
    }

    public void setHold(){
        setHold(getAngle());
    }

    public void setHold(double position) {
        setpoint.position = position;
        holdMotionController.reset();
        alphaArmMotor.setSmartCurrentLimit(80, 20, 10000);
        state = State.HOLD;
        LOGGER.info("HOLD");
    }

    public void setActive(){
        if(state != State.ACTIVE) {
            LOGGER.info("ACTIVE");
        }
        alphaArmMotor.setSmartCurrentLimit(80, 20, 10000);
        state = State.ACTIVE;
    }

    //get velocity
    public double getVelocity() {
        return alphaArmMotor.getEncoder().getVelocity() * AlphaArmConstants.DPS_FROM_RPM;
    }

    //get position
    public double getAngle(){
        double angle = alphaArmMotor.getEncoder().getPosition() * AlphaArmConstants.DEGREES_PER_REV + encoderOffset;
        if (angle < MIN) {
            encoderOffset -= angle - MIN;
        }else if (angle > MAX){
            encoderOffset -= angle - MAX;
        }
        return angle;
    }

    private void updatePower(double power) {
        alphaArmMotor.set(power);
    }

    private void teleopUpdates() {

        //We do not want to update offsets if the robot is not enabled as hold could be changed by accident.
        if(!DriverStation.getInstance().isEnabled()) return;

        OI oi = OI.getInstance();
        int dpad = oi.getDriverDPad();
        double manual = oi.getRightManipThrottle();
        manual += (dpad == OI.DPAD_EAST) ? 1 : ((dpad == OI.DPAD_WEST) ? -1 : 0);

        double velocity = manual * MAX_MANUAL_VELOCITY;
        double deltaTheta = velocity * 0.02;

        if((deltaTheta > 0 && lastPosition.getOffset() > MAX_OFFSET) ||
                (deltaTheta < 0 && lastPosition.getOffset() < -MAX_OFFSET)) {
            return;
        }

        setpoint.setVelocity(velocity);

        setpoint.position += deltaTheta;

        lastPosition.updateOffset(deltaTheta);
    }

    @Override
        protected void initDefaultCommand() {
    }

    @Override
    public void init() {
        new InitializeAngle(encoder, this) {
            @Override
            protected void setOffset(double offset) {
                encoderOffset = offset;
                setHold();
            }
        }.start();
    }

    @Override
    public void update(boolean isEnabled) {
        SmartDashboard.putNumber("AlphaArm Angle", getAngle());
        SmartDashboard.putNumber("Raw AlphaArm Angle", encoder.getRaw());
        //System.out.println(encoder.getRaw() + " " + encoder.getPosition());

        if(isEnabled) {
            if (state == State.HOLD) {


                currentState.position = AlphaArm.getInstance().getAngle();
                currentState.velocity = AlphaArm.getInstance().getVelocity();

                setpoint.acceleration = Math.cos(Math.toRadians(currentState.position)) * AlphaArmConstants.MAX_GRAVITY_ANGULAR_ACCLERATION;

                teleopUpdates();

                double power = holdMotionController.update(Timer.getFPGATimestamp(), currentState, setpoint);
                double voltage = RobotController.getBatteryVoltage();

                updatePower(power / voltage);
            }
        } else {
            state = State.DISABLED;
            updatePower(0.0);
        }
    }

    @Override
    public ArrayList<CANSparkMax> getMotorInfo() {
        return motors;
    }

    @Override
    public String getSubsystemName() {
        return this.getClass().getSimpleName();
    }

}
