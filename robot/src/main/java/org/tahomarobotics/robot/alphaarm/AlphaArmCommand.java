/*
 * Copyright 2019 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.alphaarm;

import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.tahomarobotics.robot.Robot;
import org.tahomarobotics.robot.RobotConfiguration;
import org.tahomarobotics.robot.motion.MotionController;
import org.tahomarobotics.robot.motion.MotionProfile;
import org.tahomarobotics.robot.motion.MotionProfileFactory;
import org.tahomarobotics.robot.motion.MotionState;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.state.SwitchPlaceMode;
import org.tahomarobotics.robot.util.ChartData;

public class AlphaArmCommand extends Command {

    private final AlphaArm arm = AlphaArm.getInstance();

    private double startTime;
    private double startPosition;
    private boolean isCompleted = false, rocketPos;

    private double maxVelocity = 360;
    private final double maxAccel= 1500;

    private double P = RobotConfiguration.getConfigurationValue(.005, .04);
    private double I = RobotConfiguration.getConfigurationValue(.01, .06);
    private double V = RobotConfiguration.getConfigurationValue(.0, .00);
    private final static double kffv = AlphaArmConstants.kFFV;
    private final static double kffa = AlphaArmConstants.kFFA / 1.1;
    private final static double tolerance = 1;
    private final static double TIMEOUT = 0.25;

    private final MotionController controller = new MotionController(P, V, I, kffv, kffa, tolerance);

    private AlphaArm.AlphaArmPosition position;
    private MotionProfile profile;
    private MotionProfileFactory.Profile profileType = MotionProfileFactory.Profile.Trapezoid;
    private MotionState setpoint = new MotionState();
    private MotionState currentState = new MotionState();

    private final ChartData chartData = new ChartData("AlphaArm"," Time ( Sec)", "Velocity (in/sec)",
            new String[]{"expected-vel", "actual-vel", "voltage", "expected-pos", "actual-pos"});

    public AlphaArmCommand(AlphaArm.AlphaArmPosition target, boolean rocketPos, double maxVel){
        position = target;
        this.rocketPos = rocketPos;
        maxVelocity = maxVel;
    }

    public AlphaArmCommand(AlphaArm.AlphaArmPosition target, boolean rocketPos){
        this(target, rocketPos, 450);
    }

    public AlphaArmCommand(AlphaArm.AlphaArmPosition target, double maxVelocity){
        this(target, false, maxVelocity);
    }

    public AlphaArmCommand(AlphaArm.AlphaArmPosition target){
        this(target, false, 450);
    }
    @Override
    protected void initialize() {
        arm.setActive();

        double startPosition = arm.getAngle();
        boolean topRocket = (position == AlphaArm.AlphaArmPosition.HATCH && rocketPos
                          && RobotState.getInstance().getWantedPlaceMode() == SwitchPlaceMode.PlaceMode.BALL);

        double pos = arm.getSetAngle(topRocket ? AlphaArm.AlphaArmPosition.TOP_ROCKET : position);

        profile = MotionProfileFactory.createMotionProfile(profileType,0.0, startPosition, pos,
                0.0, 0.0 ,maxVelocity, maxAccel, 0);

        setTimeout(profile.getEndTime() + TIMEOUT);

        startTime = Timer.getFPGATimestamp();

        chartData.clear();

        controller.reset();

        setpoint.setPosition(0).setAcceleration(0).setVelocity(0);
    }

    @Override
    protected void execute() {

        double time = Timer.getFPGATimestamp();

        double deltaTime = time - startTime;

        currentState.setTime(deltaTime);
        currentState.setPosition(arm.getAngle());
        currentState.setVelocity(arm.getVelocity());

        isCompleted = !profile.getSetpoint(deltaTime, setpoint);

        setpoint.acceleration += Math.cos(Math.toRadians(setpoint.position)) * AlphaArmConstants.MAX_GRAVITY_ANGULAR_ACCLERATION;

        double voltage = controller.update(deltaTime, currentState, setpoint);
        double percent = voltage / RobotController.getBatteryVoltage();

        arm.setPower(percent);

        chartData.addData(new double[]{deltaTime, setpoint.velocity,
                currentState.velocity, voltage * 10.0, setpoint.position, currentState.position});

    }

    @Override
    protected boolean isFinished() {
        return isCompleted && (isTimedOut() || controller.onTarget());
    }

    @Override
    protected void end() {
        SmartDashboard.putRaw("AlphaArmChartData", chartData.serialize());
        arm.setHold(setpoint.position);
    }
}