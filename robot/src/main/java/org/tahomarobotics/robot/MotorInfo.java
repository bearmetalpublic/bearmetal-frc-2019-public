/*
 * Copyright 2019 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot;

import com.revrobotics.CANSparkMax;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.chassis.Chassis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class MotorInfo extends Thread{
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
	private final NetworkTable table = NetworkTableInstance.getDefault().getTable("SmartDashboard");
	private final NetworkTableEntry entry = table.getEntry("MotorData");
	private final ArrayList<SubsystemMotors> subsystemMotors = new ArrayList<>();
	private double temp;
	private double current;
	private double attempts;
	private int count = 0;

	public void addSubsystem(SubsystemMotors subsystem){
		subsystemMotors.add(subsystem);
	}

	@Override
	public void run(){
		while(true){
			String string3d = Double.toString(Timer.getFPGATimestamp() * 1000);
			for(SubsystemMotors subsystem : subsystemMotors){
				String subsystemName = subsystem.getSubsystemName();
				String subsystemString = subsystemName;
				boolean print = false;
				for(CANSparkMax motor : subsystem.getMotorInfo()) {
					temp = 0.0;
					current = 0.0;
					attempts = 0;
					while(((temp = motor.getMotorTemperature()) == 0.0 || (current = motor.getOutputCurrent()) == 0.0) && attempts++ <= 3){}
					if(temp > 40.0){
						print = true;
					}
					subsystemString += "; " + current + "<>" + temp;
				}
				if(count%10 == 0 && print) {
					LOGGER.info(subsystemName + " : " + subsystemString.replaceAll(subsystemName, "").replaceAll("<>", ", ").replaceFirst(";", ""));
				}
				if(subsystem instanceof Chassis){
					double max = 0.0;
					double average = 0.0;
					for(CANSparkMax motor : subsystem.getMotorInfo()){
						double temp = motor.getMotorTemperature();
						average += temp;
						max = temp > max ? temp : max;
					}
					average = average / subsystem.getMotorInfo().size();
					SmartDashboard.putNumber("Chassis Max Temp", max);
					SmartDashboard.putNumber("Chassis Average Temp", average);
					if(max > 80.0){
						OI.getInstance().vibrateOverheat();
					} else {
						OI.getInstance().endVibrate();
					}
				}
				string3d += ", " + subsystemString;
			}
			entry.setString(string3d);
			count++;
			try {

				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public interface SubsystemMotors {
		ArrayList<CANSparkMax> getMotorInfo();
		String getSubsystemName();
	}
}
