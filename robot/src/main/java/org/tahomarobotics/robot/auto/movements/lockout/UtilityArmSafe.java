package org.tahomarobotics.robot.auto.movements.lockout;

import edu.wpi.first.wpilibj.command.Command;
import org.tahomarobotics.robot.utilityarm.UtilityArm;

public class UtilityArmSafe extends Command {

	private final UtilityArm arm = UtilityArm.getInstance();

	@Override
	protected boolean isFinished() {
		return arm.getAverageAngle() > 120.0;
	}

}
