package org.tahomarobotics.robot.auto.movements.lockout;

import edu.wpi.first.wpilibj.command.Command;
import org.tahomarobotics.robot.alphaarm.AlphaArm;

public class AlphaArmSafe extends Command {

	private final AlphaArm arm = AlphaArm.getInstance();

	@Override
	protected boolean isFinished() {
		return arm.getAngle() > 30.0;
	}
}
