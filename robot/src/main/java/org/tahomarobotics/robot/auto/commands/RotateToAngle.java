package org.tahomarobotics.robot.auto.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.tahomarobotics.robot.chassis.Chassis;
import org.tahomarobotics.robot.motion.MotionController;
import org.tahomarobotics.robot.motion.MotionState;
import org.tahomarobotics.robot.path.PathBuilder;
import org.tahomarobotics.robot.state.RobotState;

public class RotateToAngle extends Command {

	private static final Chassis chassis = Chassis.getInstance();

	private final double angle;

	private static final RobotState robotState = RobotState.getInstance();

	private double kP = 0.006;
	private double kV = 0.000;
	private double kI = 0.005;

	private final MotionController motionController = new MotionController(kP, kV, kI,
			0,0, 7);


	private final MotionState motionSetpoint = new MotionState();
	private final MotionState currentMotionState = new MotionState();

	public RotateToAngle(/*PathBuilder.Mirror mirror,*/ double finalAngle){

		requires(chassis);
//		angle = PathBuilder.advandcedMirrorAngle(finalAngle, mirror);
		angle = finalAngle;
	}

	@Override
	protected boolean isFinished() {

		double elapsedTime = Timer.getFPGATimestamp();

		currentMotionState.setPosition(robotState.getHeading());
		currentMotionState.setVelocity(robotState.getRotVel());

		motionSetpoint.setPosition(angle);
		motionSetpoint.setVelocity(0.0);

		SmartDashboard.putNumber("rotate to angle current angle", robotState.getHeading());

		double rotPower = motionController.update(elapsedTime, currentMotionState, motionSetpoint);

		chassis.setPower(0.0, rotPower);

		return motionController.onTarget();
	}


}
