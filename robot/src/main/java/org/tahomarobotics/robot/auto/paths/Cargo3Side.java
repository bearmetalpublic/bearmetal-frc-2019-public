package org.tahomarobotics.robot.auto.paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.auto.AutoConstants;
import org.tahomarobotics.robot.auto.movements.LowPlace;
import org.tahomarobotics.robot.chassis.ChassisPathCommand;
import org.tahomarobotics.robot.chassis.ChassisPathCommandGroup;
import org.tahomarobotics.robot.path.PathActions;
import org.tahomarobotics.robot.path.PathBuilder;
import org.tahomarobotics.robot.state.Pose2D;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.vision.SendPhoneTargetCommand;
import org.tahomarobotics.robot.vision.VisionCommandMessage;

public class Cargo3Side extends ChassisPathCommandGroup {
    private static final Logger LOGGER = LoggerFactory.getLogger(RocketDoubleHatchBall.class);

    public static final double MAX_SPEED = 120;

    private static final Pose2D initialPose = new Pose2D(28, 113.912, 180.0);
    private final Pose2D startingPose;

    public Cargo3Side(PathBuilder.Mirror mirror) {

        startingPose = PathBuilder.mirrorPose2D(initialPose, mirror);

        ChassisPathCommand prior = null;
        addPath(prior = new PlaceFirstHatch(mirror, initialPose));
//        addPath(prior = new CollectSecondHatchA(mirror, prior.getFinalPose()));
//        addPath(prior = new CollectSecondHatchB(mirror, prior.getFinalPose()));
//        addPath(prior = new PlaceSecondHatchA(mirror, prior.getFinalPose()));
//        addPath(prior = new PlaceSecondHatchB(mirror, prior.getFinalPose()));
//        addPath(prior = new CollectFirstCargoA(mirror, prior.getFinalPose()));
//        addPath(prior = new CollectFirstCargoB(mirror, prior.getFinalPose()));
//        addPath(prior = new PlaceFirstCargoA(mirror, prior.getFinalPose()));
//        addPath(prior = new PlaceFirstCargoB(mirror, prior.getFinalPose()));
    }

    private class PlaceFirstHatch extends ChassisPathCommand {
        public PlaceFirstHatch(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addLine(189, MAX_SPEED, new PathActions.PathAction(new LowPlace(), 0.25),
                    new PathActions.PathAction(new SendPhoneTargetCommand(
                            mirror == PathBuilder.Mirror.Both || mirror == PathBuilder.Mirror.X ? VisionCommandMessage.Sorting.RIGHTMOST : VisionCommandMessage.Sorting.LEFTMOST
                    ), 0.0));
            pathBuilder.addArcToPoint(280, 50, MAX_SPEED * 0.8);
        }
    }
    private class CollectSecondHatchA extends ChassisPathCommand {
        public CollectSecondHatchA(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(276.898, 73.939, MAX_SPEED);
        }
    }
    private class CollectSecondHatchB extends ChassisPathCommand {
        public CollectSecondHatchB(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Forward, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(226.073, 90.574, MAX_SPEED * 0.2);
            pathBuilder.addArcToPoint(174.443, 71.057, MAX_SPEED * 0.2);
            pathBuilder.addArcToPoint(17.086, 25.965, MAX_SPEED * 0.2);
        }
    }
    private class PlaceSecondHatchA extends ChassisPathCommand {
        public PlaceSecondHatchA(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(174.443, 71.057, MAX_SPEED);
            pathBuilder.addArcToPoint(226.073, 90.574, MAX_SPEED * 0.2);
            pathBuilder.addArcToPoint(276.898, 73.939, MAX_SPEED * 0.2);
            pathBuilder.addArcToPoint(286.011, 55.423, MAX_SPEED * 0.2);
        }
    }
    private class PlaceSecondHatchB extends ChassisPathCommand {
        public PlaceSecondHatchB(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Forward, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(281.773, 115.375, MAX_SPEED * 0.2);
        }
    }
    private class CollectFirstCargoA extends ChassisPathCommand {
        public CollectFirstCargoA(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(286.011, 55.423, MAX_SPEED * 0.4);

        }
    }
    private class CollectFirstCargoB extends ChassisPathCommand {
        public CollectFirstCargoB(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Forward, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(276.898, 73.939, MAX_SPEED * 0.4);
            pathBuilder.addArcToPoint(226.073, 90.574, MAX_SPEED * 0.4);
            pathBuilder.addArcToPoint(174.443, 71.057, MAX_SPEED * 0.4);
            pathBuilder.addArcToPoint(52.021, 63.961, MAX_SPEED * 0.4);
        }
    }
    private class PlaceFirstCargoA extends ChassisPathCommand {
        public PlaceFirstCargoA(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(174.443, 71.057, MAX_SPEED * 0.4);
            pathBuilder.addArcToPoint(226.073, 90.574, MAX_SPEED * 0.4);
            pathBuilder.addArcToPoint(271.014, 79.194, MAX_SPEED);
            pathBuilder.addArcToPoint(296.400, 34.225, MAX_SPEED);
        }
    }
    private class PlaceFirstCargoB extends ChassisPathCommand {
        public PlaceFirstCargoB(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Forward, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(300.874, 71.057, MAX_SPEED * 0.4);
            pathBuilder.addArcToPoint(303.523, 84.627, MAX_SPEED * 0.4);
            pathBuilder.addLine(30.748, MAX_SPEED * 0.5);
        }
    }

    /**
     * Command group initialize
     */
    @Override
    protected void initialize() {
        // reset robot pose to the start of the path
        RobotState.getInstance().resetRobotPose(startingPose.x, startingPose.y, startingPose.heading);
    }


    @Override
    public Pose2D getStartingPose() {
        return startingPose;
    }


}
