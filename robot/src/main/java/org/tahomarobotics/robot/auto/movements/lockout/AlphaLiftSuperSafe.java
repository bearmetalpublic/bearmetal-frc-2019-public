package org.tahomarobotics.robot.auto.movements.lockout;

import edu.wpi.first.wpilibj.command.Command;
import org.tahomarobotics.robot.alphalift.AlphaLift;

public class AlphaLiftSuperSafe extends Command {

	private final AlphaLift lift = AlphaLift.getInstance();

	@Override
	protected boolean isFinished() {
		return lift.getPosition() > 20.5;
	}
}
