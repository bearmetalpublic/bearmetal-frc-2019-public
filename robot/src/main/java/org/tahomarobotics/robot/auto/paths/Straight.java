package org.tahomarobotics.robot.auto.paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.chassis.ChassisPathCommand;
import org.tahomarobotics.robot.chassis.ChassisPathCommandGroup;
import org.tahomarobotics.robot.path.PathBuilder;
import org.tahomarobotics.robot.state.Pose2D;

public class Straight extends ChassisPathCommandGroup {
    private static final Logger LOGGER = LoggerFactory.getLogger(RocketDoubleHatchBall.class);

    public static final double MAX_SPEED = 100;

    private static final Pose2D initialPose = new Pose2D(65.5, 150.125, 0.0);

    private final Pose2D startingPose;

    public Straight(PathBuilder.Mirror mirror){
        startingPose = PathBuilder.mirrorPose2D(initialPose, mirror);
        addPath(new PlaceFirstHatch(mirror, startingPose));
    }

    private class PlaceFirstHatch extends ChassisPathCommand {
        public PlaceFirstHatch(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Forward, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addLine(50, MAX_SPEED);
        }
    }

    @Override
    public Pose2D getStartingPose() {
        return startingPose;
    }
}
