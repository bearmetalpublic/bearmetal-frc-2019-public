package org.tahomarobotics.robot.auto.paths;

import edu.wpi.first.wpilibj.command.CommandGroup;
import org.tahomarobotics.robot.path.PathBuilder;

public class AssistedRocket extends CommandGroup {

	public AssistedRocket(PathBuilder.Mirror mirror){
		addSequential(new RocketOffLevel2(mirror));
	}
}
