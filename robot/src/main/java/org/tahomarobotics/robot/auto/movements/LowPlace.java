package org.tahomarobotics.robot.auto.movements;

import edu.wpi.first.wpilibj.command.Command;
import org.tahomarobotics.robot.path.ActionIF;
import org.tahomarobotics.robot.utilityarm.UtilityArm;

public class LowPlace extends Command implements ActionIF {
	private final UtilityArm arm = UtilityArm.getInstance();

	@Override
	protected boolean isFinished() {
		System.out.println(arm.getAverageAngle());
		if(arm.getAverageAngle() < 5.0) {
			new LowPlaceFromStow().start();
			System.out.println("From Stow");
		}else {
			new LowPlaceFromCollect().start();
			System.out.println("From Collect");
		}
		return true;
	}
}
