package org.tahomarobotics.robot.auto.commands;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.CommandGroup;
import org.tahomarobotics.robot.OI;

public class DriverOverride extends Command {
    private final Command autoCommand;

    public DriverOverride(CommandGroup autoCommand){
        this.autoCommand = autoCommand;
    }

    @Override
    protected void initialize(){
        autoCommand.start();
    }

    @Override
    protected boolean isFinished() {
        if(OI.getInstance().manipButtonA()){
            autoCommand.cancel();
        }
        return autoCommand.isCompleted() || autoCommand.isCanceled();
    }

    @Override
    protected void interrupted() {
        autoCommand.cancel();
    }
}
