/*
 * Copyright 2019 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.auto.commands;

import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.tahomarobotics.robot.RobotConfiguration;
import org.tahomarobotics.robot.betalift.BetaLift;
import org.tahomarobotics.robot.betalift.BetaLiftConstants;
import org.tahomarobotics.robot.chassis.Chassis;
import org.tahomarobotics.robot.motion.MotionController;
import org.tahomarobotics.robot.motion.MotionProfile;
import org.tahomarobotics.robot.motion.MotionProfileFactory;
import org.tahomarobotics.robot.motion.MotionState;
import org.tahomarobotics.robot.state.ClimbIntentMode;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.util.ChartData;
import org.tahomarobotics.robot.util.Constants;
import org.tahomarobotics.robot.utilityarm.UtilityArm;
import org.tahomarobotics.robot.utilityarm.UtilityArmCollector;
import org.tahomarobotics.robot.utilityarm.UtilityArmConstants;


public class CoordinatedClimb extends Command {

    private final BetaLift betaLift = BetaLift.getInstance();

    private final UtilityArm utilityArm = UtilityArm.getInstance();

    private final UtilityArmCollector utilityArmCollector = UtilityArmCollector.getInstance();

    private final Chassis chassis = Chassis.getInstance();

    private double height, delay, liftDelay;
    private static final double ACCEL_TIME = 0.15;

    private static final double ARM_DELAY = 0.0;

    private static final double TIMEOUT = 6.0;

    private final ChartData betaLiftChart = new ChartData("Beta Lift Motion", "Time (Sec)", "Velocity (in/sec)",
            new String[] {"expected-vel", "actual-vel", "voltage", "expected-pos", "actual-pos"});

    private final ChartData armLiftChart = new ChartData("Arm Lift Motion", "Time (Sec)", "Velocity (deg/sec)",
            new String[] {"expected-vel", "actual-vel", "voltage", "expected-pos", "actual-pos"});

    private MotionProfile betaLiftProfile;
    private MotionProfile armLiftProfile;

    private final MotionState betaLiftSetpoint = new MotionState();
    private final MotionState armLiftSetpoint = new MotionState();

    private final MotionState betaLiftCurrentMotion = new MotionState();
    private final MotionState armLiftCurrentMotion = new MotionState();

    private static final double BETA_kP = RobotConfiguration.getConfigurationValue(1., 0.5);
    private static final double BETA_kV = RobotConfiguration.getConfigurationValue(0, 0);
    private static final double BETA_kI = RobotConfiguration.getConfigurationValue(1.0, 0.5);
    private static final double BETA_kFFV = BetaLiftConstants.kFFV;
    private static final double BETA_kFFA = BetaLiftConstants.kFFA_CLIMB;

    private static final double ARM_kP = RobotConfiguration.getConfigurationValue(1.5, 2.8); //1.5
    private static final double ARM_kV = RobotConfiguration.getConfigurationValue(.01, 0);
    private static final double ARM_kI = RobotConfiguration.getConfigurationValue(.09, 12.0); //1,4
    private static final double ARM_kFFV = UtilityArmConstants.kFFV;
    private static final double ARM_kFFA = UtilityArmConstants.kFFA_CLIMB * RobotConfiguration.getConfigurationValue(0.7, 0.9);

    private static final double TOLERANCE = 1.0;

    private final MotionController betaLiftController = new MotionController(BETA_kP, BETA_kV, BETA_kI, BETA_kFFV, BETA_kFFA, TOLERANCE);
    private final MotionController armLiftController = new MotionController(ARM_kP, ARM_kV, ARM_kI, ARM_kFFV, ARM_kFFA, TOLERANCE);

    private boolean isBetaLiftCompleted;
    private boolean isArmLiftCompleted;
    private boolean isSonicPassed;

    private static final double STATIC_ACCELERATION = Constants.GRAVITIONAL_ACCELERATION / Constants.METERS_PER_INCH;

    public CoordinatedClimb() {
        requires(betaLift);
        requires(utilityArm);
        requires(utilityArmCollector);
        requires(chassis);

    }

    @Override
    protected void initialize() {
        height = RobotState.getInstance().getClimbIntent().height;
        delay = RobotState.getInstance().getClimbIntent() == ClimbIntentMode.ClimbIntent.LEVEL2 ? 0.0 : ARM_DELAY;
        liftDelay = RobotState.getInstance().getClimbIntent() == ClimbIntentMode.ClimbIntent.LEVEL2 ? ARM_DELAY * 3: 0.0;
        double duration = RobotState.getInstance().getClimbIntent() == ClimbIntentMode.ClimbIntent.LEVEL2 ? 0.5 : .8;

        double maxVel = this.height / (duration - ACCEL_TIME);
        double MAX_ACC = maxVel/ACCEL_TIME;

        betaLiftProfile = MotionProfileFactory.createMotionProfile(MotionProfileFactory.Profile.Trapezoid, liftDelay, 2,
                this.height + 2.5, 0.0, 0.0, maxVel, MAX_ACC, 0.0);
        maxVel = this.height / (duration - ACCEL_TIME - delay);
        MAX_ACC = maxVel/(ACCEL_TIME-.05);

        armLiftProfile = MotionProfileFactory.createMotionProfile(MotionProfileFactory.Profile.Trapezoid, delay, 0, this.height-.5, 0.0, 0.0, maxVel, MAX_ACC, 0.0);

        setTimeout(betaLiftProfile.getEndTime() + TIMEOUT);

        isBetaLiftCompleted = false;
        betaLiftChart.clear();
        betaLiftController.reset();
        betaLiftSetpoint.setTime(0).setPosition(0).setVelocity(0).setAcceleration(0);

        isArmLiftCompleted = false;
        armLiftChart.clear();
        armLiftController.reset();
        armLiftSetpoint.setTime(0).setVelocity(0).setPosition(0).setAcceleration(0);

        utilityArmCollector.setPower(-0.4);

        first = true;
    }

    @Override
    protected void execute() {
        double voltage = 0;
        double deltaTime = timeSinceInitialized();
        double batteryVoltage = RobotController.getBatteryVoltage();

        // beta-lift actual motion
        betaLiftCurrentMotion.setTime(deltaTime);
        betaLiftCurrentMotion.setPosition(betaLift.getPosition());
        betaLiftCurrentMotion.setVelocity(betaLift.getVelocity());

        if(deltaTime >= liftDelay){
            // beta-lift control calculation
            isBetaLiftCompleted = !betaLiftProfile.getSetpoint(deltaTime, betaLiftSetpoint);
            betaLiftSetpoint.acceleration += STATIC_ACCELERATION;

            voltage = betaLiftController.update(deltaTime, betaLiftCurrentMotion, betaLiftSetpoint);
            betaLift.setPublicPower(voltage/batteryVoltage);

        }

        betaLiftChart.addData(new double[] { deltaTime, betaLiftSetpoint.velocity, betaLiftCurrentMotion.velocity, voltage, betaLiftSetpoint.position, betaLiftCurrentMotion.position } );

        // alpha arm lift actual motion
        armLiftCurrentMotion.setTime(deltaTime);
        armLiftCurrentMotion.setPosition(UtilityArmConstants.calculateHeight(utilityArm.getAverageAngle(), height));
        armLiftCurrentMotion.setVelocity(UtilityArmConstants.calculateVerticalVelocity(utilityArm.getAverageAngle(), utilityArm.getAverageAngularVelocity()));

        // alpha arm lift control calculation
        if (deltaTime >= delay) {
            isArmLiftCompleted = !armLiftProfile.getSetpoint(deltaTime, armLiftSetpoint);
            armLiftSetpoint.acceleration += STATIC_ACCELERATION;
            armLiftSetpoint.acceleration /= -Math.cos(Math.toRadians(UtilityArmConstants.calculateAngle(armLiftSetpoint.position, height) + UtilityArmConstants.ARM_BEND_ANGLE));
            if (armLiftCurrentMotion.position > height) {
                armLiftSetpoint.acceleration *= 0.5;
            }
            voltage = armLiftController.update(deltaTime, armLiftCurrentMotion, armLiftSetpoint);
            utilityArm.setPower(voltage/batteryVoltage);
        }

        if(first && isArmLiftCompleted){
            initTime = Timer.getFPGATimestamp();
            first = false;
        }else if (isArmLiftCompleted) {
            isSonicPassed = betaLift.sonicPassed();
            chassis.setPower(0.30, 0);
        }

        armLiftChart.addData(new double[] { deltaTime, armLiftSetpoint.velocity, armLiftCurrentMotion.velocity, voltage, armLiftSetpoint.position, armLiftCurrentMotion.position } );
    }
double initTime;
boolean first = true;
    @Override
    protected boolean isFinished() {
        return (isBetaLiftCompleted && betaLiftController.onTarget() && isArmLiftCompleted && armLiftController.onTarget() ) && (isTimedOut() || initTime + 1. < Timer.getFPGATimestamp());
    }


    @Override
    protected void end() {
        betaLift.setPublicPower(0);
        utilityArm.setPower(0);
        utilityArmCollector.setPower(0);
        chassis.setPower(0,0);

        SmartDashboard.putRaw("BetaLiftChartData", betaLiftChart.serialize());
        SmartDashboard.putRaw("UtilityArmChartData", armLiftChart.serialize());
    }

}