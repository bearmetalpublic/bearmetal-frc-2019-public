package org.tahomarobotics.robot.auto.paths;

import edu.wpi.first.wpilibj.command.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.OI;
import org.tahomarobotics.robot.alphacollector.PowerCollector;
import org.tahomarobotics.robot.auto.AutoConstants;
import org.tahomarobotics.robot.auto.Autonomous;
import org.tahomarobotics.robot.auto.commands.RotateToAngle;
import org.tahomarobotics.robot.auto.movements.LowPlace;
import org.tahomarobotics.robot.chassis.AutoDriveToTape;
import org.tahomarobotics.robot.chassis.ChassisPathCommand;
import org.tahomarobotics.robot.chassis.ChassisPathCommandGroup;
import org.tahomarobotics.robot.chassis.TeleopDriveCommand;
import org.tahomarobotics.robot.path.PathActions;
import org.tahomarobotics.robot.path.PathBuilder;
import org.tahomarobotics.robot.state.Pose2D;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.state.UpdateRobotPose;
import org.tahomarobotics.robot.state.UpdateRobotXY;
import org.tahomarobotics.robot.vision.CamMode;
import org.tahomarobotics.robot.vision.Limelight;
import org.tahomarobotics.robot.vision.SendPhoneTargetCommand;
import org.tahomarobotics.robot.vision.VisionCommandMessage;

public class Cargo2Front1Side extends ChassisPathCommandGroup {
    private static final Logger LOGGER = LoggerFactory.getLogger(RocketDoubleHatchBall.class);

    public static final double MAX_SPEED = 130;


    private static final Pose2D initialPose = new Pose2D(46.175, 117.146, 0.0);
    private static final Pose2D afterDrop = new Pose2D(76.175, 117.146, 0.0);
    private static final Pose2D afterFirstHatch = new Pose2D(198.675, 150.146, 0);
    private static final Pose2D afterGrabSecondHatch = new Pose2D(17.375, 25.736, 180);

    private final Pose2D startingPose;
    private final Pose2D afterFall;
    private final Pose2D secondPose;
    private final Pose2D thirdPose;

    private final Limelight.VisionModes visionMode1;
    private final Limelight.VisionModes visionMode2;

    public Cargo2Front1Side(PathBuilder.Mirror mirror) {

        startingPose = PathBuilder.mirrorPose2D(initialPose, mirror);
        afterFall = PathBuilder.mirrorPose2D(afterDrop, mirror);
        secondPose = PathBuilder.mirrorPose2D(afterFirstHatch, mirror);
        thirdPose = PathBuilder.mirrorPose2D(afterGrabSecondHatch, mirror);

        visionMode1 = mirror == PathBuilder.Mirror.Both || mirror == PathBuilder.Mirror.X ?
                Limelight.VisionModes.RIGHT : Limelight.VisionModes.LEFT;
        visionMode2 = mirror == PathBuilder.Mirror.Both || mirror == PathBuilder.Mirror.X ?
                Limelight.VisionModes.LEFT : Limelight.VisionModes.RIGHT;

        ChassisPathCommand prior = null;
        addPath(prior = new PlaceFirstHatch(mirror, initialPose));
        addPath(prior = new PlaceFirstHatchB(mirror, prior.getFinalPose()));
        addSequential(new UpdateRobotPose(afterFall));
        addPath(prior = new PlaceFirstHatchC(mirror, afterDrop));
        addSequential(new AutoDriveToTape(1.0, 30));
        addSequential(new UpdateRobotXY(secondPose));
        addPath(prior = new CollectSecondHatchA(mirror, afterFirstHatch));
        addPath(prior = new CollectSecondHatchB(mirror, prior.getFinalPose()));
        addSequential(new AutoDriveToTape(1.0, 30));
        addSequential(new UpdateRobotPose(thirdPose));
        addPath(prior = new PlaceSecondHatchA(mirror, afterGrabSecondHatch));
        addSequential(new RotateToAngle(90));
        addSequential(new AutoDriveToTape(1.0, 30));
    }

    private class PlaceFirstHatch extends ChassisPathCommand {
        public PlaceFirstHatch(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Forward, mirror, initialPose, 18.0);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addLine(20, 40, new PathActions.PathAction(new LowPlace(), 0));
        }
    }
    private class PlaceFirstHatchB extends ChassisPathCommand {
        public PlaceFirstHatchB(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Reversed, mirror, initialPose, 18.0);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addLine(7, 30);

        }
    }
    private class PlaceFirstHatchC extends ChassisPathCommand {
        public PlaceFirstHatchC(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Forward, mirror, initialPose, 20, true);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addLine(40, 55);
            pathBuilder.addArcToPoint(143.957,141.106, 50, new PathActions.PathAction(new CamMode(Limelight.VisionModes.RIGHT), 0.0));
            pathBuilder.addArcToPoint(166,155.34, 45);
        }
    }
    private class CollectSecondHatchA extends ChassisPathCommand {
        public CollectSecondHatchA(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Reversed, mirror, initialPose, 22);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(159, 168, MAX_SPEED * .8, new PathActions.PathAction(new PowerCollector(1.0), 0.0, false));
        }
    }
    private class CollectSecondHatchB extends ChassisPathCommand {
        public CollectSecondHatchB(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Forward, mirror, initialPose,25,  true);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(120, 40, 100, new PathActions.PathAction(new CamMode(Limelight.VisionModes.LOW), 0.0));
            pathBuilder.addLine(10, 100);
        }
    }
    private class PlaceSecondHatchA extends ChassisPathCommand {
        public PlaceSecondHatchA(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addLine(52, MAX_SPEED);
            pathBuilder.addArcToPoint(132.366, 44.654, MAX_SPEED * .85, new PathActions.PathAction(new CamMode(Limelight.VisionModes.LEFT), 0.0));
            pathBuilder.addArcToPoint(258, 70, MAX_SPEED * .75);
        }
    }


    /**
     * Command group initialize
     */
    @Override
    protected void initialize() {
        // reset robot pose to the start of the path
        RobotState.getInstance().resetRobotPose(startingPose.x, startingPose.y, startingPose.heading);
    }


    @Override
    public Pose2D getStartingPose() {
        return startingPose;
    }


}
