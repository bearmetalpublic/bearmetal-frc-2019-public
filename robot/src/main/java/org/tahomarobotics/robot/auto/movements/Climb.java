package org.tahomarobotics.robot.auto.movements;

import edu.wpi.first.wpilibj.command.CommandGroup;
import org.tahomarobotics.robot.auto.commands.CoordinatedClimb;
import org.tahomarobotics.robot.betalift.BetaLiftCommand;
import org.tahomarobotics.robot.chassis.DriverForward;

public class Climb extends CommandGroup {
	public Climb(){
		addSequential(new CoordinatedClimb());
		addParallel(new BetaLiftCommand(1));
		addSequential(new DriverForward());
//		addParallel(new AlphaLiftCommand(1));
	}
}
