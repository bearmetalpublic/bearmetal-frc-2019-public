/*
 * Copyright 2019 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.auto.movements;

import edu.wpi.first.wpilibj.command.CommandGroup;
import org.tahomarobotics.robot.alphaarm.AlphaArm;
import org.tahomarobotics.robot.alphaarm.AlphaArmCommand;
import org.tahomarobotics.robot.alphacollector.AlphaCollector;
import org.tahomarobotics.robot.alphalift.AlphaLift;
import org.tahomarobotics.robot.alphalift.AlphaLiftCommand;
import org.tahomarobotics.robot.auto.movements.lockout.AlphaArmSafe;
import org.tahomarobotics.robot.auto.movements.lockout.AlphaArmSuperSafe;
import org.tahomarobotics.robot.auto.movements.lockout.AlphaLiftSuperSafe;
import org.tahomarobotics.robot.utilityarm.UtilityArmCommand;

public class CargoshipCargo extends CommandGroup {

	public CargoshipCargo(){
		addParallel(new AlphaLiftCommand(AlphaLift.AlphaLiftPosition.CARGO_SHIP,
				AlphaCollector.IS_CENTER_COLLECTOR ? true : false));
		addSequential(new AlphaLiftSuperSafe());
		addParallel(new AlphaArmCommand(AlphaArm.AlphaArmPosition.HATCH));
		addSequential(new AlphaArmSuperSafe());
		addParallel(new UtilityArmCommand(UtilityArmCommand.Angle.STOW));
		addSequential(new AlphaArmCommand(AlphaArm.AlphaArmPosition.CARGO));
	}
}
