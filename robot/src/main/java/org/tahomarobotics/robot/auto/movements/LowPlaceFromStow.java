/*
 * Copyright 2019 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.auto.movements;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.CommandGroup;
import org.tahomarobotics.robot.alphaarm.AlphaArm;
import org.tahomarobotics.robot.alphaarm.AlphaArmCommand;
import org.tahomarobotics.robot.alphalift.AlphaLift;
import org.tahomarobotics.robot.alphalift.AlphaLiftCommand;
import org.tahomarobotics.robot.auto.movements.lockout.AlphaArmSafe;
import org.tahomarobotics.robot.auto.movements.lockout.AlphaArmSuperSafe;

public class LowPlaceFromStow extends CommandGroup {

	public LowPlaceFromStow(){

		addParallel(new AlphaArmCommand(AlphaArm.AlphaArmPosition.BOTTOM_HATCH));
		addSequential(new AlphaArmSuperSafe());
		addParallel(new AlphaLiftCommand(AlphaLift.AlphaLiftPosition.BOTTOM));
		//Start other commands once a command is complete even while another command is still running
//		addSequential(new StartWhenComplete(new Command[]{new AlphaLiftCommand(AlphaLift.AlphaLiftPosition.BOTTOM)}, new AlphaArmSafe()));
//		addSequential(new AlphaArmSafe());
//		addSequential(new AlphaLiftCommand(AlphaLift.AlphaLiftPosition.BOTTOM));
	}
}
