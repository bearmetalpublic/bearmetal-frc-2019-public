/*
 * Copyright 2019 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.auto.movements;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.CommandGroup;
import org.tahomarobotics.robot.alphaarm.AlphaArm;
import org.tahomarobotics.robot.alphaarm.AlphaArmCommand;
import org.tahomarobotics.robot.alphacollector.AlphaCollector;
import org.tahomarobotics.robot.alphacollector.OpenCollectorCommand;
import org.tahomarobotics.robot.alphalift.AlphaLift;
import org.tahomarobotics.robot.alphalift.AlphaLiftCommand;
import org.tahomarobotics.robot.auto.movements.lockout.AlphaArmSafe;
import org.tahomarobotics.robot.auto.movements.lockout.AlphaLiftSafe;
import org.tahomarobotics.robot.auto.movements.lockout.UtilityArmSafe;
import org.tahomarobotics.robot.state.SwitchPlaceMode;
import org.tahomarobotics.robot.utilityarm.UtilityArmCommand;

public class CollectCargo extends CommandGroup {

	public CollectCargo(){

		if (AlphaCollector.IS_CENTER_COLLECTOR) {
			addParallel(new OpenCollectorCommand(OpenCollectorCommand.State.OPEN));
			addParallel(new AlphaArmCommand(AlphaArm.AlphaArmPosition.SAFE_MOVEMENT));
			addParallel(new AlphaLiftCommand(AlphaLift.AlphaLiftPosition.BALL_COLLECT, true));
			addSequential(new AlphaLiftSafe());
			addSequential(new AlphaArmSafe());
			addParallel(new UtilityArmCommand(UtilityArmCommand.Angle.COLLECTOR));
			addSequential(new UtilityArmSafe());
			addSequential(new AlphaArmCommand(AlphaArm.AlphaArmPosition.BALL_COLLECT));

//			Command[] utilCommands = new Command[]{new AlphaArmCommand(AlphaArm.AlphaArmPosition.BALL_COLLECT, 200)};
//			Command[] alphaCommands = new Command[]{new UtilityArmCommand(UtilityArmCommand.Angle.COLLECTOR), new StartWhenComplete(utilCommands, new UtilityArmSafe())};
//			addSequential(new StartWhenComplete(alphaCommands, new AlphaArmSafe(), new AlphaLiftSafe()));
			addSequential(new SwitchPlaceMode(SwitchPlaceMode.PlaceMode.BALL));

		} else {

			addSequential(new AlphaLiftCommand(AlphaLift.AlphaLiftPosition.SAFE, true));
			//Made alpha arm command parallel to utility arm command
			addParallel(new AlphaArmCommand(AlphaArm.AlphaArmPosition.SAFE_MOVEMENT));
			addParallel(new UtilityArmCommand(UtilityArmCommand.Angle.COLLECTOR));
			//TODO: Investigate why utility arm safe exists

			//addSequential(new StartWhenComplete(new UtilityArmSafe(), new Command[]{new AlphaLiftCommand(AlphaLift.AlphaLiftPosition.BALL_COLLECT, true), new AlphaArmCommand(AlphaArm.AlphaArmPosition.BALL_COLLECT, 200)}));
//			addSequential(new UtilityArmSafe());
//			addParallel(new AlphaLiftCommand(AlphaLift.AlphaLiftPosition.BALL_COLLECT, true));
//			addSequential(new AlphaArmCommand(AlphaArm.AlphaArmPosition.BALL_COLLECT, 200));
		}
	}
}
