package org.tahomarobotics.robot.auto.paths;

import edu.wpi.first.wpilibj.command.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.OI;
import org.tahomarobotics.robot.alphacollector.PowerCollector;
import org.tahomarobotics.robot.auto.movements.LowPlace;
import org.tahomarobotics.robot.chassis.AutoDriveToTape;
import org.tahomarobotics.robot.chassis.ChassisPathCommand;
import org.tahomarobotics.robot.chassis.ChassisPathCommandGroup;
import org.tahomarobotics.robot.chassis.TeleopDriveCommand;
import org.tahomarobotics.robot.path.PathActions;
import org.tahomarobotics.robot.path.PathBuilder;
import org.tahomarobotics.robot.state.*;
import org.tahomarobotics.robot.vision.CamMode;
import org.tahomarobotics.robot.vision.Limelight;

public class DoubleRocketOffLevel2 extends ChassisPathCommandGroup {
	private static final Logger LOGGER = LoggerFactory.getLogger(RocketDoubleHatchBall.class);

	public static final double MAX_SPEED = 120;


	private static final Pose2D initialPose = new Pose2D(46.175, 117.146, 0.0);
	private static final Pose2D afterDrop = new Pose2D(76.175, 117.146, 0.0);
	private static final Pose2D afterFirstHatch = new Pose2D(199.992, 32.360, -45.33);
	private static final Pose2D afterGrabSecondHatch = new Pose2D(17.375, 25.736, -180);
	private static final Pose2D afterSecondHatch = new Pose2D(225.774, 32.36, -145);

	private final Pose2D startingPose;
	private final Pose2D afterFall;
	private final Pose2D secondPose;
	private final Pose2D thirdPose;
	private final Pose2D fourthPose;

	private final Limelight.VisionModes visionMode1;
	private final Limelight.VisionModes visionMode2;

	public DoubleRocketOffLevel2(PathBuilder.Mirror mirror) {

		startingPose = PathBuilder.mirrorPose2D(initialPose, mirror);
		afterFall = PathBuilder.mirrorPose2D(afterDrop, mirror);
		secondPose = PathBuilder.mirrorPose2D(afterFirstHatch, mirror);
		thirdPose = PathBuilder.mirrorPose2D(afterGrabSecondHatch, mirror);
		fourthPose = PathBuilder.mirrorPose2D(afterSecondHatch, mirror);

		visionMode1 = mirror == PathBuilder.Mirror.Both || mirror == PathBuilder.Mirror.X ?
				Limelight.VisionModes.RIGHT : Limelight.VisionModes.LEFT;
		visionMode2 = mirror == PathBuilder.Mirror.Both || mirror == PathBuilder.Mirror.X ?
				Limelight.VisionModes.LEFT : Limelight.VisionModes.RIGHT;

		ChassisPathCommand prior = null;
		addPath(prior = new PlaceFirstHatch(mirror, initialPose));
		addParallel(new TeleopDriveCommand());
		addParallel(new CamMode(Limelight.VisionModes.RIGHT));
		addSequential(new Command() {

			@Override
			protected boolean isFinished() {
				return OI.getInstance().driverButtonA();
			}
		});
		addSequential(new AutoDriveToTape(.95, 30, 2.5, -4, mirror));
		addSequential(new UpdateRobotXY(secondPose));
		addParallel(new PowerCollector(1.0, .7));
		addPath(prior = new CollectSecondHatchA(mirror, afterFirstHatch));
		addParallel(new TeleopDriveCommand());
		addParallel(new CamMode(Limelight.VisionModes.LOW));
		addSequential(new Command() {

			@Override
			protected boolean isFinished() {
				return OI.getInstance().driverButtonA();
			}
		});
		addSequential(new UpdateRobotXY(thirdPose));
		addPath(prior = new PlaceSecondHatchA(mirror, afterGrabSecondHatch));
		addParallel(new TeleopDriveCommand());
		addParallel(new CamMode(Limelight.VisionModes.LEFT));
		addSequential(new Command() {

			@Override
			protected boolean isFinished() {
				return OI.getInstance().driverButtonA();
			}
		});
		addSequential(new AutoDriveToTape(1.0, 30));
		addParallel(new PowerCollector(1.0, 2));
		addSequential(new UpdateRobotPose(fourthPose));
		addPath(new Backup(mirror, afterSecondHatch));
	}

	private class PlaceFirstHatch extends ChassisPathCommand {
		public PlaceFirstHatch(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Forward, mirror, initialPose, 20.0);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addLine(20, 80, new PathActions.PathAction(new LowPlace(), .25));
			pathBuilder.addLine(50, 80, new PathActions.PathAction(new CamMode(Limelight.VisionModes.RIGHT), 0.0));

		}
	}

	private class CollectSecondHatchA extends ChassisPathCommand {
		public CollectSecondHatchA(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addLine(6, MAX_SPEED, new PathActions.PathAction(new CamMode(Limelight.VisionModes.LOW), 0.0));
		}
	}

	private class PlaceSecondHatchA extends ChassisPathCommand {
		public PlaceSecondHatchA(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Reversed, mirror, initialPose, 28);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addArcToPoint(178.7, 60, MAX_SPEED);
			pathBuilder.addArcToPoint(290, 30.613, MAX_SPEED, new PathActions.PathAction(new CamMode(Limelight.VisionModes.LEFT), 0.0));
		}
	}

	private class Backup extends ChassisPathCommand {
		public Backup(PathBuilder.Mirror mirror, Pose2D initialPose) {
			super(PathBuilder.PathDirection.Reversed, mirror, initialPose, 28);
		}

		@Override
		protected void createPath(PathBuilder pathBuilder) {
			pathBuilder.addLine(6, MAX_SPEED, new PathActions.PathAction(new CamMode(Limelight.VisionModes.LEFT), 0.0));
		}
	}


	/**
	 * Command group initialize
	 */
	@Override
	protected void initialize() {
		// reset robot pose to the start of the path
		RobotState.getInstance().resetRobotPose(startingPose.x, startingPose.y, startingPose.heading);
	}


	@Override
	public Pose2D getStartingPose() {
		return startingPose;
	}

}
