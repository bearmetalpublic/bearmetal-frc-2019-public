/*
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.auto;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.InitIF;
import org.tahomarobotics.robot.UpdateIF;
import org.tahomarobotics.robot.auto.commands.DriverOverride;
import org.tahomarobotics.robot.auto.paths.*;
import org.tahomarobotics.robot.chassis.ChassisPathCommandGroup;
import org.tahomarobotics.robot.path.PathBuilder.Mirror;

import org.tahomarobotics.robot.state.Pose2D;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.utilityarm.UtilityArmClimbCommand;

/**
 * The Autonomous class is responsible for selection of the autonomous command
 * and then starting and stopping it during play.
 *
 */
public class Autonomous implements UpdateIF, InitIF {

    private static final Logger LOGGER = LoggerFactory.getLogger(Autonomous.class);

    private enum StartPosition {
        LEFT,
        RIGHT
    }

    private enum AutoSelections {
        CARGO_3_SIDE,
        CARGO_2_FRONT_1_SIDE,
        CARGO_1_FRONT_2_SIDE,
        ROCKET_DOUBLE_HATCH_BALL,
        DOUBLE_HATCH_ROCKET,
        ASSISTED_DOUBLE_ROCKET,
        ROCKET_OFF_LEVEL_2,
        STRAIGHT,
        NO_OP
    }

    private static final Autonomous INSTANCE = new Autonomous();

    private final RobotState robotState = RobotState.getInstance();

    private ChassisPathCommandGroup selectedAutonomousPath;
    private Command selectedAutonomousCommand;
    private AutoSelections autoSelection;
    private Mirror mirror = Mirror.None;

    private final SendableChooser<AutoSelections> chooser = new SendableChooser<>();
    private final SendableChooser<StartPosition> posChooser = new SendableChooser<>();

    private Autonomous() {
        chooser.setDefaultOption(AutoSelections.CARGO_3_SIDE.toString() , AutoSelections.CARGO_3_SIDE);
        chooser.addOption(AutoSelections.CARGO_2_FRONT_1_SIDE.toString() , AutoSelections.CARGO_2_FRONT_1_SIDE);
        chooser.addOption(AutoSelections.CARGO_1_FRONT_2_SIDE.toString() , AutoSelections.CARGO_1_FRONT_2_SIDE);
        chooser.addOption(AutoSelections.ROCKET_DOUBLE_HATCH_BALL.toString() , AutoSelections.ROCKET_DOUBLE_HATCH_BALL);
        chooser.addOption(AutoSelections.DOUBLE_HATCH_ROCKET.toString() , AutoSelections.DOUBLE_HATCH_ROCKET);
        chooser.addOption(AutoSelections.ASSISTED_DOUBLE_ROCKET.toString(), AutoSelections.ASSISTED_DOUBLE_ROCKET);
        chooser.addOption(AutoSelections.ROCKET_OFF_LEVEL_2.toString(), AutoSelections.ROCKET_OFF_LEVEL_2);
        chooser.addOption(AutoSelections.STRAIGHT.toString(), AutoSelections.STRAIGHT);
        chooser.addOption(AutoSelections.NO_OP.toString(), AutoSelections.NO_OP);

        posChooser.setDefaultOption("Left", StartPosition.LEFT);
        posChooser.addOption("Right", StartPosition.RIGHT);

        SmartDashboard.putData("PathChooser", chooser);
        SmartDashboard.putData("Position", posChooser);
    }

    /**
     * Returns the Autonomous singleton
     */
    public static Autonomous getInstance() {
        return INSTANCE;
    }

    /**
     * Start running the selected autonomous command.
     */
    public void start() {
        if (selectedAutonomousPath != null) {
            selectedAutonomousPath.publish();
            selectedAutonomousCommand = new DriverOverride(selectedAutonomousPath);
            selectedAutonomousCommand.start();
        }
    }

    /**
     * Stop running the selected autonomous command.
     */
    public void stop() {
        if (selectedAutonomousCommand != null) {
            selectedAutonomousCommand.cancel();
        }
    }

    @Override
    public void init(){
    }

    @Override
    public void update(boolean isEnabled) {

        Mirror mirror = getMirror();
        AutoSelections autoSelection = chooser.getSelected();

        if (mirror == this.mirror && autoSelection == this.autoSelection) {
            return;
        }

        this.mirror = mirror;
        this.autoSelection = autoSelection;

        LOGGER.info(DriverStation.getInstance().getAlliance().toString() + " " + autoSelection + " " + mirror);

        switch(autoSelection){
            case CARGO_3_SIDE:
                selectedAutonomousPath = new Cargo3Side(mirror);
                break;
            case CARGO_2_FRONT_1_SIDE:
                selectedAutonomousPath = new Cargo2Front1Side(mirror);
                break;
            case CARGO_1_FRONT_2_SIDE:
                selectedAutonomousPath = new Cargo1Front2Side(mirror);
                break;
            case ROCKET_DOUBLE_HATCH_BALL:
                selectedAutonomousPath = new RocketDoubleHatchBall(mirror);
                break;
            case DOUBLE_HATCH_ROCKET:
                selectedAutonomousPath = new DoubleRocketOffLevel2(mirror);
                break;
            case ASSISTED_DOUBLE_ROCKET:
                selectedAutonomousPath = new DoubleHatchRocket(mirror);
                break;
            case ROCKET_OFF_LEVEL_2:
                selectedAutonomousPath = new RocketOffLevel2(mirror);
                break;
            case STRAIGHT:
                selectedAutonomousPath = new Straight(mirror);
                break;
            case NO_OP:
                selectedAutonomousPath = new NoOp();
                break;

        }

        selectedAutonomousPath.publish();

        Pose2D startingPose = selectedAutonomousPath.getStartingPose();
        RobotState.getInstance().resetRobotPose(startingPose.x, startingPose.y, startingPose.heading);
    }

    private Mirror getMirror() {
        DriverStation.Alliance alliance = DriverStation.getInstance().getAlliance();
        StartPosition startPosition = posChooser.getSelected();

        return alliance == DriverStation.Alliance.Blue ?
                (startPosition == StartPosition.LEFT ? Mirror.Y :  Mirror.None) :
                (startPosition == StartPosition.LEFT ? Mirror.X : Mirror.Both);
    }
}
