package org.tahomarobotics.robot.auto.paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.auto.AutoConstants;
import org.tahomarobotics.robot.chassis.ChassisPathCommand;
import org.tahomarobotics.robot.chassis.ChassisPathCommandGroup;
import org.tahomarobotics.robot.motion.Motion2DProfileFactory;
import org.tahomarobotics.robot.path.PathBuilder;
import org.tahomarobotics.robot.state.Pose2D;
import org.tahomarobotics.robot.state.RobotState;

public class Cargo1Front2Side extends ChassisPathCommandGroup {
    private static final Logger LOGGER = LoggerFactory.getLogger(RocketDoubleHatchBall.class);

    public static final double MAX_SPEED = 120;

    private static final Pose2D initialPose = new Pose2D(65.5, 150.125, 0.0);

    private final Pose2D startingPose;

    public Cargo1Front2Side(PathBuilder.Mirror mirror){
        startingPose = PathBuilder.mirrorPose2D(initialPose, mirror);

        ChassisPathCommand prior = null;
        addPath(prior = new PlaceFirstHatch(mirror, initialPose));
        addPath(prior = new CollectSecondHatchA(mirror, prior.getFinalPose()));
        addPath(prior = new CollectSecondHatchB(mirror, prior.getFinalPose()));
        addPath(prior = new PlaceSecondHatchA(mirror, prior.getFinalPose()));
        addPath(prior = new PlaceSecondHatchB(mirror, prior.getFinalPose()));
        addPath(prior = new CollectFirstCargoA(mirror, prior.getFinalPose()));
        addPath(prior = new CollectFirstCargoB(mirror, prior.getFinalPose()));
        addPath(prior = new PlaceFirstCargoA(mirror, prior.getFinalPose()));
        addPath(prior = new PlaceFirstCargoB(mirror, prior.getFinalPose()));
    }

    private class PlaceFirstHatch extends ChassisPathCommand {
        public PlaceFirstHatch(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Forward, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addLine(136.273, MAX_SPEED);
        }
    }

    private class CollectSecondHatchA extends ChassisPathCommand {
        public CollectSecondHatchA(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(182.844, 166.809, MAX_SPEED * 0.6);
        }
    }
    private class CollectSecondHatchB extends ChassisPathCommand {
        public CollectSecondHatchB(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Forward, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(78.256, 30.195, MAX_SPEED * 0.6);
            pathBuilder.addArcToPoint(17.086, 25.965, MAX_SPEED * 0.6);
        }
    }
    private class PlaceSecondHatchA extends ChassisPathCommand {
        public PlaceSecondHatchA(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(174.443, 71.057, MAX_SPEED * 0.7);
            pathBuilder.addArcToPoint(226.073, 90.574, MAX_SPEED * 0.6);
            pathBuilder.addArcToPoint(276.898, 73.939, MAX_SPEED * 0.6);
        }
    }
    private class PlaceSecondHatchB extends ChassisPathCommand {
        public PlaceSecondHatchB(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Forward, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(260.23, 115.375, MAX_SPEED * 0.6);
        }
    }
    private class CollectFirstCargoA extends ChassisPathCommand {
        public CollectFirstCargoA(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(276.898, 73.939, MAX_SPEED);

        }
    }
    private class CollectFirstCargoB extends ChassisPathCommand {
        public CollectFirstCargoB(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Forward, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(226.073, 90.574, MAX_SPEED * 0.4);
            pathBuilder.addArcToPoint(174.443, 71.057, MAX_SPEED * 0.4);
            pathBuilder.addArcToPoint(52.021, 63.961, MAX_SPEED * 0.4);
        }
    }
    private class PlaceFirstCargoA extends ChassisPathCommand {
        public PlaceFirstCargoA(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(174.443, 71.057, MAX_SPEED * 0.4);
            pathBuilder.addArcToPoint(226.073, 90.574, MAX_SPEED * 0.4);
            pathBuilder.addArcToPoint(276.898, 73.939, MAX_SPEED);
            pathBuilder.addArcToPoint(286.011, 55.423, MAX_SPEED);
        }
    }
    private class PlaceFirstCargoB extends ChassisPathCommand {
        public PlaceFirstCargoB(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Forward, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(281.773, 115.375, MAX_SPEED);
        }
    }

    /**
     * Command group initialize
     */
    @Override
    protected void initialize() {
        // reset robot pose to the start of the path
        RobotState.getInstance().resetRobotPose(startingPose.x, startingPose.y, startingPose.heading);
    }

    @Override
    public Pose2D getStartingPose() {
        return startingPose;
    }
}
