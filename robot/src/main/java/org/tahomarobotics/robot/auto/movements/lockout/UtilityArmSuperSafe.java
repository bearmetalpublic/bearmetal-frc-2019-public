package org.tahomarobotics.robot.auto.movements.lockout;

import edu.wpi.first.wpilibj.command.Command;
import org.tahomarobotics.robot.utilityarm.UtilityArm;

public class UtilityArmSuperSafe extends Command {

	private final UtilityArm arm = UtilityArm.getInstance();

	@Override
	protected boolean isFinished() {
		return arm.getAverageAngle() < 4.0;
	}

}
