package org.tahomarobotics.robot.auto.movements;

import edu.wpi.first.wpilibj.command.CommandGroup;
import org.tahomarobotics.robot.alphaarm.AlphaArm;
import org.tahomarobotics.robot.alphaarm.AlphaArmCommand;
import org.tahomarobotics.robot.alphalift.AlphaLift;
import org.tahomarobotics.robot.alphalift.AlphaLiftCommand;
import org.tahomarobotics.robot.auto.movements.lockout.UtilityArmSafe;
import org.tahomarobotics.robot.betalift.BetaLiftCommand;
import org.tahomarobotics.robot.betalift.SetHoldCommand;
import org.tahomarobotics.robot.state.ClimbIntentMode;
import org.tahomarobotics.robot.utilityarm.UtilityArmCommand;

public class PrepClimb extends CommandGroup {
   public PrepClimb(ClimbIntentMode.ClimbIntent intent){
       addSequential(new AlphaLiftCommand(AlphaLift.AlphaLiftPosition.BALL_COLLECT, true));
       addSequential(new AlphaArmCommand((AlphaArm.AlphaArmPosition.SAFE_MOVEMENT)));
       addSequential(new UtilityArmCommand(intent.utilArmAngle));
       addSequential(new ClimbIntentMode(intent));

       addSequential(new AlphaLiftCommand(4));
   }
}
