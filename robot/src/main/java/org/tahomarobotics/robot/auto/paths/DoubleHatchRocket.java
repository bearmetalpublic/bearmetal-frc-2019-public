package org.tahomarobotics.robot.auto.paths;

import edu.wpi.first.wpilibj.command.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.OI;
import org.tahomarobotics.robot.alphacollector.PowerCollector;
import org.tahomarobotics.robot.chassis.ChassisPathCommand;
import org.tahomarobotics.robot.chassis.ChassisPathCommandGroup;
import org.tahomarobotics.robot.chassis.TeleopDriveCommand;
import org.tahomarobotics.robot.path.PathActions;
import org.tahomarobotics.robot.path.PathBuilder;
import org.tahomarobotics.robot.state.Pose2D;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.state.UpdateRobotPose;

public class DoubleHatchRocket extends ChassisPathCommandGroup {
    private static final Logger LOGGER = LoggerFactory.getLogger(DoubleHatchRocket.class);

    public static final double MAX_SPEED = 100;

    private static final Pose2D initialPose = new Pose2D(28, 113.912, 180.0);
    private static final Pose2D afterFirstHatch = new Pose2D(198.274, 26.8, 28.77);
    private static final Pose2D afterGrabSecondHatch = new Pose2D(17.375, 25.736, 180);

    private final Pose2D startingPose;
    private final Pose2D secondPose;
    private final Pose2D thirdPose;

    public DoubleHatchRocket(PathBuilder.Mirror mirror){

        startingPose = PathBuilder.mirrorPose2D(initialPose, mirror);
        secondPose = PathBuilder.mirrorPose2D(afterFirstHatch, mirror);
        thirdPose = PathBuilder.mirrorPose2D(afterGrabSecondHatch, mirror);

        ChassisPathCommand prior = null;
        addPath(new PlaceFirstHatchA(mirror, initialPose));
        addParallel(new TeleopDriveCommand());
        addSequential(new Command(){

            @Override
            protected boolean isFinished() {
                return OI.getInstance().driverButtonA();
            }
        });
        addSequential(new UpdateRobotPose(secondPose));
        addPath(prior = new CollectSecondHatchA(mirror, afterFirstHatch));
        addPath(new CollectSecondHatchB(mirror, prior.getFinalPose()));
        addParallel(new TeleopDriveCommand());
        addSequential(new Command(){

            @Override
            protected boolean isFinished() {
                return OI.getInstance().driverButtonA();
            }
        });
        addSequential(new UpdateRobotPose(thirdPose));
        addPath(new PlaceSecondHatchA(mirror, afterGrabSecondHatch));
    }

    private class PlaceFirstHatchA extends ChassisPathCommand {
        public PlaceFirstHatchA(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Forward, mirror, initialPose, 30);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addLine(20, 25);
            pathBuilder.addArcToPoint(249.5, 81.867, MAX_SPEED);
            pathBuilder.addArcToPoint(323, 90.549, MAX_SPEED);
        }
    }


    private class CollectSecondHatchA extends ChassisPathCommand {
        public CollectSecondHatchA(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Reversed, mirror, initialPose, 15);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(292.738, 26.251, MAX_SPEED, new PathActions.PathAction(new PowerCollector(.5, 5.0), 0.0));
        }
    }

    private class CollectSecondHatchB extends ChassisPathCommand {
        public CollectSecondHatchB(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Forward, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint( 42.532, 274.057,MAX_SPEED * 0.6);
            pathBuilder.addArcToPoint( 54.471, 202.359,MAX_SPEED);
            pathBuilder.addArcToPoint( 25.335, 102.614,MAX_SPEED);
            pathBuilder.addLine(55, MAX_SPEED, new PathActions.PathAction(new PowerCollector(-.5, 5.0), 0.0));
        }
    }

    private class PlaceSecondHatchA extends ChassisPathCommand {
        public PlaceSecondHatchA(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addLine(100.627, MAX_SPEED);
            pathBuilder.addArcToPoint(128.761, 65.299, MAX_SPEED * 0.6);
        }
    }

    /**
     * Command group initialize
     */
    @Override
    protected void initialize() {
        // reset robot pose to the start of the path
        RobotState.getInstance().resetRobotPose(startingPose.x, startingPose.y, startingPose.heading);
    }


    @Override
    public Pose2D getStartingPose() {
        return startingPose;
    }

}