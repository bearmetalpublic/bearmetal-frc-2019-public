package org.tahomarobotics.robot.auto.paths;

import org.tahomarobotics.robot.chassis.ChassisPathCommandGroup;
import org.tahomarobotics.robot.state.Pose2D;

public class NoOp extends ChassisPathCommandGroup {

    private static final Pose2D pose = new Pose2D(65.5, 118, 0.0);

    @Override
    public Pose2D getStartingPose() {
        return pose;
    }
}
