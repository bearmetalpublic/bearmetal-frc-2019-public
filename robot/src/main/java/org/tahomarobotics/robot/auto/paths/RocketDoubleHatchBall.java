package org.tahomarobotics.robot.auto.paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.alphacollector.OpenCollectorCommand;
import org.tahomarobotics.robot.auto.AutoConstants;
import org.tahomarobotics.robot.auto.movements.LowPlace;
import org.tahomarobotics.robot.auto.movements.MaxRocket;
import org.tahomarobotics.robot.chassis.ChassisPathCommand;
import org.tahomarobotics.robot.chassis.ChassisPathCommandGroup;
import org.tahomarobotics.robot.path.PathActions;
import org.tahomarobotics.robot.path.PathBuilder;
import org.tahomarobotics.robot.state.Pose2D;
import org.tahomarobotics.robot.state.RobotState;

public class RocketDoubleHatchBall extends ChassisPathCommandGroup {
    private static final Logger LOGGER = LoggerFactory.getLogger(RocketDoubleHatchBall.class);

    public static final double MAX_SPEED = 110;

    private static final Pose2D initialPose = new Pose2D(66.595, 113.912, 180.0);

    private final Pose2D startingPose;

    public RocketDoubleHatchBall(PathBuilder.Mirror mirror){

        startingPose = PathBuilder.mirrorPose2D(initialPose, mirror);

        ChassisPathCommand prior = null;
        addPath(prior = new PlaceFirstHatchA(mirror, initialPose));
//        addPath(prior = new PlaceFirstHatchB(mirror, prior.getFinalPose()));
//        addPath(prior = new CollectSecondHatchA(mirror, prior.getFinalPose()));
//        addPath(prior = new CollectSecondHatchB(mirror, prior.getFinalPose()));
//        addPath(prior = new PlaceSecondHatchA(mirror, prior.getFinalPose()));
//        addPath(prior = new PlaceSecondHatchB(mirror, prior.getFinalPose()));
//        addPath(prior = new CollectFirstCargoA(mirror, prior.getFinalPose()));
//        addPath(prior = new CollectFirstCargoB(mirror, prior.getFinalPose()));
//        addPath(prior = new PlaceFirstCargoA(mirror, prior.getFinalPose()));
//        addPath(prior = new PlaceFirstCargoB(mirror, prior.getFinalPose()));
    }

    private class PlaceFirstHatchA extends ChassisPathCommand {
        public PlaceFirstHatchA(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addLine(50, MAX_SPEED);
            pathBuilder.addArcToPoint(239.020, 68.289, MAX_SPEED);
            pathBuilder.addArcToPoint(288.374, 79.054, MAX_SPEED, new PathActions.PathAction(new MaxRocket()));
            pathBuilder.addLine(17.865, MAX_SPEED);

        }
    }

    private class PlaceFirstHatchB extends  ChassisPathCommand {
        public PlaceFirstHatchB(PathBuilder.Mirror mirror, Pose2D initialPose){
            super(PathBuilder.PathDirection.Forward, mirror, initialPose);
        }


        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(286.238, 73.656, MAX_SPEED,
                    new PathActions.PathAction(new MaxRocket(), 0.0));
            pathBuilder.addArcToPoint(259.100, 10, MAX_SPEED * 0.7, new PathActions.PathAction(
                    new OpenCollectorCommand(OpenCollectorCommand.State.OPEN,
                            .5), .75, true));

        }
    }
    private class CollectSecondHatchA extends ChassisPathCommand {
        public CollectSecondHatchA(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addLine(10, MAX_SPEED * 0.3, new PathActions.PathAction(new LowPlace(), 0.0));
        }
    }

    private class CollectSecondHatchB extends ChassisPathCommand {
        public CollectSecondHatchB(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Forward, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addLine(94.151, MAX_SPEED);
            pathBuilder.addArcToPoint(67.336, 25.736, MAX_SPEED);
            pathBuilder.addLine(49.962, MAX_SPEED * 0.5);
        }
    }

    private class PlaceSecondHatchA extends ChassisPathCommand {
        public PlaceSecondHatchA(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addLine(49.962, MAX_SPEED);
            pathBuilder.addArcToPoint(193.241, 41.811, MAX_SPEED);
            pathBuilder.addLine(94.151, MAX_SPEED * 0.5);
        }
    }

    private class PlaceSecondHatchB extends ChassisPathCommand {
        public PlaceSecondHatchB(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Forward, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(269.987, 46.075, MAX_SPEED * 0.3);
            pathBuilder.addArcToPoint(259.1, 26.799, MAX_SPEED * 0.3);
        }
    }

    private class CollectFirstCargoA extends ChassisPathCommand {
        public CollectFirstCargoA(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(295.441, 23.678, MAX_SPEED * 0.3);
        }
    }

    private class CollectFirstCargoB extends ChassisPathCommand {
        public CollectFirstCargoB(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Forward, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint( 193.548, 52.430, MAX_SPEED * 0.7);
            pathBuilder.addArcToPoint(52.792, 70.396, MAX_SPEED * 0.5);
        }
    }

    private class PlaceFirstCargoA extends ChassisPathCommand{
        public PlaceFirstCargoA(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Reversed, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(240.21, 86.201, MAX_SPEED * 0.8);
        }
    }
    private class PlaceFirstCargoB extends ChassisPathCommand{
        public PlaceFirstCargoB(PathBuilder.Mirror mirror, Pose2D initialPose) {
            super(PathBuilder.PathDirection.Forward, mirror, initialPose);
        }

        @Override
        protected void createPath(PathBuilder pathBuilder) {
            pathBuilder.addArcToPoint(229.063, 66.380, MAX_SPEED * 0.6);
            pathBuilder.addLine(20.939, MAX_SPEED);
        }
    }

    /**
     * Command group initialize
     */
    @Override
    protected void initialize() {
        // reset robot pose to the start of the path
        RobotState.getInstance().resetRobotPose(startingPose.x, startingPose.y, startingPose.heading);
    }


    @Override
    public Pose2D getStartingPose() {
        return startingPose;
    }

}