package org.tahomarobotics.robot.auto.movements.lockout;

import edu.wpi.first.wpilibj.command.Command;
import org.tahomarobotics.robot.alphalift.AlphaLift;

public class AlphaLiftSafe extends Command {

	private final AlphaLift lift = AlphaLift.getInstance();

	@Override
	protected boolean isFinished() {
		return lift.getPosition() > 6;
	}
}
