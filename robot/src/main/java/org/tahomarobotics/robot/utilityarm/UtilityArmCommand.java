package org.tahomarobotics.robot.utilityarm;

import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.tahomarobotics.robot.RobotConfiguration;
import org.tahomarobotics.robot.motion.MotionController;
import org.tahomarobotics.robot.motion.MotionProfile;
import org.tahomarobotics.robot.motion.MotionProfileFactory;
import org.tahomarobotics.robot.motion.MotionState;
import org.tahomarobotics.robot.util.ChartData;

public class UtilityArmCommand extends Command {

    private boolean dontMove = false;

    private static final double MAX_VELOCITY = 750;
    private static final double MAX_ACCELERATION = 2500;

    private final double P = RobotConfiguration.getConfigurationValue(.01, .0);
    private final double I = RobotConfiguration.getConfigurationValue(0.01, 0.02);
    private final double v = RobotConfiguration.getConfigurationValue(.0, .0);

    public enum Angle {
        STOW(0),
        COLLECTOR(152),
        LEVEL2(173),
        LEVEL3(107);

        private final double angle;

        private Angle(double angle){
            this.angle = angle;
        }
    }
    private final ChartData chartData = new ChartData("UtilityArm", "Time", "Angle (Degree)",
            new String[] {"Wanted Velocity", "Actual Velocity", "Wanted Position", "Actual Position", "Voltage", "Wanted Acceleration", "Actual Acceleration"});
    //tolerance of position to a degree
    private final double TOLERANCE = 1;
    private final double TIMEOUT = 0.25;

    private final MotionController controller = new MotionController(P, v, I,
            UtilityArmConstants.kFFV, UtilityArmConstants.kFFA_UNLOADED, TOLERANCE);
    private double goal;
    private final UtilityArm utilityArm = UtilityArm.getInstance();
    private final MotionState currentState = new MotionState(),
                              setpoint = new MotionState();


    private boolean motionComplete, smartDashboard;
    private MotionProfile motionProfile;

    public UtilityArmCommand(Angle angle){
        this(angle.angle);
    }

    public UtilityArmCommand(double goal){
        requires(utilityArm);
        this.goal = goal;
    }

    public UtilityArmCommand(boolean useSmartDashboard){
        SmartDashboard.putNumber("UtilArm Goal", 90);
        this.goal = 0;
        smartDashboard = useSmartDashboard;
    }

    @Override
    protected void initialize() {

        dontMove = (goal == Angle.STOW.angle && utilityArm.getAverageAngle() < Angle.STOW.angle + 5);

        controller.reset();
        chartData.clear();

        if(smartDashboard){
            goal = SmartDashboard.getNumber("UtilArm Goal", 90);
        }

        motionProfile = MotionProfileFactory.createMotionProfile(MotionProfileFactory.Profile.Trapezoid, 0, utilityArm.getAverageAngle(),
                goal, utilityArm.getAverageAngularVelocity(), 0, MAX_VELOCITY, MAX_ACCELERATION, 0);

        setTimeout(motionProfile.getEndTime() + TIMEOUT);
        utilityArm.setActive();

    }

    @Override
    protected void execute() {
        if(dontMove)
            return;

        double elapsedTime = timeSinceInitialized();

        motionComplete = !motionProfile.getSetpoint(elapsedTime, setpoint);

        //Account for gravity by adding the acceleration of gravity
        setpoint.acceleration += UtilityArm.getInstance().calculateAccelGravity(setpoint);

        setpoint.acceleration = Math.toRadians(setpoint.acceleration);

        currentState.position = utilityArm.getAverageAngle();
        currentState.velocity = utilityArm.getAverageAngularVelocity();

        double output = controller.update(elapsedTime, currentState, setpoint);
        //System.out.println(currentState.velocity);
        double voltage = RobotController.getBatteryVoltage();
        utilityArm.setPower(output / voltage);

        chartData.addData(new double[] {elapsedTime, setpoint.velocity, currentState.velocity, setpoint.position, currentState.position, output * 10, 0, 0});
    }


    @Override
    protected boolean isFinished() {
        return ((motionComplete && controller.onTarget()) || isTimedOut()) || dontMove;
    }

    @Override
    protected void end(){
        utilityArm.setHold(setpoint.position);
        SmartDashboard.putRaw("UtilityArmChartData", chartData.serialize());
    }




}
