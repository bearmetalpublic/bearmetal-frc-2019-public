/*
 * Copyright 2019 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.utilityarm;

import org.tahomarobotics.robot.RobotConstants;
import org.tahomarobotics.robot.util.Constants;

public class UtilityArmConstants {

    // Transmission
    private static final double TICKS_PER_REV = 4096;
    private static final double STAGE_ONE = 60/10D;
    private static final double STAGE_TWO = 54/24D;
    private static final double STAGE_THREE = 60/16D;
    private static final double EFFICIENCY = 1;

    public static final double GEAR_RATIO = STAGE_ONE * STAGE_TWO * STAGE_THREE;

    //Pulse Per Inch count
    public static final double DEGREES_PER_REVOLUTION = GEAR_RATIO;
    public static final double TICKS_PER_DEGREE = DEGREES_PER_REVOLUTION / TICKS_PER_REV;

    public static final double ARM_BEND_ANGLE = 21.77;
    public static final double VERTICAL_ANGLE = ARM_BEND_ANGLE - 90;
    private static final double PIVOT_HEIGHT = 10.675;
    private static final double WHEEL_RADIUS = 1.25;
    private static final double ARM_LENGTH = 12.106;

    private static final double MOTOR_COUNT = 2;

    private static final double CLIMB_MASS = (117.9 * Constants.KILOGRAM_PER_LBMASS) / 4.0; // kg
    public static final double ARM_MASS = 5.903 * Constants.KILOGRAM_PER_LBMASS;

    public static final double LENGTH_COM = 8.334600;
    public static final double ANGLE_ADDITIONAL_COM = 15.9924;

    // voltage needed per unit of velocity (degrees/second)
    public static final double kFFV  = GEAR_RATIO/ Math.toDegrees(RobotConstants.kV);

    public static final double MOMENT_OF_INTERIA = 410.055 //M R^2
            * Constants.KILOGRAM_PER_LBMASS
            * Constants.METERS_PER_INCH
            * Constants.METERS_PER_INCH; // lbmass in^2 to kg m^2

    // voltage needed per unit of acceleration (inches/second^2)
    private static final double ARM_LENGTH_METERS = ARM_LENGTH * Constants.METERS_PER_INCH;
    public static final double kFFA_CLIMB = CLIMB_MASS *  ARM_LENGTH_METERS * RobotConstants.RESISTANCE / (GEAR_RATIO * RobotConstants.kT) * Constants.METERS_PER_INCH;
    public static final double kFFA_UNLOADED = MOMENT_OF_INTERIA * RobotConstants.RESISTANCE * 2.0 / (GEAR_RATIO * RobotConstants.kT * MOTOR_COUNT);


    public static final double ACCEL_OF_GRAVITY = 9.80665 / Constants.METERS_PER_INCH;

    public static double calculateHeight(double angle, double platformHeight) {

        double armHeight = platformHeight - PIVOT_HEIGHT + WHEEL_RADIUS;
        return armHeight - ARM_LENGTH * Math.sin(Math.toRadians(ARM_BEND_ANGLE + angle));
    }

    public static double calculateVerticalVelocity(double angle, double angularVelocity) {
        //=-ARM_LENGTH * COS(RADIANS(ARM_BEND_ANGLE+F3))*RADIANS(H3)
        return -ARM_LENGTH * Math.cos(Math.toRadians(ARM_BEND_ANGLE + angle)) * Math.toRadians(angularVelocity);
    }

    public static double calculateAngle(double height, double platformHeight) {
        double armHeight = platformHeight - PIVOT_HEIGHT + WHEEL_RADIUS;
        return 180 - ARM_BEND_ANGLE - Math.toDegrees(Math.asin((armHeight - height)/ARM_LENGTH));
    }

    public static double calculateAngularVelocity(double height, double verticalVelocity, double platformHeight) {
        double armHeight = platformHeight - PIVOT_HEIGHT + WHEEL_RADIUS;
        double k = (armHeight - height)/ARM_LENGTH;
        return Math.toDegrees(verticalVelocity / Math.sqrt(1-k*k) / ARM_LENGTH);
    }
}
