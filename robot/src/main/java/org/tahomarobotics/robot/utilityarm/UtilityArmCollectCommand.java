/*
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.utilityarm;

import edu.wpi.first.wpilibj.command.Command;

public class UtilityArmCollectCommand extends Command {

    public enum State {
        HIGH(1.0), MEDIUM(0.5), LOW(0.25), REVERSE(-1.0), OFF(0.0);
        public final double power;
        State(double power){
            this.power = power;
        }
    }

    private UtilityArmCollector utilityArmCollector = UtilityArmCollector.getInstance();

    private double power;


    public UtilityArmCollectCommand(State state){
        this(state, 1);
    }

    public UtilityArmCollectCommand(State state, double timeout) {
        requires(utilityArmCollector);
        power = state.power;
        setTimeout(timeout);
    }

    @Override
    protected boolean isFinished() {
        utilityArmCollector.setPower(power);
        return isTimedOut();
    }

}
