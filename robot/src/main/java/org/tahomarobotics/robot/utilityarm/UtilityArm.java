/*
 * Copyright 2019 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.utilityarm;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.tahomarobotics.robot.RobotConfiguration;
import org.tahomarobotics.robot.RobotMap;
import org.tahomarobotics.robot.UpdateIF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.*;
import org.tahomarobotics.robot.motion.MotionController;
import org.tahomarobotics.robot.motion.MotionState;
import org.tahomarobotics.robot.util.InitializeAngle;

import java.util.ArrayList;

public class UtilityArm extends Subsystem implements UpdateIF, InitIF, MotorInfo.SubsystemMotors {

    private static Logger LOGGER = LoggerFactory.getLogger(UtilityArm.class);

    private final ArrayList<CANSparkMax> motors = new ArrayList<>();

    private static double LEFT_MAG_OFFSET = RobotConfiguration.getConfigurationValue(56, 117.5);
    private static double RIGHT_MAG_OFFSET = RobotConfiguration.getConfigurationValue(-288.5, -191.5);

    //Utility arm gearbox
    private static final double RPM_TO_DPS = 1D/60 * 360;
    private static final double MOTOR_RPM_TO_ARM_DPS = RPM_TO_DPS / UtilityArmConstants.GEAR_RATIO;

    private static final UtilityArm instance = new UtilityArm();

    private final CANSparkMax leftUtilityArm = new CANSparkMax(RobotMap.LEFT_UTILITY_ARM, CANSparkMaxLowLevel.MotorType.kBrushless);
    private final CANSparkMax rightUtilityArm = new CANSparkMax(RobotMap.RIGHT_UTILITY_ARM, CANSparkMaxLowLevel.MotorType.kBrushless);

    private final MagEncoder leftMagEncoder = new MagEncoder(0, true, LEFT_MAG_OFFSET, 90,  RobotConfiguration.getConfigurationValue(-3, -2.7));
    private final MagEncoder rightMagEncoder = new MagEncoder(1, false, RIGHT_MAG_OFFSET, 90, RobotConfiguration.getConfigurationValue(-3, -4.86));

    private double leftOffset, rightOffset;

    private final double P = RobotConfiguration.getConfigurationValue(.05, .03);
    private final double I = RobotConfiguration.getConfigurationValue(.05, .03);
    private final double v = RobotConfiguration.getConfigurationValue(0.0, 0.0);
    private final MotionController holdMotionController = new MotionController(P, v, I, 0, UtilityArmConstants.kFFA_UNLOADED, 0);
    private final MotionState setpoint = new MotionState();

    private static final int HOLD_CURRENT_LIMIT = 10;
    private static final int ACTIVE_CURRENT_LIMIT = 40;
    private static final double DISABLE_HOLD_POWER_ANGLE = 5.0;

    private static final double MIN = -5;
    private static final double MAX = 206;

    private UtilityArm(){

        leftUtilityArm.setParameter(CANSparkMaxLowLevel.ConfigParameter.kSensorType, RobotMap.NEO_HALL_EFFECT_ENC);
        rightUtilityArm.setParameter(CANSparkMaxLowLevel.ConfigParameter.kSensorType, RobotMap.NEO_HALL_EFFECT_ENC);

        leftUtilityArm.setPeriodicFramePeriod(CANSparkMaxLowLevel.PeriodicFrame.kStatus2, 20);
        rightUtilityArm.setPeriodicFramePeriod(CANSparkMaxLowLevel.PeriodicFrame.kStatus2, 20);

        leftUtilityArm.setIdleMode(CANSparkMax.IdleMode.kBrake);
        rightUtilityArm.setIdleMode(CANSparkMax.IdleMode.kBrake);

        leftUtilityArm.getEncoder().setPosition(0.0);
        rightUtilityArm.getEncoder().setPosition(0.0);

        rightUtilityArm.setInverted(true);

        motors.add(leftUtilityArm);
        motors.add(rightUtilityArm);

        setHold();
    }

    public void setPower(double power){
        setActive();
        updatePower(power);
    }

    private void updatePower(double power) {
        double offset = 0;//getPid(getLeftAngle(), getRightAngle());
        leftUtilityArm.set(power - offset);
        rightUtilityArm.set(power + offset);
    }
    //returns left side of the utility arm in degrees
    public double getLeftAngle(){
        double angle = leftUtilityArm.getEncoder().getPosition()*360/UtilityArmConstants.GEAR_RATIO + leftOffset;
        if(angle < MIN){
            leftOffset -= angle - MIN;
        }else if(angle > MAX){
            leftOffset -= angle - MAX;
        }
        return angle;
    }

    //returns right side of the utility arm in degrees
    public double getRightAngle(){
        double angle = rightUtilityArm.getEncoder().getPosition()*360/UtilityArmConstants.GEAR_RATIO + rightOffset;
        if(angle < MIN){
            rightOffset -= angle - MIN;
        }else if(angle > MAX){
            rightOffset -= angle - MAX;
        }
        return angle;
    }

    //returns the average angle in degrees
    public double getAverageAngle(){
        return (getLeftAngle() + getRightAngle()) / 2;
    }

    //returns the left angular velocity in deg/sec
    public double getLeftAngularVelocity(){
        return leftUtilityArm.getEncoder().getVelocity() * MOTOR_RPM_TO_ARM_DPS;
    }

    //returns the right angular velocity in deg/sec
    public double getRightAngularVelocity(){
        return rightUtilityArm.getEncoder().getVelocity() * MOTOR_RPM_TO_ARM_DPS;
    }

    //returns the average angular velocity in deg/sec
    public double getAverageAngularVelocity(){
        return (getLeftAngularVelocity() + getRightAngularVelocity()) / 2;
    }

    public static UtilityArm getInstance(){
        return instance;
    }

    public void setIdleMode(CANSparkMax.IdleMode idleMode){
        leftUtilityArm.setIdleMode(idleMode);
        rightUtilityArm.setIdleMode(idleMode);
        LOGGER.info("Idle mode changed: " + idleMode);
    }
    @Override
    protected void initDefaultCommand() {
    }

    public enum State {
        HOLD,
        ACTIVE,
        DISABLED
    }

    private MotionState currentState = new MotionState();

    private State state = State.HOLD;

    double average = 0;
    @Override
    public void update(boolean isEnabled) {

        SmartDashboard.putNumber("Left Util Arm", getLeftAngle());
        SmartDashboard.putNumber("Right Util Arm", getRightAngle());

//        SmartDashboard.putNumber("Left Util Arm Raw", leftMagEncoder.getRaw());
//        SmartDashboard.putNumber("Right Util Arm Raw", rightMagEncoder.getRaw());

        if(isEnabled) {
            if (state == State.HOLD) {

                currentState.position = getAverageAngle();
                currentState.velocity = getAverageAngularVelocity();
                setpoint.acceleration = calculateAccelGravity(currentState);

                double voltage = holdMotionController.update(Timer.getFPGATimestamp(), currentState, setpoint);

                if (currentState.position < DISABLE_HOLD_POWER_ANGLE) {
                    voltage = 0;
                }

                updatePower(voltage / RobotController.getBatteryVoltage());
            }
        } else {
            state = State.DISABLED;
            updatePower(0.0);
        }
    }

    @Override
    public void init() {

        leftOffset = -3;
        rightOffset = -3;
//        new CommandGroup(){
//            {
//                setRunWhenDisabled(true);
//                addSequential(new InitializeAngle(leftMagEncoder, UtilityArm.getInstance()) {
//                    @Override
//                    protected void setOffset(double offset) {
//                        leftOffset = offset;
//                        System.out.format("Left Offset: %8.2f\n", leftOffset);
//                    }
//                });
//                addSequential(new InitializeAngle(rightMagEncoder, UtilityArm.getInstance()) {
//                    @Override
//                    protected void setOffset(double offset) {
//                        rightOffset = offset;
//                        System.out.format("Right Offset: %8.2f\n", rightOffset);
//                    }
//                });
//            }
//        }.start();
    }

    private static double GRAVITY_CONSTANT = UtilityArmConstants.ACCEL_OF_GRAVITY / UtilityArmConstants.LENGTH_COM;

    public double calculateAccelGravity(MotionState setpoint){
        //L * M * G * cos(theta) / I
        return Math.cos(Math.toRadians(setpoint.position + UtilityArmConstants.ANGLE_ADDITIONAL_COM)) * GRAVITY_CONSTANT;
    }

    public void setActive() {
        if(state != State.ACTIVE){
            LOGGER.info("ACTIVE");
        }
        leftUtilityArm.setSmartCurrentLimit(80, 20, 10000);
        rightUtilityArm.setSmartCurrentLimit(80, 20, 10000);
        state = State.ACTIVE;
    }
    public void setHold(){
        setHold(getAverageAngle());
    }

    public void setHold(double position) {
        LOGGER.info("Set Hold: " + position);
        this.setpoint.position = position;
        this.setpoint.velocity = 0;
        this.setpoint.acceleration = 0;

        holdMotionController.reset();

        leftUtilityArm.setSmartCurrentLimit(80, 20, 10000);
        rightUtilityArm.setSmartCurrentLimit(80, 20, 10000);

        state = State.HOLD;
        LOGGER.info("HOLD");
    }

    @Override
    public ArrayList<CANSparkMax> getMotorInfo() {
        return motors;
    }

    @Override
    public String getSubsystemName() {
        return this.getClass().getSimpleName();
    }

}
