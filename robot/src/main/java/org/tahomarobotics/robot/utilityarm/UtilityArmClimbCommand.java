/*
 * Copyright 2019 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.utilityarm;

import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.tahomarobotics.robot.motion.*;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.util.ChartData;

public class UtilityArmClimbCommand extends Command {

    private final UtilityArm arm = UtilityArm.getInstance();

    private double distance;
    private static final double DURATION = 4.0; // seconds
    private static final double TA = 1.0; // seconds

    private double initTime;

    private MotionProfile profile;

    private MotionState currentState = new MotionState();
    private MotionState motionSetpoint = new MotionState();
    private MotionState controllerSetpoint = new MotionState();

    private static final double kP = 0;
    private static final double kV = 0;
    private static final double kI = 0;
    private static final double kffV = UtilityArmConstants.kFFV;
    private static final double kffA = UtilityArmConstants.kFFA_CLIMB;
    private static final double positionTolerance = 2;

    private MotionController rotController = new MotionController(kP, kV, kI, kffV, kffA, positionTolerance);

    private boolean isComplete;

    private static final double TIMEOUT = 0.0;

    private final ChartData chartDataIn = new ChartData("Arm motion", "Time (Sec)", "Velocity (deg/sec)",
            new String[] {"expected-vel", "actual-vel", /* "expected-angpos", "actual-angpos",*/ "expected-angvel", "actual-angvel", "voltage"});

    public UtilityArmClimbCommand(){
        requires(arm);

    }

    @Override
    protected void initialize() {
        initTime = Timer.getFPGATimestamp();

        distance = RobotState.getInstance().getClimbIntent().height;

        double MAX_VEL = distance / (DURATION - TA);
        double MAX_ACC = MAX_VEL/TA;
        profile = MotionProfileFactory.createMotionProfile(MotionProfileFactory.Profile.Trapezoid, 0, 0,
                distance, 0, 0, MAX_VEL, MAX_ACC, 0);

        setTimeout(profile.getEndTime() + TIMEOUT);

        chartDataIn.clear();

        rotController.reset();
        motionSetpoint.setTime(0).setVelocity(0).setPosition(0).setAcceleration(0);
    }

    //    0.00000574155 is the loop time
    @Override
    protected void execute() {

        double elapsedTime = Timer.getFPGATimestamp() - initTime;

        isComplete = !profile.getSetpoint(elapsedTime, motionSetpoint);

        // create controller setpoint values
        controllerSetpoint.time = motionSetpoint.time;

        // position and velocity will be in degrees and degrees/second
        double angle = UtilityArmConstants.calculateAngle(motionSetpoint.position, distance);
        controllerSetpoint.position = angle;
        controllerSetpoint.velocity = UtilityArmConstants.calculateAngularVelocity(motionSetpoint.position, motionSetpoint.velocity, distance);

        // acceleration will be in inches/sec2
        controllerSetpoint.acceleration = motionSetpoint.acceleration + UtilityArmConstants.ACCEL_OF_GRAVITY;
        controllerSetpoint.acceleration /= Math.sin(Math.toRadians(angle + UtilityArmConstants.VERTICAL_ANGLE));

        //updates current state in in and in/s
        currentState.setTime(elapsedTime);
        currentState.setPosition(arm.getAverageAngle());
        currentState.setVelocity(arm.getAverageAngularVelocity());

        double voltage = rotController.update(elapsedTime, currentState, controllerSetpoint);
        double pct = voltage / RobotController.getBatteryVoltage();

        arm.setPower(pct);

        //System.out.format("%8.3f %8.3f %8.3f\n", controllerSetpoint.velocity, voltage, currentState.velocity);

        double velocity = UtilityArmConstants.calculateVerticalVelocity(currentState.position, currentState.velocity);
        // outputs everything in terms of inches
        chartDataIn.addData(new double[] {elapsedTime,
                motionSetpoint.velocity, velocity,
                //controllerSetpoint.position, currentState.position,
                controllerSetpoint.velocity, currentState.velocity, voltage} );
    }

    @Override
    protected boolean isFinished() {
        return isComplete && (isTimedOut() || rotController.onTarget());
    }

    @Override
    protected void end() {
        arm.setPower(0);

        SmartDashboard.putRaw("UtilityArmChartData", chartDataIn.serialize());
    }


}