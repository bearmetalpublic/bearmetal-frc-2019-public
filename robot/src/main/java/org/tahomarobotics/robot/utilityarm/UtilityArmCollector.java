/*
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.utilityarm;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import edu.wpi.first.wpilibj.command.Subsystem;
import org.tahomarobotics.robot.RobotMap;

public class UtilityArmCollector extends Subsystem {

    private static final UtilityArmCollector INSTANCE = new UtilityArmCollector();

    private final CANSparkMax motor = new CANSparkMax(RobotMap.UTILITY_ARM_COLLECTOR, CANSparkMaxLowLevel.MotorType.kBrushless);

    private UtilityArmCollector(){
        motor.setSmartCurrentLimit(80, 20, 10000);
        motor.setInverted(true);
    }

    public static UtilityArmCollector getInstance() {
        return INSTANCE;
    }

    @Override
    protected void initDefaultCommand(){
        setDefaultCommand(new UtilityArmCollectTeleOpCommand());
    }

    public void setPower(double power){
        motor.set(power);
    }

}
