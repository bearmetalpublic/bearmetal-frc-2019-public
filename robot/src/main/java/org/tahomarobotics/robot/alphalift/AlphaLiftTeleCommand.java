/*
 * Copyright 2019 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.alphalift;

import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.command.Command;
import org.tahomarobotics.robot.OI;

public class AlphaLiftTeleCommand extends Command {

	private final AlphaLift lift = AlphaLift.getInstance();
	private final OI oi = OI.getInstance();

	public AlphaLiftTeleCommand(){
		requires(lift);
	}
	@Override
	protected boolean isFinished() {
//		lift.setPower(limitVoltage(oi.getRightManipThrottle(), lift.getPosition(), lift.getTopVelocity()));
		lift.setPower(oi.getRightManipThrottle());
		return false;
	}

	private double limitVoltage(double input, double pos, double vel){
		double limit, cmdLimit, cmd;
		double MAX_ACCEL = 300.0;
		input *= RobotController.getBatteryVoltage();

		if (vel < 0) {
			limit = -Math.sqrt(2 * MAX_ACCEL * Math.max(pos,0));
			cmdLimit = AlphaLiftConstants.kFFV  * limit + AlphaLiftConstants.kFFA * MAX_ACCEL;
			cmd = Math.max(input, cmdLimit);
		} else {
			pos = AlphaLiftConstants.TOP_LIFT_POSITION - pos;
			limit = Math.sqrt(2 * MAX_ACCEL * Math.max(pos,0));
			cmdLimit = AlphaLiftConstants.kFFV * limit - AlphaLiftConstants.kFFA * MAX_ACCEL;
			cmd = Math.min(input, cmdLimit);
		}

		limit = cmd/12;

		double sign = Math.signum(limit);
		double min = input < 0 ? 0.05 : 0.25;
		limit = sign * Math.max(Math.abs(limit), Math.min(input, min));

		return limit;
	}
}
