/*
 * Copyright 2019 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.alphalift;

import org.tahomarobotics.robot.util.Constants;

public class AlphaLiftConstants {
    private static final double MOTOR_COUNT = 2.0;

    private static final double STAGE_ONE = 52/10D;
    private static final double STAGE_TWO = 36/24D;
    private static final double CIRCUMFERENCE = 2 * ((5*36d) /25.4);
    private static final double DIAMETER = CIRCUMFERENCE / (2 * Math.PI);
    private static final double RADIUS = DIAMETER / 2;

    public static final double GEAR_RATIO = STAGE_ONE * STAGE_TWO;

    public static final double REV_TO_INCHES = CIRCUMFERENCE / GEAR_RATIO;
    public static final double RPMS_TO_INCHES_PER_SEC = REV_TO_INCHES / 60;

    private static final double ALPHA_LIFT_MASS = 7.667;

    public static final double TOP_LIFT_POSITION = 59.9;

    public static final double SPEC_VOLTAGE = 12.0;
    public static final double FREE_SPEED = 5676; // RPM
    public static final double FREE_ANGULAR_VELOCITY = FREE_SPEED * 2 * Math.PI / 60; // radian/sec
    public static final double FREE_CURRENT = 1.8; // Amps
    public static final double STALL_TORQUE = 2.6; // N-m
    public static final double STALL_CURRENT = 105; // Amps
    public static final double RESISTANCE = SPEC_VOLTAGE / STALL_CURRENT;
    public static final double kT = STALL_TORQUE / (STALL_CURRENT - FREE_CURRENT); // Nm/Amp
    public static final double kV = FREE_ANGULAR_VELOCITY / (SPEC_VOLTAGE - RESISTANCE * FREE_CURRENT); // rad/s per volt
// 7.8 / 49.53 / 1.1278
    public static final double kFFV = GEAR_RATIO / kV / RADIUS / 2.0;

    private static final double LBMASS_TO_KGS = 0.453592;
    private static final double LIFT_BASE_MASS = 7.543 * LBMASS_TO_KGS;
    private static final double CART_MASS = 9.760 * LBMASS_TO_KGS;
    private static final double COLLECTOR_MASS = 6.0 * LBMASS_TO_KGS;
//    private static final double COLLECTOR_MASS = 0 * LBMASS_TO_KGS;
    private static final double INTERIA_MASS = LIFT_BASE_MASS / 2 +  ( CART_MASS +  COLLECTOR_MASS);

    public static final double kFFA = RESISTANCE * Constants.METERS_PER_INCH * RADIUS * INTERIA_MASS / (kT * GEAR_RATIO) / MOTOR_COUNT;

    private static final double SPRING_FORCE = 20 * 4.4482216282509;
    public static final double STATIC_ACCELERATION = (Constants.GRAVITIONAL_ACCELERATION * INTERIA_MASS - SPRING_FORCE/2) /  INTERIA_MASS;

}
