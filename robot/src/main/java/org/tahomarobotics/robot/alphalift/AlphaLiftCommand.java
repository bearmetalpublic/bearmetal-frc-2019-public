/*
 * Copyright 2019 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.alphalift;

import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.tahomarobotics.robot.RobotConfiguration;
import org.tahomarobotics.robot.motion.MotionController;
import org.tahomarobotics.robot.motion.MotionProfile;
import org.tahomarobotics.robot.motion.MotionProfileFactory;
import org.tahomarobotics.robot.motion.MotionState;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.state.SwitchPlaceMode;
import org.tahomarobotics.robot.util.ChartData;
import org.tahomarobotics.robot.util.Constants;

public class AlphaLiftCommand extends Command {

	private double startTime;
	private double startPosition;
	private double targetPos;
	private AlphaLift.AlphaLiftPosition target;
	private boolean isCompleted = false;

	private final double maxVelocity = 80;
	private final double maxAccel = 400;

	private final double P = RobotConfiguration.getConfigurationValue(.08, .1);
	private final double I = RobotConfiguration.getConfigurationValue(.05, .1);
	private final double v = RobotConfiguration.getConfigurationValue(.0, .0);
	private final double kffv = AlphaLiftConstants.kFFV;
	private final double kffa = AlphaLiftConstants.kFFA * RobotConfiguration.getConfigurationValue(1.4, 1.6);
	private final double tolerance = 0.25;
	private double TIMEOUT = 0.5;

	private final MotionController controller = new MotionController(P, v, I, kffv, kffa, tolerance);
	private MotionProfile profile;
	private MotionProfileFactory.Profile profileType = MotionProfileFactory.Profile.Trapezoid;
	private MotionState setpoint = new MotionState();
	private MotionState currentState = new MotionState();

	private final AlphaLift alphaLift = AlphaLift.getInstance();

	private final ChartData chartData = new ChartData("Alpha Lift Motion", "Time (Sec)", "Velocity (in/sec)",
			new String[] {"expected-vel", "actual-vel", "voltage", "expected-pos", "actual-pos"});


	private boolean collect;
	public AlphaLiftCommand(double position){
		this(AlphaLift.AlphaLiftPosition.CUSTOM, false);
		targetPos = position;

	}

	public AlphaLiftCommand(AlphaLift.AlphaLiftPosition position){
		this(position, false);
	}

	public AlphaLiftCommand(AlphaLift.AlphaLiftPosition position, boolean collect){
		this.collect = collect;
		this.target = position;
		requires(alphaLift);
	}

	@Override
	protected void initialize() {
		double pos = (target == AlphaLift.AlphaLiftPosition.CUSTOM) ? targetPos : alphaLift.getSetHeight(target);

		if(RobotState.getInstance().getWantedPlaceMode() == SwitchPlaceMode.PlaceMode.BALL && pos < 50 && !collect){
			pos += 8.5;
		}

		profile = MotionProfileFactory.createMotionProfile(profileType, 0.0, alphaLift.getPosition(), pos, alphaLift.getTopVelocity(),
				0.0, maxVelocity, maxAccel, 0.0);

		setTimeout(profile.getEndTime() + TIMEOUT);

		startTime = Timer.getFPGATimestamp();

		chartData.clear();

		controller.reset();

		setpoint.setPosition(0).setVelocity(0).setAcceleration(0);
	}

	@Override
	protected void execute() {
		double time = Timer.getFPGATimestamp();

		double deltaTime = time - startTime;

		currentState.setTime(deltaTime);
		currentState.setPosition(alphaLift.getPosition());
		currentState.setVelocity(alphaLift.getTopVelocity());

		isCompleted = !profile.getSetpoint(deltaTime, setpoint);

		setpoint.acceleration = Constants.METERS_PER_INCH * setpoint.acceleration + AlphaLiftConstants.STATIC_ACCELERATION;

		double voltage = controller.update(deltaTime, currentState, setpoint);
		double percent = voltage / RobotController.getBatteryVoltage();

		alphaLift.setPower(percent);


		chartData.addData(new double[]{deltaTime, setpoint.velocity, currentState.velocity, voltage, setpoint.position, currentState.position});
	}

	@Override
	protected boolean isFinished() {
		return isCompleted && (isTimedOut() || controller.onTarget());
	}

	@Override
	protected void end() {
		SmartDashboard.putRaw("AlphaLiftChartData", chartData.serialize());
		alphaLift.setHold(setpoint.position);
	}

}
