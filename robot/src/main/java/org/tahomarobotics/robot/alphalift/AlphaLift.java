/*
 * Copyright 2019 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.alphalift;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.*;
import org.tahomarobotics.robot.alphacollector.AlphaCollector;
import org.tahomarobotics.robot.motion.MotionController;
import org.tahomarobotics.robot.motion.MotionState;

import java.util.ArrayList;

public class AlphaLift extends Subsystem implements UpdateIF, MotorInfo.SubsystemMotors {

    public enum AlphaLiftPosition {
        TOP(57, true),
        MID(30, true),
        CARGO_SHIP(AlphaCollector.IS_CENTER_COLLECTOR ? 27 : 24, true),
        SAFE(14, false),
        BALL_COLLECT(AlphaCollector.IS_CENTER_COLLECTOR ? 9.5 : 9.12, true),
        SAFE_MOVEMENT(10.5, false),
        BOTTOM(RobotConfiguration.getConfigurationValue(.2, 1), false),
        CUSTOM(.5, false);

        private double offset;
        public double position;
        private boolean allowsOffset;

        AlphaLiftPosition(double position, boolean allowsOffset){
            this.allowsOffset = allowsOffset;
            this.position = position;
        }

        public double getOffset(){
            return offset;
        }

        public void setOffset(double deltaTheta){
//            if(!allowsOffset) return;
//            offset += deltaTheta;
        }
    }

    private enum State {
        HOLD,
        ACTIVE,
        DISABLED
    }

    private static Logger LOGGER = LoggerFactory.getLogger(AlphaLift.class);

    private static final double MAX_VELOCITY = 10;

    private static final AlphaLift instance = new AlphaLift();

    private final CANSparkMax alphaTop = new CANSparkMax(RobotMap.ALPHA_LIFT_TOP_MOTOR, CANSparkMaxLowLevel.MotorType.kBrushless);
    private final CANSparkMax alphaBottom = new CANSparkMax(RobotMap.ALPHA_LIFT_BOTTOM_MOTOR, CANSparkMaxLowLevel.MotorType.kBrushless);

    private static double MAX_OFFSET = 5.0;

    private final ArrayList<CANSparkMax> motors = new ArrayList<>();

    private State state = State.HOLD;

    private double offset = 0.0;

    private AlphaLiftPosition lastPosition = AlphaLiftPosition.BOTTOM;

    private static final int HOLD_CURRENT_LIMIT = 10;
    private static final int ACTIVE_CURRENT_LIMIT = 40;
    private static final double DISABLE_HOLD_POWER_HEIGHT = 1.0;

    private final double P = RobotConfiguration.getConfigurationValue(.5, .5);
    private final double I = RobotConfiguration.getConfigurationValue(.5, .1);
    private final double V = RobotConfiguration.getConfigurationValue(0, 0);
    private final MotionController holdMotionController = new MotionController(P, V, I, 0, 0, 0);
    private final MotionState holdSetpoint = new MotionState();
    private final MotionState currentState = new MotionState();

    public static AlphaLift getInstance(){
        return instance;
    }

    private AlphaLift(){

        alphaBottom.follow(alphaTop);
        alphaTop.setPeriodicFramePeriod(CANSparkMaxLowLevel.PeriodicFrame.kStatus1, 20);
        alphaTop.setPeriodicFramePeriod(CANSparkMaxLowLevel.PeriodicFrame.kStatus2, 20);
        motors.add(alphaTop);
        motors.add(alphaBottom);
        //its over 9000
        alphaTop.setSmartCurrentLimit(80, 20, 10000);
        alphaBottom.setSmartCurrentLimit(80, 20, 10000);
    }

    private double getCurent(){
        return alphaTop.getOutputCurrent();
    }

    public void setPower(double power){
        setActive();
        updatePower(power);
    }

    public double getSetHeight(AlphaLiftPosition wantedHeight){
        lastPosition = wantedHeight;
        return lastPosition.position + lastPosition.getOffset();
    }

    public double getTopVelocity(){
        return alphaTop.getEncoder().getVelocity() * AlphaLiftConstants.RPMS_TO_INCHES_PER_SEC;
    }

    public double getBottomVelocity(){
        return alphaBottom.getEncoder().getVelocity() * AlphaLiftConstants.RPMS_TO_INCHES_PER_SEC;
    }
    private double getRawPosition() {
        return alphaTop.getEncoder().getPosition() * AlphaLiftConstants.REV_TO_INCHES;
    }

    public double getPosition(){
        double pos = getRawPosition() - offset;
        if(pos < 0.0){
            offset = getRawPosition();
            LOGGER.info("Went below zero");
        }
        return getRawPosition() - offset;
    }

    public void setIdleMode(CANSparkMax.IdleMode mode){
        alphaTop.setIdleMode(mode);
        alphaBottom.setIdleMode(mode);
    }

    public void setHold(){
        setHold(getPosition());
    }

    public void setHold(double position){
        holdSetpoint.position = position;
        holdSetpoint.velocity = 0;
        holdSetpoint.acceleration = 0;
        holdMotionController.reset();
        state = State.HOLD;
        LOGGER.info("Hold alpha lift");
    }

    public void setActive() {
        if(state != State.ACTIVE) {
            LOGGER.info("Alpha lift Active");
            state = State.ACTIVE;
        }
    }

    private void updatePower(double power) {
        alphaTop.set(power);
    }

    private void teleopUpdates() {

        //We do not want to update offsets if the robot is not enabled as hold could be changed by accident.
        if(!DriverStation.getInstance().isEnabled()) return;


        OI oi = OI.getInstance();
        int dpad = oi.getDriverDPad();
        double manual = oi.getLeftManipThrottle();
        manual += (dpad == OI.DPAD_SOUTH) ? 1 : ((dpad == OI.DPAD_NORTH) ? -1:0);

        double velocity = manual*MAX_VELOCITY;
        double deltaPosition = velocity * 0.02;

        if((lastPosition.getOffset() > MAX_OFFSET && deltaPosition > 0) ||
                (lastPosition.getOffset() < -MAX_OFFSET && deltaPosition < 0) ||
                (!lastPosition.allowsOffset)) return;


        holdSetpoint.setVelocity(velocity);

        holdSetpoint.position += deltaPosition;

        lastPosition.setOffset(deltaPosition);

        // add OI controller inputs and changes to holdSetpoint
    }

    @Override
    protected void initDefaultCommand() {
    }

    @Override
    public void update(boolean isEnabled) {

        SmartDashboard.putNumber("Lift Height", getPosition());

        if(isEnabled) {
            switch (state) {
                case HOLD:
                    currentState.position = AlphaLift.getInstance().getPosition();
                    currentState.velocity = AlphaLift.getInstance().getTopVelocity();

                    teleopUpdates();

                    double voltage = holdMotionController.update(Timer.getFPGATimestamp(), currentState, holdSetpoint);

                    if (currentState.position < DISABLE_HOLD_POWER_HEIGHT && voltage < 0) {
                        voltage = 0;
                    }

                    updatePower(voltage / RobotController.getBatteryVoltage());
                    break;

                case ACTIVE:
                    break;
            }
        } else {
            state = State.DISABLED;
            updatePower(0.0);
        }
    }

    @Override
    public ArrayList<CANSparkMax> getMotorInfo() {
        return motors;
    }

    @Override
    public String getSubsystemName() {
        return this.getClass().getSimpleName();
    }
}
