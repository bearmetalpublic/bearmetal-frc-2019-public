/*
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot;

public class RobotMap {

    //Drive motor CAN mapping
    public static final int LEFT_REAR_MOTOR = 1;
    public static final int LEFT_FRONT_MOTOR = 2;
    public static final int RIGHT_REAR_MOTOR = 3;
    public static final int RIGHT_FRONT_MOTOR = 4;

    public static final int ALPHA_ARM_MOTOR = 7;
    public static final int ALPHA_COLLECTOR_MOTOR = 10;

    public static final int ALPHA_LIFT_TOP_MOTOR = 5;
    public static final int ALPHA_LIFT_BOTTOM_MOTOR = 6;
    //Betalift mapping
    public static final int BETALIFT_MOTOR_BOTTOM_FRONT = 15;
    public static final int BETALIFT_MOTOR_BOTTOM_BACK = 16;
    public static final int BETALIFT_MOTOR_TOP = 14;

    //Utility Arm
    public static final int LEFT_UTILITY_ARM = 11;
    public static final int RIGHT_UTILITY_ARM = 12;
	public static final int UTILITY_ARM_COLLECTOR = 13;

    // IMU CAN mapping
    public static final int PIGEON_IMU         = 7;

    // Pneumatics mapping
    public static final int PCM_MODULE = RobotConfiguration.getConfigurationValue(0, 1);

    public static final int NEO_HALL_EFFECT_ENC = 1;

    public static final int COLLECTOR_SOLENOID = RobotConfiguration.getConfigurationValue(2, 0);
}
