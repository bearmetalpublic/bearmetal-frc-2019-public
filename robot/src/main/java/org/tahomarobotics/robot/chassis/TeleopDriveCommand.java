/*
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.chassis;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.OI;

import edu.wpi.first.wpilibj.command.Command;
import org.tahomarobotics.robot.alphacollector.AlphaCollector;
import org.tahomarobotics.robot.alphacollector.PowerCollector;
import org.tahomarobotics.robot.motion.MotionController;
import org.tahomarobotics.robot.motion.MotionState;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.util.MathUtil;
import org.tahomarobotics.robot.utilityarm.UtilityArm;
import org.tahomarobotics.robot.vision.Limelight;

public class TeleopDriveCommand extends Command {

    // larger sensitivity for rotation
    private static final double ROTATIONAL_SENSITIVITY = 3;

    // normal sensitivity for forward
    private static final double FORWARD_SENSITIVITY = 1.7;

    // used to command drive motors
    private final Chassis chassis = Chassis.getInstance();

    // used for throttle input
    private final OI oi = OI.getInstance();

    private static final RobotState robotState = RobotState.getInstance();

    private double kP = 0.02;
    private double kV = 0.000;
    private double kI = 0.005;

    private final MotionController motionController = new MotionController(kP, kV, kI,
            0,0, 0);

    private static Logger LOGGER = LoggerFactory.getLogger(TeleopDriveCommand.class);

    private final MotionState motionSetpoint = new MotionState();
    private final MotionState currentMotionState = new MotionState();

    private volatile double angle = 0;
    private double scalar = 1.0;
    private double initTime;

    /**
     * Manual teleoperation drive command
     */
    public TeleopDriveCommand() {

        // as this is the default command for Chassis, this is required
        requires(chassis);

    }



    private boolean driving = true;
    private boolean first = true;

    @Override
    protected boolean isFinished() {
        if(driving && first){
            Limelight.drivingMode(true);
            first = false;
        }else if(!driving && !first) {
            Limelight.drivingMode(false);
            first = true;
        }


        if(oi.getDriverRightTrigger() != 0.0 ){
            driving = false;
            double wantedAngle = Limelight.getX();

            double elapsedTime = Timer.getFPGATimestamp() - initTime;
            double distance = Limelight.getDistance();
            distance = Math.pow(((distance + 370.0) / 500.0), 10) + .25;
            distance = distance > 1.0 ? 1.0 : distance;
            double forward = OI.getInstance().getDriverRightTrigger() * (distance) * .8;
            forward = Math.abs(forward) > .8 ? .8 : forward;
            double robotHeading = robotState.getHeading();
            double delta = robotHeading - wantedAngle;

            currentMotionState.setPosition(delta);
            currentMotionState.setVelocity(robotState.getRotVel());

            motionSetpoint.setPosition(robotHeading);
            motionSetpoint.setVelocity(0.0);

            double rotPower = -motionController.update(elapsedTime, currentMotionState, motionSetpoint);

            chassis.setPower(forward, rotPower);


        }else if (oi.getDriverLeftTrigger() != 0.0) {

            driving = false;
            double wantedAngle = Limelight.getX();

            double elapsedTime = Timer.getFPGATimestamp() - initTime;
            double robotHeading = robotState.getHeading();
            double delta = robotHeading - wantedAngle;

            currentMotionState.setPosition(delta);
            currentMotionState.setVelocity(robotState.getRotVel());

            motionSetpoint.setPosition(robotHeading);
            motionSetpoint.setVelocity(0.0);

            double rotPower = -motionController.update(elapsedTime, currentMotionState, motionSetpoint);

            chassis.setPower(0.0, rotPower);
        }else{
            driving = true;
            // obtain throttle values from controller
            double leftThrottle = oi.getLeftThrottle();
            double rightThrottle = oi.getRightThrottle();

            // convert left and right controls to forward and rotate so that each can be
            // desensitized with different values
            double forwardPower = desensitizePowerBased((rightThrottle + leftThrottle) / 2, FORWARD_SENSITIVITY);
            double rotatePower = desensitizePowerBased((rightThrottle - leftThrottle) / 2, ROTATIONAL_SENSITIVITY);

            chassis.setPower(forwardPower, rotatePower * .8);

        }
        return false;
    }

    /**
     * Reduces the sensitivity around the zero point to make the Robot more
     * controllable.
     *
     * @param value - raw input
     * @param power - 1.0 indicates linear (full sensitivity) - larger number reduces small values
     *
     * @return 0 to +/- 100%
     */
    private double desensitizePowerBased(double value, double power) {
        return Math.pow(Math.abs(value), power - 1) * value;
    }

}
