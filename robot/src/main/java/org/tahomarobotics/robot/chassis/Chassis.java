/*
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.chassis;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.MotorInfo;
import org.tahomarobotics.robot.RobotMap;

import java.util.ArrayList;

/**
 * Chassis class controls the drive base motors, transmission and provides velocity feedback.
 *
 */
public class Chassis extends Subsystem implements MotorInfo.SubsystemMotors {

    // singleton instance
    private static final Chassis INSTANCE = new Chassis();

    private static Logger LOGGER = LoggerFactory.getLogger(Chassis.class);

    private final ArrayList<CANSparkMax> motors = new ArrayList<>();

    // motor instances
    private final CANSparkMax leftFrontMaster = new CANSparkMax(RobotMap.LEFT_FRONT_MOTOR, CANSparkMaxLowLevel.MotorType.kBrushless);
    private final CANSparkMax leftBackFollower = new CANSparkMax(RobotMap.LEFT_REAR_MOTOR, CANSparkMaxLowLevel.MotorType.kBrushless);
    private final CANSparkMax rightFrontMaster = new CANSparkMax(RobotMap.RIGHT_FRONT_MOTOR, CANSparkMaxLowLevel.MotorType.kBrushless);
    private final CANSparkMax rightBackFollower = new CANSparkMax(RobotMap.RIGHT_REAR_MOTOR, CANSparkMaxLowLevel.MotorType.kBrushless);


    /**
     * Constructor is used to initialize motor controller for proper operation
     */
    private Chassis() {
        itGoesTo11(true);


        rightFrontMaster.setInverted(true);
        rightBackFollower.setInverted(true);

        leftFrontMaster.setPeriodicFramePeriod(CANSparkMaxLowLevel.PeriodicFrame.kStatus1, 5);
        rightFrontMaster.setPeriodicFramePeriod(CANSparkMaxLowLevel.PeriodicFrame.kStatus1, 5);

        leftBackFollower.follow(leftFrontMaster);
        rightBackFollower.follow(rightFrontMaster);

        leftFrontMaster.setParameter(CANSparkMaxLowLevel.ConfigParameter.kSensorType, RobotMap.NEO_HALL_EFFECT_ENC);
        leftBackFollower.setParameter(CANSparkMaxLowLevel.ConfigParameter.kSensorType, RobotMap.NEO_HALL_EFFECT_ENC);
        rightFrontMaster.setParameter(CANSparkMaxLowLevel.ConfigParameter.kSensorType, RobotMap.NEO_HALL_EFFECT_ENC);
        rightBackFollower.setParameter(CANSparkMaxLowLevel.ConfigParameter.kSensorType, RobotMap.NEO_HALL_EFFECT_ENC);

        motors.add(leftFrontMaster);
        motors.add(leftBackFollower);
        motors.add(rightFrontMaster);
        motors.add(rightBackFollower);
    }

    /**
     * @return the Chassis singleton instance
     */
    public static Chassis getInstance() {
        return INSTANCE;
    }

    /**
     * Install telop-drive command.  This command runs until
     * another chassis command requires the Chassis sub-system.
     */
    @Override
    protected void initDefaultCommand() {
        setDefaultCommand(new TeleopDriveCommand());
    }

    /**
     * Set the power level of the drive base motors
     *
     * @param forwardPower - forward power level
     * @param rotatePower - rotate power level
     */
    public void setPower(double forwardPower, double rotatePower) {
        setLeftRightPower(forwardPower - rotatePower, forwardPower + rotatePower);
    }

    /**
     * Set the power level of the drive base motors
     *
     * @param left - left power level
     * @param right - right power level
     */
    public void setLeftRightPower(double left, double right) {
        leftFrontMaster.set(left);
        rightFrontMaster.set(right);
    }

    /**
     * Returns average forward velocity (inches/second)
     *
     * @return forward velocity in (inches/second)
     */
    public double getForwardVelocity() {
        //System.out.println(leftFrontMaster.getEncoder().getTopVelocity()); //* ChassisConstants.PULSES_PER_100MS_TO_INCHES_PER_SEC);
        double leftVelocity  = leftFrontMaster.getEncoder().getVelocity()  * ChassisConstants.INCHES_PER_REVOLUTOIN;
        double rightVelocity = rightFrontMaster.getEncoder().getVelocity() * ChassisConstants.INCHES_PER_REVOLUTOIN;
        double forwardVelocity = (leftVelocity + rightVelocity) / 2;

        return forwardVelocity;
    }

    public void setIdleMode(CANSparkMax.IdleMode mode){
        LOGGER.info("Chassis IdleMode: " + mode.toString());
        leftFrontMaster.setIdleMode(mode);
        leftBackFollower.setIdleMode(mode);
        rightFrontMaster.setIdleMode(mode);
        rightBackFollower.setIdleMode(mode);
    }

    private static final int HIGH_CURRENT = 60;
    private static final int LOW_CURRENT = 30;
    private static final int UNLIMITED = 10000;
    private static final int LIMITED = 250;

    private static boolean prevGoTo11 = true;

    public void itGoesTo11(){
        itGoesTo11(!prevGoTo11);
    }

    public void itGoesTo11(boolean goTo11){
            if (goTo11) {

                SmartDashboard.putBoolean("State", true);
                leftBackFollower.setSmartCurrentLimit(HIGH_CURRENT, LOW_CURRENT, UNLIMITED);
                leftFrontMaster.setSmartCurrentLimit(HIGH_CURRENT, LOW_CURRENT, UNLIMITED);

                rightBackFollower.setSmartCurrentLimit(HIGH_CURRENT, LOW_CURRENT, UNLIMITED);
                rightFrontMaster.setSmartCurrentLimit(HIGH_CURRENT, LOW_CURRENT, UNLIMITED);
            } else {

                SmartDashboard.putBoolean("State", false);
                leftBackFollower.setSmartCurrentLimit(LOW_CURRENT, HIGH_CURRENT, LIMITED);
                leftFrontMaster.setSmartCurrentLimit(LOW_CURRENT, HIGH_CURRENT, LIMITED);

                rightBackFollower.setSmartCurrentLimit(LOW_CURRENT, HIGH_CURRENT, LIMITED);
                rightFrontMaster.setSmartCurrentLimit(LOW_CURRENT, HIGH_CURRENT, LIMITED);
            }
        prevGoTo11 = goTo11;
    }

    @Override
    public ArrayList<CANSparkMax> getMotorInfo() {
        return motors;
    }

    @Override
    public String getSubsystemName() {
        return this.getClass().getSimpleName();
    }
}
