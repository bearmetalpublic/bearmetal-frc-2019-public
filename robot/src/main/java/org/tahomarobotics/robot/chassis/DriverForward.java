package org.tahomarobotics.robot.chassis;

import edu.wpi.first.wpilibj.command.Command;

public class DriverForward extends Command {

	private final Chassis chassis = Chassis.getInstance();

	public DriverForward(){
		requires(chassis);
	}

	@Override
	protected void initialize() {
		setTimeout(0.75);
		chassis.setPower(.27, .0);
		System.out.println("SETPOWER");
	}

	@Override
	protected boolean isFinished() {
		return isTimedOut();
	}

	@Override
	protected void end() {
		chassis.setPower(0,0);
	}
}
