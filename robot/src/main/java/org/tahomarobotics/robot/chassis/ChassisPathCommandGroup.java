/*
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.chassis;

import java.util.List;

import org.tahomarobotics.robot.path.Waypoint;
import org.tahomarobotics.robot.state.Pose2D;
import org.tahomarobotics.robot.util.PathData;

import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * Abstract command group for holding path information such that it can be published to dash-board
 *
 */
public abstract class ChassisPathCommandGroup extends CommandGroup {

    // used to publish paths
    private final PathData pathData = new PathData();

    /**
     * Adds path for sequential execution while collecting way-points to publish
     *
     * @param path - ChassisPathCommand to add
     */
    protected void addPath(ChassisPathCommand path) {

        addSequential(path);

        List<Waypoint> waypoints = path.getWaypoints();

        double[] data = new double[waypoints.size() * 2];
        int i = 0;
        for (Waypoint pt : waypoints) {
            data[i++] = pt.x;
            data[i++] = pt.y;
        }

        pathData.addPath(data);
    }

    /**
     * Publishes to the full set of paths added to this command group to the dash-board
     */
    public void publish() {
        SmartDashboard.putRaw("Paths", pathData.serialize());
    }

    public abstract Pose2D getStartingPose();
}
