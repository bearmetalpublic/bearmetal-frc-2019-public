package org.tahomarobotics.robot.chassis;

import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.tahomarobotics.robot.motion.MotionState;
import org.tahomarobotics.robot.path.PathBuilder;
import org.tahomarobotics.robot.state.Pose2D;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.util.ChartData;

public class RotationalCommand extends Command {

	private final Chassis chassis = Chassis.getInstance();

	private final ChartData chartData = new ChartData("Path Motion", "Time (sec)", "Velocity (in/sec)",
			new String[] {"velocity", "position"});

	double time;

	Pose2D currentPose = new Pose2D();

	MotionState  currentMotionState = new MotionState();

	double dist = 5;
	double wantedTime = 5;
	public RotationalCommand(){
		requires(chassis.getInstance());
	}

	@Override
	protected void initialize() {
		time = Timer.getFPGATimestamp();
	}

	@Override
	protected boolean isFinished() {
		double time = this.timeSinceInitialized();
		RobotState.getInstance().getRobotState(currentPose, currentMotionState, time, false);

		chassis.setPower(dist*12*2*Math.PI/wantedTime * ChassisConstants.kFFV / RobotController.getBatteryVoltage(), Math.toRadians(360/wantedTime) * ChassisConstants.kFFV_ROT / RobotController.getBatteryVoltage());

		chartData.addData(new double[] {Timer.getFPGATimestamp(), chassis.getForwardVelocity(), currentMotionState.position / 10});
		return time> wantedTime;
	}

	@Override
	protected void end() {
		SmartDashboard.putRaw("PathMotionChartData", chartData.serialize());
	}
}
