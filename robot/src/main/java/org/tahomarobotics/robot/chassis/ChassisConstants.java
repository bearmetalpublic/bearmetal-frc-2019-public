/*
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.chassis;

import org.tahomarobotics.robot.RobotConstants;
import org.tahomarobotics.robot.util.Constants;

public class ChassisConstants {


    // Transmission
    private static final double FIRST_STAGE = 40d/11d;
    private static final double SECOND_STAGE = 42d/24d;
    private static final double GEAR_RATIO  = FIRST_STAGE * SECOND_STAGE;
    private static final double EFFICIENCY = 1;

    //Pulse Per Inch count
    private static final double WHEEL_DIA = 4.; // inches
    private static final double REVOLUTIONS_PER_WHEEL_ROTATION = GEAR_RATIO * 60;
    public static final double INCHES_PER_REVOLUTOIN = WHEEL_DIA * Math.PI / REVOLUTIONS_PER_WHEEL_ROTATION;
    public static final double WHEELBASE_HALF_WIDTH = 24.375 / 2;

    private static final int MOTOR_COUNT = 4;

    // Wheels
    private static final double WHEEL_DIAMETER = 4 * Constants.METERS_PER_INCH;
    private static final double WHEEL_RADIUS = WHEEL_DIAMETER / 2;

    // Robot Rigid Body
    private static final double MASS = 117.9 * Constants.KILOGRAM_PER_LBMASS; // kg
    private static final double TRACK_HALF_WIDTH = 24.375 / 2.0 * Constants.METERS_PER_INCH;

    private static final double TORQUE_PER_VOLT = RobotConstants.kT * MOTOR_COUNT * GEAR_RATIO * EFFICIENCY / RobotConstants.RESISTANCE;

    // voltage needed per unit of velocity (inches/second)
    public static final double kFFV  = Constants.METERS_PER_INCH * GEAR_RATIO  / WHEEL_RADIUS  / RobotConstants.kV * 24/20 * 24/24.9 * 24/25 * 24/24.4;

    // voltage needed per unit of acceleration (inches/second^2)
    public static final double kFFA  = Constants.METERS_PER_INCH * MASS * WHEEL_RADIUS / TORQUE_PER_VOLT;

    // additional voltage needed per unit of angular velocity (radians/sec -> volts)
    public static final double kFFV_ROT = TRACK_HALF_WIDTH * GEAR_RATIO / WHEEL_RADIUS / RobotConstants.kV * 177/120 * 118.5/120;

    private static final double MOMENT_OF_INTERIA = 20779.439
            * Constants.KILOGRAM_PER_LBMASS
            * Constants.METERS_PER_INCH
            * Constants.METERS_PER_INCH; // lbmass in^2 to kg m^2

    // additional voltage needed per unit of angular acceleration (radians/sec^2 -> volts)
    public static final double kFFA_ROT = MOMENT_OF_INTERIA / TRACK_HALF_WIDTH * WHEEL_RADIUS / TORQUE_PER_VOLT;

    // Path Controller and constants
    public static final double LOOKAHEAD_DISTANCE = 24.0;


}
