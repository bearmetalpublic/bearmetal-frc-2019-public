package org.tahomarobotics.robot.chassis;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.OI;
import org.tahomarobotics.robot.alphacollector.PowerCollector;
import org.tahomarobotics.robot.motion.MotionController;
import org.tahomarobotics.robot.motion.MotionState;
import org.tahomarobotics.robot.path.PathBuilder;
import org.tahomarobotics.robot.state.RobotState;
import org.tahomarobotics.robot.vision.Limelight;

public class AutoDriveToTape extends Command {

	private final Chassis chassis = Chassis.getInstance();

	private static final RobotState robotState = RobotState.getInstance();

	private double kP = 0.018;
	private double kV = 0.000;
	private double kI = 0.005;

	private final MotionController motionController = new MotionController(kP, kV, kI,
			0,0, 2);

	private static Logger LOGGER = LoggerFactory.getLogger(AutoDriveToTape.class);

	private final MotionState motionSetpoint = new MotionState();
	private final MotionState currentMotionState = new MotionState();



	private double speed;

	private final double offset;

	public AutoDriveToTape(){
		this(1, 32);
	}
	public AutoDriveToTape(double speed){
		this(speed, 32);
	}
	double distance;

	public AutoDriveToTape(double speed, double distance){
		this(speed, distance, 4);
	}
	public AutoDriveToTape(double speed, double distance, double time){
		this(speed, distance, time, 0.0, PathBuilder.Mirror.X);
	}
	public AutoDriveToTape(double speed, double distance, double time, double offset, PathBuilder.Mirror mirror){
		requires(chassis);
		Limelight.setVisionMode();
		Limelight.drivingMode(false);
		this.speed = speed;
		this.distance = distance;
		this.offset = (mirror == PathBuilder.Mirror.X || mirror == PathBuilder.Mirror.Y) ? -offset : offset;
		setTimeout(time);
	}

	@Override
	protected boolean isFinished() {
		double wantedAngle = Limelight.getX();

		double elapsedTime = Timer.getFPGATimestamp();
		double distance = Limelight.getDistance();
		double scaledDistance = distance;
		scaledDistance = Math.pow(((scaledDistance + 340.0) / 500.0), 10) + .25;
		scaledDistance = scaledDistance > 1.0 ? 1.0 : scaledDistance;
		double forward = speed * (scaledDistance);
		forward = Math.abs(forward) >= .8 ? .8 : forward;
		double robotHeading = robotState.getHeading();

		double delta = (robotHeading - wantedAngle) + offset;

		currentMotionState.setPosition(delta);
		currentMotionState.setVelocity(robotState.getRotVel());

		motionSetpoint.setPosition(robotHeading);
		motionSetpoint.setVelocity(0.0);

		double rotPower = -motionController.update(elapsedTime, currentMotionState, motionSetpoint);

		chassis.setPower(forward, rotPower);

		return distance < this.distance || isTimedOut();
	}

	@Override
	protected void end() {
		LOGGER.info("auto drive to tape finished");
		Limelight.drivingMode(true);
		chassis.setPower(0.0, 0.0);
	}
}
