package org.tahomarobotics.robot.chassis;

import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.command.Command;

public class GoTo11Toggle extends Command {
    private final JoystickButton trackButton;
    public GoTo11Toggle(JoystickButton trackButton) {
        this.trackButton = trackButton;
    }

    @Override
    protected boolean isFinished() {
        Chassis.getInstance().itGoesTo11();
        return true;
    }
}
