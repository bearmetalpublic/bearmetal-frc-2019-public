/**
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.alphacollector.AlphaCollector;
import org.tahomarobotics.robot.alphacollector.OpenCollectorCommand;
import org.tahomarobotics.robot.auto.commands.RotateToAngle;
import org.tahomarobotics.robot.auto.commands.SwitchCommands;
import org.tahomarobotics.robot.auto.movements.*;
import org.tahomarobotics.robot.chassis.AutoDriveToTape;
import org.tahomarobotics.robot.state.ClimbIntentMode;
import org.tahomarobotics.robot.state.SwitchPlaceMode;
import org.tahomarobotics.robot.vision.CamMode;
import org.tahomarobotics.robot.vision.Limelight;

/**
 * Operator Interface defines the driver and manipulator controls for the Robot.
 * It maps controllers and controller buttons and sticks to functions.
 */
@SuppressWarnings("unused")
public class OI implements UpdateIF {

    private static final Logger LOGGER = LoggerFactory.getLogger(OI.class);

    // The amount of the analog control range removed when the value are near zero
    private static final double DEAD_BAND = 9.0 / 127.0;

    private static final OI instance = new OI();

    // Controller for the Driver Functions
    private final XboxController driverController = new XboxController(0);

    private final JoystickButton driverButtonRBumber;
    private final JoystickButton driverButtonLBumber;
    private final JoystickButton driverButtonA;
    private final JoystickButton driverButtonB;
    private final JoystickButton driverButtonX;
    private final JoystickButton driverButtonY;
    private final JoystickButton driverButtonBack;
    private final JoystickButton driverButtonStart;
    private final JoystickButton driverButtonLeftJoystick;
    private final JoystickButton driverButtonRightJoystick;


    // Controller for the Driver Functions
    private final XboxController manipulatorController = new XboxController(1);

    private final JoystickButton manipulatorButtonRBumber;
    private final JoystickButton manipulatorButtonLBumber;
    private final JoystickButton manipulatorButtonA;
    private final JoystickButton manipulatorButtonB;
    private final JoystickButton manipulatorButtonX;
    private final JoystickButton manipulatorButtonY;
    private final JoystickButton manipulatorButtonBack;
    private final JoystickButton manipulatorButtonStart;
    private double endVibrateTime = -1;


    private OI() {

        driverButtonRBumber = new JoystickButton(driverController, 6);
        driverButtonLBumber = new JoystickButton(driverController, 5);
        driverButtonA = new JoystickButton(driverController, 1);
        driverButtonB = new JoystickButton(driverController, 2);
        driverButtonX = new JoystickButton(driverController, 3);
        driverButtonY = new JoystickButton(driverController, 4);
        driverButtonBack = new JoystickButton(driverController, 7);
        driverButtonStart = new JoystickButton(driverController, 8);
        driverButtonLeftJoystick = new JoystickButton(driverController, 9);
        driverButtonRightJoystick = new JoystickButton(driverController, 10);

        driverButtonA.whenPressed(new LowPlace());
        driverButtonB.whenPressed(new SwitchCommands());
        driverButtonX.whenPressed(new MidPlace());
        driverButtonY.whenPressed(new MaxRocket());
        driverButtonLBumber.whenPressed(new SwitchPlaceMode(SwitchPlaceMode.PlaceMode.BALL));
        driverButtonRBumber.whenPressed(new SwitchPlaceMode(SwitchPlaceMode.PlaceMode.HATCH));
        driverButtonBack.whenPressed(new PrepClimb(ClimbIntentMode.ClimbIntent.LEVEL3));
        driverButtonStart.whenPressed(new Climb());
//        driverButtonLeftJoystick.whenPressed(new GoTo11Toggle(driverButtonRightJoystick));
//        driverButtonRightJoystick.whenPressed(new GoTo11Toggle(driverButtonLeftJoystick));


        manipulatorButtonRBumber = new JoystickButton(manipulatorController, 6);
        manipulatorButtonLBumber = new JoystickButton(manipulatorController, 5);
        manipulatorButtonA = new JoystickButton(manipulatorController, 1);
        manipulatorButtonB = new JoystickButton(manipulatorController, 2);
        manipulatorButtonX = new JoystickButton(manipulatorController, 3);
        manipulatorButtonY = new JoystickButton(manipulatorController, 4);
        manipulatorButtonBack = new JoystickButton(manipulatorController, 7);
        manipulatorButtonStart = new JoystickButton(manipulatorController, 8);

        manipulatorButtonA.whenPressed(new LowPlace());
        manipulatorButtonB.whenPressed(new CollectCargo());
        manipulatorButtonX.whenPressed(new MidPlace());
        manipulatorButtonY.whenPressed(new MaxRocket());

        if (AlphaCollector.IS_CENTER_COLLECTOR) {
            manipulatorButtonLBumber.whenPressed(new SwitchPlaceMode(SwitchPlaceMode.PlaceMode.BALL));
            manipulatorButtonRBumber.whenPressed(new SwitchPlaceMode(SwitchPlaceMode.PlaceMode.HATCH));
        } else {
            manipulatorButtonLBumber.whenPressed(new OpenCollectorCommand(OpenCollectorCommand.State.CLOSE));
            manipulatorButtonRBumber.whenPressed(new OpenCollectorCommand(OpenCollectorCommand.State.OPEN));
        }

        manipulatorButtonBack.whenPressed(new PrepClimb(ClimbIntentMode.ClimbIntent.LEVEL3));
        manipulatorButtonStart.whenPressed(new PrepClimb(ClimbIntentMode.ClimbIntent.LEVEL2));
    }

    public static final OI getInstance() {
        return instance;
    }

    public boolean driverButtonA(){
        return driverButtonA.get();
    }

    public boolean manipButtonA() {
        return manipulatorButtonA.get();
    }

    @Override
    public void update(boolean isEnabled) {
        if(manipulatorController.getPOV() >= 0 && manipulatorController.getButtonCount() > 0 && isEnabled) {
            new CargoshipCargo().start();
        }
        if(endVibrateTime != -1 && Timer.getFPGATimestamp() >=  endVibrateTime){
            endVibrate();
        }
    }

    /**
     * Removes small values emitted from the controller axis when stick is relaxed.
     *
     * Returns the value only if value isn't in the range of -DEAD_BAND to +DEAD_BAND
     */
    private double applyDeadBand(double value){
        return applyDeadBand(value, DEAD_BAND);
    }

    /**
     * Remove a small dead-band.  After remove the dead-band value, the result is scaled
     * to ensure full range can be produced.
     *
     * @param value - raw analog input
     * @param deadBand - small value to remove
     *
     * @return processed analog value
     */
    private double applyDeadBand(double value, double deadBand) {
        double absValue = Math.abs(value);
        return (absValue < deadBand) ? 0 : ((absValue - deadBand) / (1.0 - deadBand) * Math.signum(value));
    }

    public final static int DPAD_WEST = 270;
    public final static int DPAD_EAST = 90;
    public final static int DPAD_NORTH = 180;
    public final static int DPAD_SOUTH = 0;


    public synchronized void startVibrate(){
        if(DriverStation.getInstance().isEnabled()) {
            driverController.setRumble(GenericHID.RumbleType.kRightRumble, 1.0);
            driverController.setRumble(GenericHID.RumbleType.kLeftRumble, 1.0);
            manipulatorController.setRumble(GenericHID.RumbleType.kRightRumble, 1.0);
            manipulatorController.setRumble(GenericHID.RumbleType.kLeftRumble, 1.0);
            //endVibrateTime = -1;
        }
    }

    public synchronized void vibrateOverheat(){
        startVibrate();
        endVibrateTime = -1;
    }

    public synchronized void endVibrate(){
        driverController.setRumble(GenericHID.RumbleType.kRightRumble, 0.0);
        driverController.setRumble(GenericHID.RumbleType.kLeftRumble, 0.0);
        manipulatorController.setRumble(GenericHID.RumbleType.kRightRumble, 0.0);
        manipulatorController.setRumble(GenericHID.RumbleType.kLeftRumble, 0.0);
        endVibrateTime = -1;
    }
    public int getDriverDPad() {
        return driverController.getPOV();
    }

    public double getLeftManipThrottle() {
        return -applyDeadBand(manipulatorController.getY(Hand.kLeft), 24.0/127.0);
    }
    public double getRightManipThrottle(){
        return -applyDeadBand(manipulatorController.getY(Hand.kRight), 24.0/127.0);
    }
    public double getDriverLeftTrigger() {
        return driverController.getTriggerAxis(Hand.kLeft);
    }

    public double getDriverRightTrigger() {
        return applyDeadBand(driverController.getTriggerAxis(Hand.kRight));
    }

    /**
     * Returns percent of left throttle.
     *
     * @return left throttle (0% to 100%)
     */
    public double getLeftThrottle() {
        return -applyDeadBand(driverController.getY(Hand.kLeft));
    }

    /**
     * Returns percent of right throttle.
     *
     * @return right throttle (0% to 100%)
     */
    public double getRightThrottle() {
        return -applyDeadBand(driverController.getY(Hand.kRight));
    }

    public double getManipLeftTrigger() {
        return manipulatorController.getTriggerAxis(Hand.kLeft);
    }

    public double getManipRightTrigger() {
        return manipulatorController.getTriggerAxis(Hand.kRight);
    }

    /**
     * @param time in seconds
     */
    public void vibrateTime(double time) {
        startVibrate();
        endVibrateTime = Timer.getFPGATimestamp() + time;
    }
}
