package org.tahomarobotics.robot.vision;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.tahomarobotics.robot.path.ActionIF;
import org.tahomarobotics.robot.path.PathBuilder;

public class SendPhoneTargetCommand extends Command implements ActionIF {

    private VisionCommandMessage visionCommandMessage;

    public SendPhoneTargetCommand(VisionCommandMessage.Sorting sorting){
        setRunWhenDisabled(true);
        visionCommandMessage = new VisionCommandMessage();

        for(int i = 0; i < 8; i++){
            visionCommandMessage.targets[i] = new VisionCommandMessage.Target();
        }

        visionCommandMessage.sortingIndex = sorting;
        setTimeout(1.0);
    }

    @Override
    public void start(){
        isFinished();
    }

    @Override
    public boolean isRunning(){
        return false;
    }

    @Override
    protected boolean isFinished() {
        visionCommandMessage.time = Timer.getFPGATimestamp();

        //Try to send the message for a second, otherwise timeout
        if(!VisionHandler.getInstance().isConnected()) return isTimedOut();

        VisionHandler.getInstance().sendMessage(visionCommandMessage);
        return true;
    }
}
