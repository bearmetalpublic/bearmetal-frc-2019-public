/*
 * Copyright 2019 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.vision;

import edu.wpi.first.wpilibj.Notifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tahomarobotics.robot.InitIF;

import java.io.IOException;
public class AdbBridge {

    private static Logger LOGGER = LoggerFactory.getLogger(AdbBridge.class);

    private static String MAIN_ACTIVITY_BEARVISION = "org.tahomarobotics.bear_vision/org.tahomarobotics.bear_vision.OpenCVCamera";
    private static String MAIN_ACTIVITY_CHEEZYDROID = "com.team254.cheezdroid/com.team254.cheezdroid.VisionTrackerActivity";
    private static double PERIOD = 1.0;

    private boolean connected, firstFailLog = true;

    private Notifier notifier = new Notifier(this::run);

    private static final AdbBridge instance = new AdbBridge();

    public static AdbBridge getInstance(){
        return instance;
    }

    public AdbBridge(){
        notifier.startPeriodic(PERIOD);
    }

    private boolean runCommand(String command) {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process process = runtime.exec("/usr/bin/adb " + command,
                    new String[]{"HOME=/home/lvuser"});

            process.waitFor();

            if (process.exitValue() != 0) {
                //LOGGER.error("ADB EXIT STATUS " + process.exitValue());
                return false;
            } else {
                return true;
            }

        } catch (IOException IOException) {
            LOGGER.error(IOException.toString());
        } catch (InterruptedException interruptedException) {
            LOGGER.error("Interrupted: " + interruptedException.toString());
        }

        //Code should not reach here
        return false;
    }

    public void restart(){
        connected = false;
    }

    private void run(){
        if(!connected && startApp()) {
            openPort();
            connected = true;
            VisionHandler.getInstance().adbGood();
        }

        if(VisionHandler.getInstance().wantAdbRestart()){
            connected = false;
        }
    }

    private boolean startApp(){
        if(!runCommand("shell am start " + MAIN_ACTIVITY_BEARVISION)){

            if(firstFailLog){
                LOGGER.info("Could not connect to phone");
                firstFailLog = false;
            }

            return false;
        } else {
            firstFailLog = true;
            LOGGER.info("Connected to phone");
            return true;
        }
    }

    private void openPort(){
        runCommand("reverse tcp:8254 tcp:8254");
    }
}
