package org.tahomarobotics.robot.vision;

import edu.wpi.first.wpilibj.command.Command;
import org.tahomarobotics.robot.path.ActionIF;

public class CamMode extends Command implements ActionIF {

	private final Limelight.VisionModes mode;

	public CamMode(Limelight.VisionModes mode){
		this.mode = mode;
	}
	@Override
	protected boolean isFinished() {
		Limelight.setVisionMode(mode);
		Limelight.drivingMode(false);
		return true;
	}
}
