package org.tahomarobotics.robot.vision;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class VisionHandler implements VisionStatusListener {

    private RoborioServer server = new RoborioServer("localhost", VisionConstants.PORT);

    private static final VisionHandler instance = new VisionHandler();

    public static VisionHandler getInstance(){
        return instance;
    }

    private VisionHandler(){
        server.addListener(this);
    }

    @Override
    public void handleMessage(VisionStatusMessage msg) {
        SmartDashboard.putNumberArray("VisionPose:dist,yaw", new double[] {msg.cameraPose.distance, msg.cameraPose.yaw});
        //TODO: Actually handle the message
    }

    public void adbGood() {
        server.adbNotConnect = false;
    }

    public boolean wantAdbRestart(){
        return server.adbNotConnect;
    }

    public void sendMessage(VisionCommandMessage msg){
        server.sendMessage(msg);
    }

    public void registerListener(VisionStatusListener listener){
        server.addListener(listener);
    }

    public boolean isConnected(){
        return server.connected;
    }
}
