package org.tahomarobotics.robot.vision;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.tahomarobotics.robot.path.PathBuilder;
import org.tahomarobotics.robot.path.Waypoint;

import java.security.InvalidParameterException;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class VisionConstants {

    public static final int PORT = 8254;

    public enum Target {
        ROCKET_CLOSE,
        ROCKET_CARGO,
        ROCKET_FAR,
        CARGOSHIP_FRONT,
        CARGOSHIP_CLOSE,
        CARGOSHIP_MIDDLE,
        CARGOSHIP_FAR,
        HATCH_PICKUP,
        ZERO
    }

    public static final int HUE_LOWER_BOUND = 0;
    public static final int HUE_UPPER_BOUND = 0;
    public static final int SATURATION_LOWER_BOUND = 0;
    public static final int SATURATION_UPPER_BOUND = 0;
    public static final int VALUE_LOWER_BOUND = 0;
    public static final int VALUE_UPPER_BOUND = 0;

    public static final long EXPOSURE = 0;

    //The target is the two pieces of tape on top of each goal.
    private static final double CLOSE_POINT_DISTANCE = 8.0; //Distance between the two closest points in the target
    private static final double CLOSE_POINT_HEIGHT = 31.121;

    private static final double FAR_POINT_DISTANCE = 14.623; //Distance between the two farthest points in the target
    private static final double FAR_POINT_HEIGHT = 26.296;

    private static final double TOP_POINT_DISTANCE = 11.873; //Distance between the top two points in the target;
    private static final double TOP_POINT_HEIGHT = 31.621;

    private static final double BOTTOM_POINT_DISTANCE = 10.750; //Distance between the bottom two points in the target.
    private static final double BOTTOM_POINT_HEIGHT = 25.796;

    private static final double ROCKET_HEIGHT_DIFFERENCE = 7.733; // Difference in height between the hatches and the Cargo target.


    private static final double BLUE_CLOSE_ROCKET_X = 214.428;
    private static final double BLUE_FAR_ROCKET_X = 243.696;
    private static final double BLUE_CLOSE_FAR_ROCKET_Y = 18.127;

    private static final double BLUE_MIDDLE_ROCKET_X = 229.062;
    private static final double BLUE_MIDDLE_ROCKET_Y = 27.463;

    private static final double FRONT_CARGOSHIP_X = 220.187;
    private static final double FRONT_CARGOSHIP_Y = 150.146;

    private static final double ANGLE_FOR_ROCKET_SIDE = Math.toRadians(118.77);

    private static final double CLOSE_SIDE_CARGOHSIP_X = 260.687;
    private static final double MIDDLE_SIDE_CARGOSHIP_X = 282.437;
    private static final double FAR_SIDE_CARGOSHIP_X = 304.187;
    private static final double SIDE_CARGOSHIP_Y = 133.146;
    private static final double HATCH_PICKUP_Y = 25.737;

    /**
     * Return the array of 3d points
     * @param target
     * @param mirror
     * @return
     */
    public static VisionCommandMessage.Target[] getPhonePoint(Target target, PathBuilder.Mirror mirror) {
        VisionCommandMessage.Target[] waypoints = new VisionCommandMessage.Target[8];

        for(int i = 0; i < 8; i++){
            waypoints[i] = new VisionCommandMessage.Target();
        }

        Waypoint point = getPoint(target, mirror);

        double angle = 0;

        if (target == Target.CARGOSHIP_FRONT)
            angle = (mirror == PathBuilder.Mirror.X || mirror == PathBuilder.Mirror.Both) ? 180 : 0;
        if (target == Target.CARGOSHIP_CLOSE || target == Target.CARGOSHIP_MIDDLE || target == Target.CARGOSHIP_FAR)
            angle = (mirror == PathBuilder.Mirror.Y || mirror == PathBuilder.Mirror.Both) ? 270 : 90;
        if (target == Target.ROCKET_CARGO)
            angle = (mirror == PathBuilder.Mirror.Y || mirror == PathBuilder.Mirror.Both) ? 90 : 270;
        if (target == Target.ROCKET_CLOSE)
            switch (mirror) {
                case Both: angle = -(180 - ANGLE_FOR_ROCKET_SIDE); break;
                case Y: angle = -ANGLE_FOR_ROCKET_SIDE; break;
                case X: angle = ANGLE_FOR_ROCKET_SIDE; break;
                case None: angle = 180 - ANGLE_FOR_ROCKET_SIDE; break;
            }
        if(target == Target.ROCKET_FAR)
            switch(mirror){
                case Both: angle = -ANGLE_FOR_ROCKET_SIDE; break;
                case Y: angle = -(180 - ANGLE_FOR_ROCKET_SIDE); break;
                case X: angle = ANGLE_FOR_ROCKET_SIDE; break;
                case None: angle = 180 - ANGLE_FOR_ROCKET_SIDE; break;
            }
        if(target == Target.ROCKET_CARGO)
            angle = (mirror == PathBuilder.Mirror.Y) ? 90 : 270;

        angle = Math.toRadians(angle);
        double cos = cos(angle);
        double sin = sin(angle);

        waypoints[0].x = point.x - cos * (CLOSE_POINT_DISTANCE / 2);
        waypoints[0].y = CLOSE_POINT_HEIGHT;
        waypoints[0].z = point.y - sin * (CLOSE_POINT_DISTANCE / 2);

        waypoints[1].x = point.x - cos * (TOP_POINT_DISTANCE / 2);
        waypoints[1].y = TOP_POINT_HEIGHT;
        waypoints[1].z = point.y - sin * (TOP_POINT_DISTANCE / 2);

        waypoints[2].x = point.x - cos * (FAR_POINT_DISTANCE / 2);
        waypoints[2].y = FAR_POINT_HEIGHT;
        waypoints[2].z = point.y - sin * (FAR_POINT_DISTANCE / 2);

        waypoints[3].x = point.x - cos * (BOTTOM_POINT_DISTANCE / 2);
        waypoints[3].y = BOTTOM_POINT_HEIGHT;
        waypoints[3].z = point.y - sin * (BOTTOM_POINT_DISTANCE / 2);

        waypoints[4].x = point.x + cos * (CLOSE_POINT_DISTANCE / 2);
        waypoints[4].y = CLOSE_POINT_HEIGHT;
        waypoints[4].z = point.y + sin * (CLOSE_POINT_DISTANCE / 2);

        waypoints[5].x = point.x + cos * (TOP_POINT_DISTANCE / 2);
        waypoints[5].y = TOP_POINT_HEIGHT;
        waypoints[5].z = point.y + sin * (TOP_POINT_DISTANCE / 2);

        waypoints[6].x = point.x + cos * (FAR_POINT_DISTANCE / 2);
        waypoints[6].y = FAR_POINT_HEIGHT;
        waypoints[6].z = point.y + sin * (FAR_POINT_DISTANCE / 2);

        waypoints[7].x = point.x + cos * (BOTTOM_POINT_DISTANCE / 2);
        waypoints[7].y = BOTTOM_POINT_HEIGHT;
        waypoints[7].z = point.y + sin * (BOTTOM_POINT_DISTANCE / 2);


        if(target == Target.ROCKET_CARGO){
            for(VisionCommandMessage.Target arr : waypoints){
                arr.y += ROCKET_HEIGHT_DIFFERENCE;
            }
        }

        SmartDashboard.putNumberArray("VisionTarget", new double[] {point.x, point.y, angle});

        return waypoints;
    }

    public static Waypoint getPoint(Target target, PathBuilder.Mirror mirror){
        Waypoint point;

        switch(target){
            case ROCKET_CLOSE: point = new Waypoint(BLUE_CLOSE_ROCKET_X, BLUE_CLOSE_FAR_ROCKET_Y, 0); break;
            case ROCKET_CARGO: point = new Waypoint(BLUE_MIDDLE_ROCKET_X, BLUE_MIDDLE_ROCKET_Y, 0); break;
            case ROCKET_FAR: point = new Waypoint(BLUE_FAR_ROCKET_X, BLUE_CLOSE_FAR_ROCKET_Y, 0); break;
            case CARGOSHIP_FRONT: point = new Waypoint(FRONT_CARGOSHIP_X, FRONT_CARGOSHIP_Y, 0); break;
            case CARGOSHIP_CLOSE: point = new Waypoint(CLOSE_SIDE_CARGOHSIP_X, SIDE_CARGOSHIP_Y, 0); break;
            case CARGOSHIP_MIDDLE: point = new Waypoint(MIDDLE_SIDE_CARGOSHIP_X, SIDE_CARGOSHIP_Y, 0); break;
            case CARGOSHIP_FAR: point = new Waypoint(FAR_SIDE_CARGOSHIP_X, SIDE_CARGOSHIP_Y, 0); break;
            case HATCH_PICKUP: point = new Waypoint(0, HATCH_PICKUP_Y, 0); break;
            case ZERO: point = new Waypoint(0, 0, 0); break;
            default: throw new InvalidParameterException();
        }

        PathBuilder.mirrorPoint(point, mirror);
        return point;
    }


}
