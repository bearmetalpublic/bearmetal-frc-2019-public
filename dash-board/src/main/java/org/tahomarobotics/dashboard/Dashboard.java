/*
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.dashboard;

import edu.wpi.first.networktables.NetworkTableInstance;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

public class Dashboard extends Application {
    private static boolean robot = false;
    private static String ip = "127.0.0.1";
    private static String team = "2046";
    private double WINDOW_PERCENT = 0.80;
    private static final String WINDOW_POSITION_X = "Window_Position_X";
    private static final String WINDOW_POSITION_Y = "Window_Position_Y";
    private static final String WINDOW_WIDTH = "Window_Width";
    private static final String WINDOW_HEIGHT = "Window_Height";
    private static final String NODE_NAME = "Dash-board";

    @Override
    public void start(Stage primaryStage) {

        primaryStage.setTitle("Bear Scope");

        Preferences pref = Preferences.userRoot().node(NODE_NAME);
        Rectangle2D screenSize = Screen.getPrimary().getBounds();
        primaryStage.setWidth(pref.getDouble(WINDOW_WIDTH, screenSize.getWidth() * WINDOW_PERCENT));
        primaryStage.setHeight(pref.getDouble(WINDOW_HEIGHT,screenSize.getHeight() * WINDOW_PERCENT));
        primaryStage.setX(pref.getDouble(WINDOW_POSITION_X, (screenSize.getWidth() - primaryStage.getWidth())/2));
        primaryStage.setY(pref.getDouble(WINDOW_POSITION_Y,(screenSize.getHeight() - primaryStage.getHeight())/2));



        // setup Menu
        final Menu fileMenu = new Menu("File");
        MenuItem quitMenuItem = new MenuItem("Quit");
        quitMenuItem.setOnAction((event) -> System.exit(0));
        fileMenu.getItems().add(quitMenuItem);
        final Menu optionsMenu = new Menu("Options");
        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(fileMenu, optionsMenu);

        List<InstrumentView> views = new ArrayList<>();

        // Create tab pane and tabs
        final TabPane tabPane = new TabPane();
        InstrumentView view;

        view = new FieldViewContainer();
        views.add(view);
        tabPane.getTabs().add(new Tab("Field", view.getView()));

        view = new MotorView();
        views.add(view);
        tabPane.getTabs().add(new Tab("Motors", view.getView()));

        view = new ChassisView();
        views.add(view);
        tabPane.getTabs().add(new Tab("Chassis", view.getView()));

        view = new AlphaLiftView();
        views.add(view);
        tabPane.getTabs().add(new Tab("Alpha Lift", view.getView()));

        view = new BetaLiftView();
        views.add(view);
        tabPane.getTabs().add(new Tab("Beta Lift", view.getView()));

        view = new UtilityArm();
        views.add(view);
        tabPane.getTabs().add(new Tab("Utility Arm", view.getView()));

        view = new AlphaArm();
        views.add(view);
        tabPane.getTabs().add(new Tab("Alpha Arm", view.getView()));

        view = new DriverView();
        views.add(view);
        tabPane.getTabs().add(new Tab("Driver View", view.getView()));
        // combine and show
        VBox vBox = new VBox();
        vBox.getChildren().addAll(menuBar, tabPane);
        vBox.setFillWidth(true);
        VBox.setVgrow(tabPane, Priority.ALWAYS);
        HBox.setHgrow(tabPane, Priority.ALWAYS);
        Scene scene = new Scene(vBox);
        primaryStage.setScene(scene);
        primaryStage.show();

        new AnimationTimer() {
            public void handle(long currentNanoTime) {
                for(InstrumentView view : views) {
                    view.updateData(currentNanoTime);
                }
            }
        }.start();

        NetworkTableInstance.getDefault().startClient(ip);


        primaryStage.setOnCloseRequest((final WindowEvent event) -> {
            Preferences preferences = Preferences.userRoot().node(NODE_NAME);
            preferences.putDouble(WINDOW_POSITION_X, primaryStage.getX());
            preferences.putDouble(WINDOW_POSITION_Y, primaryStage.getY());
            preferences.putDouble(WINDOW_WIDTH, primaryStage.getWidth());
            preferences.putDouble(WINDOW_HEIGHT, primaryStage.getHeight());

            System.exit(0);
        });
    }

    public static void main(String[] args) {
        if(Boolean.getBoolean("simulated")){
            ip = "localhost";
        } else if(Boolean.getBoolean("usb")) {
            ip = System.getProperty("ip");
            if(ip == null){
                ip = "172.22.11.2";
            }
        }else{
            ip = System.getProperty("ip");
            if(ip == null) {
                team = System.getProperty("team");
                if (team == null) {
                    team = "2046";
                }
                while (team.length() < 4) {
                    team = "0" + team;
                }
                ip = "10." + team.substring(0, 2) + "." + team.substring(2) + ".2";
            }
        }
        launch(args);
    }
}
