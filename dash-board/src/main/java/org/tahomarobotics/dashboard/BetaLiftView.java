package org.tahomarobotics.dashboard;

import edu.wpi.first.networktables.*;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import org.tahomarobotics.robot.util.ChartData;

import java.util.function.Consumer;

public class BetaLiftView extends VBox implements InstrumentView, Consumer<EntryNotification> {

	private final NetworkTable table = NetworkTableInstance.getDefault().getTable("SmartDashboard");
	private final NetworkTableEntry chartDataEntry = table.getEntry("BetaLiftChartData");

	private final XYChartPane chartPane = new XYChartPane();

	public BetaLiftView() {

		getChildren().add(chartPane.toolBar);
		getChildren().add(chartPane);
		setFillWidth(true);

		VBox.setVgrow(chartPane, Priority.ALWAYS);

		chartDataEntry.addListener(this, EntryListenerFlags.kNew | EntryListenerFlags.kUpdate);

		final EventHandler<KeyEvent> keyEventHandler = new EventHandler<KeyEvent>() {
			public void handle(final KeyEvent keyEvent) {
				System.out.println(keyEvent);
				if (keyEvent.getCode() == KeyCode.A) {
					System.out.print("A->");

				} else if (keyEvent.getCode() == KeyCode.S) {

					System.out.print("A->");
				}
			}
		};

		addEventHandler(KeyEvent.KEY_PRESSED, keyEventHandler);

	}

	@Override
	public void accept(EntryNotification t) {
		byte[] rawData = t.getEntry().getRaw(null);
		ChartData chartData = ChartData.deserialize(rawData);
		chartPane.update(chartData);
	}

	@Override
	public Node getView() {
		return this;
	}

	@Override
	public void updateData(long currentNanoTime) {
	}

}
