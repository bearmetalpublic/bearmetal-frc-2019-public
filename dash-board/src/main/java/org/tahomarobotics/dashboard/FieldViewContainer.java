package org.tahomarobotics.dashboard;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class FieldViewContainer extends VBox implements InstrumentView{
    private FieldView fieldView = new FieldView();
    private SelectionView view = new SelectionView("PathChooser", "Position");
    private Button slugTrailReset = new Button("Reset Slug Trail");
    private ToggleButton slugTrailToggle = new ToggleButton("Slug Trail");
    private HBox hBox = new HBox();

    public FieldViewContainer(){
        hBox.getChildren().add(view);
        hBox.setSpacing(5);

        slugTrailReset.setOnAction(new EventHandler<javafx.event.ActionEvent>(){
            @Override
            public void handle(ActionEvent actionEvent) {
                fieldView.resetSlugTrail();
            }
        });

        VBox vBox = new VBox(slugTrailReset);
        vBox.setAlignment(Pos.CENTER);
        hBox.getChildren().add(vBox);

        slugTrailToggle.setSelected(true);
        slugTrailToggle.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                fieldView.setSlugTrail(slugTrailToggle.isSelected());
            }
        });

        VBox vBox2 = new VBox(slugTrailToggle);
        vBox2.setAlignment(Pos.CENTER);
        hBox.getChildren().add(vBox2);

        getChildren().add(hBox);
        getChildren().add(fieldView);
        Platform.runLater(() -> {
            Rectangle clip = new Rectangle(Integer.MAX_VALUE, Integer.MAX_VALUE);
            clip.setLayoutX(fieldView.getLayoutX());
            clip.setLayoutY(fieldView.getTranslateY());
            fieldView.setClip(clip);
        });
    }

    @Override
    public Node getView() {
        return this;
    }

    @Override
    public void updateData(long currentNanoTime) {
        this.autosize();
        fieldView.updateData(currentNanoTime);
    }
}
