package org.tahomarobotics.dashboard;

import edu.wpi.first.networktables.*;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

import java.util.HashMap;
import java.util.function.Consumer;

public class DriverView extends VBox implements InstrumentView, Consumer<EntryNotification> {

	private final NetworkTable table = NetworkTableInstance.getDefault().getTable("SmartDashboard");
	private final NetworkTableEntry entry = table.getEntry("DriverView");
	private HashMap<String,String> strings = new HashMap<>();
	private GridPane area = new GridPane();

	public DriverView(){
		entry.addListener(this, EntryListenerFlags.kNew | EntryListenerFlags.kUpdate);
	}
	public Node getView(){
		return this;
	}

	public void updateData(long currentNanoTime){

	}
	public void accept(EntryNotification data){
		String dat = data.getEntry().getString("");
		if(dat.equals("")) return;
		String[] splitted = dat.split(":");
		try{
			strings.put(splitted[0], splitted[1]);
			updateView();
		}catch(IndexOutOfBoundsException e){
			System.out.println("[WARN] Bad data passed into DriverView");
		}
	}

	private void updateView() {
		Platform.runLater(()->{
			area = new GridPane();
			int i = 0;
			for(String key : strings.keySet()){
				String value = strings.get(key);

				Text keyText = new Text(key);
				keyText.setTextAlignment(TextAlignment.CENTER);
				keyText.setTranslateY(75);
				keyText.setTranslateX(10);
				VBox vBox = new VBox();
				vBox.setPrefSize(150, 150);
				vBox.setMinSize(150, 150);
				vBox.setMaxSize(200, 200);
				keyText.setStroke(Color.WHITE);
				if(value.equals("normal")){
					Background background = new Background(new BackgroundFill(Color.GREEN, CornerRadii.EMPTY, Insets.EMPTY));
					vBox.setBackground(background);
				} else {
					Background background = new Background(new BackgroundFill(Color.RED, CornerRadii.EMPTY, Insets.EMPTY));
					vBox.setBackground(background);
				}
				vBox.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
				vBox.getChildren().add(keyText);
				area.addColumn(i++, vBox);
			}


			this.getChildren().setAll(area);
		});
	}
}
