/*
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.dashboard;

import edu.wpi.first.networktables.*;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.function.Consumer;

public class MotorView extends VBox implements InstrumentView, Consumer<EntryNotification> {

	private final NetworkTable table = NetworkTableInstance.getDefault().getTable("SmartDashboard");
	private final NetworkTableEntry chartDataEntry = table.getEntry("MotorData");
	private final ArrayList<ArrayList<ArrayList<String>>>  logArray = new ArrayList<>();
	private final ArrayList<ArrayList<String>> sysArray = new ArrayList<>();
	private final ArrayList<String> nameArray = new ArrayList<>();
	//time in ms
	private double time = 0;
	private double lastTime = 0;
	private String currentGraph = "Chassis";
	private final MotorChart chartPane = new MotorChart();

	public MotorView() {

		getChildren().add(chartPane.toolBar);
		getChildren().add(chartPane);
		setFillWidth(true);

		VBox.setVgrow(chartPane, Priority.ALWAYS);

		chartDataEntry.addListener(this, EntryListenerFlags.kNew | EntryListenerFlags.kUpdate);

	}

	@Override
	public void accept(EntryNotification t) {
		String inputString = t.getEntry().getString("");
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				String[] inputArray = inputString.split(", ");
				time = Double.parseDouble(inputArray[0]);
				//if(time - lastTime >= 500){
					sysArray.clear();
					nameArray.clear();
					chartPane.toolBar.getItems().clear();
					for(int i = 1; i < inputArray.length; i++){
						String[] subString = inputArray[i].split("; ");
						sysArray.add(new ArrayList<>(Arrays.asList(subString)));
						nameArray.add(subString[0]);
						Button button = new Button(subString[0]);
						button.setOnAction(new EventHandler<ActionEvent>(){
							private final String name = subString[0];
							@Override
							public void handle(ActionEvent actionEvent) {
								currentGraph = name;
								chartPane.graphChart(name);
							}
						});
						chartPane.toolBar.getItems().add(button);
					}
					sysArray.add(0, new ArrayList<String>(Arrays.asList(new String[]{Double.toString(time)})));
					if(logArray.size() == 120){
						logArray.remove(0);
					}
					logArray.add((ArrayList<ArrayList<String>>) sysArray.clone());
					chartPane.graphChart(currentGraph);
				//}
				lastTime = time;
			}
		});

	}

	@Override
	public Node getView() {
		return this;
	}

	@Override
	public void updateData(long currentNanoTime) {
	}

	private class MotorChart extends AnchorPane implements InstrumentView{
		private final NumberAxis xAxis = new NumberAxis();
		private final NumberAxis yAxis = new NumberAxis();
		private final LineChart<Number, Number> chart = new LineChart<>(xAxis, yAxis);
		private final Text text = new Text();
		public final ToolBar toolBar;

		private MotorChart(){
			chart.setCreateSymbols(false);
			chart.setAnimated(false);
			xAxis.setLabel("Time (0.5 secs)");
			yAxis.setLabel("Amp/Temp");
			getChildren().add(chart);

			toolBar = new ToolBar();

			AnchorPane.setTopAnchor(chart, 0.0);
			AnchorPane.setLeftAnchor(chart, 0.0);
			AnchorPane.setRightAnchor(chart, 0.0);
			AnchorPane.setBottomAnchor(chart, 0.0);
		}

		@Override
		public Node getView() {
			return this;
		}

		@Override
		public void updateData(long currentNanoTime) {
		}

		public void graphChart(String name){
			chart.setTitle(name);
			int graphIndex = nameArray.indexOf(name) + 1;
			ArrayList<ObservableList<XYChart.Data<Number, Number>>> motorData = new ArrayList<>();
			for(int i = 1; i < logArray.get(logArray.size() - 1).get(graphIndex).size() * 2 + 2; i++){
				motorData.add(FXCollections.<XYChart.Data<Number, Number>>observableArrayList());
			}
			for(int i = 1; i < logArray.size(); i++){
				try {
					ArrayList<String> subData = logArray.get(i).get(graphIndex);
					double time = Double.parseDouble(logArray.get(i).get(0).get(0)) / 1000;
					for (int j = 2; j < motorData.size() - 1; j += 2) {
						String[] data = subData.get((int) (j / 2)).split("<>");
						motorData.get(j).add(new XYChart.Data<Number, Number>(i / 2, Double.parseDouble(data[0])));
						motorData.get(j + 1).add(new XYChart.Data<Number, Number>(i / 2, Double.parseDouble(data[1])));
					}
				} catch(IndexOutOfBoundsException e){
					for (int j = 2; j < motorData.size() - 1; j += 2) {
						motorData.get(j).add(new XYChart.Data<Number, Number>(i / 2, 0.0));
						motorData.get(j + 1).add(new XYChart.Data<Number, Number>(i / 2, 0.0));
					}
				}
			}
			chart.getData().clear();
			for(int i = 2; i < motorData.size() - 1; i+=2){
				XYChart.Series<Number, Number> series = new XYChart.Series<Number, Number>("Motor " + (i/2) + " current", motorData.get(i));
				chart.getData().add(series);
				series = new XYChart.Series<Number, Number>("Motor " + (i/2) + " temp", motorData.get(i + 1));
				chart.getData().add(series);
			}
		}
	}
}
