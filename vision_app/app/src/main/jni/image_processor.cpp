#include "image_processor.h"

#include <algorithm>
#include <stdlib.h>

#include <GLES2/gl2.h>
#include <EGL/egl.h>

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/core/ocl.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include "common.hpp"

enum DisplayMode {
	DISP_MODE_RAW = 0,
	DISP_MODE_THRESH = 1,
	DISP_MODE_TARGETS = 2,
	DISP_MODE_TARGETS_PLUS = 3
};

struct TargetInfo {
	double yaw, roll, pitch, x, y, z;

	TargetInfo(double yaw, double roll, double pitch, double x, double y, double z) {
		this->yaw = yaw;
		this->roll = roll;
		this->pitch = pitch;
		this->x = x;
		this->y = y;
		this->z = z;
	}

	TargetInfo() {
	}
};

static cv::Mat camera_matrix = (cv::Mat_<double>(3, 3) << 480.256f , 0, 640.0/2.0,
														0, 484.12903225806451612903225806452f, 480/2,
														0, 0, 1);
static cv::Mat dist_coeffs = (cv::Mat_<double>(5, 1) << 0.02133937, -0.91491545, -0.00700398, 0.00930445, 1.52638541); // Assuming no lens 


std::vector<std::vector<cv::Point>> contours;
std::vector<cv::Point> convex_contour;
std::vector<TargetInfo> accepted_targets;
TargetInfo targets;
std::vector<TargetInfo> rejected_targets;

static cv::Mat hsv;
static cv::Mat thresh;
static cv::Mat contour_input;
static cv::Mat input;
static cv::Mat output;
TargetInfo processImpl(int w, int h, int texOut, DisplayMode mode,
                                    int h_min, int h_max, int s_min, int s_max,
                                    int v_min, int v_max) {
	//LOGD("Image is %d x %d", w, h);
	//LOGD("H %d-%d S %d-%d V %d-%d", h_min, h_max, s_min, s_max, v_min, v_max);
	int64_t t;

	input.create(h, w, CV_8UC4);

	// read
	t = getTimeMs();
	glReadPixels(0, 0, w, h, GL_RGBA, GL_UNSIGNED_BYTE, input.data);
	//LOGD("glReadPixels() costs %d ms", getTimeInterval(t));

	// modify
	t = getTimeMs();
	cv::cvtColor(input, hsv, CV_RGBA2RGB);
	cv::cvtColor(hsv, hsv, CV_RGB2HSV);
	//LOGD("cvtColor() costs %d ms", getTimeInterval(t));

	t = getTimeMs();
	cv::inRange(hsv, cv::Scalar(h_min, s_min, v_min),
				cv::Scalar(h_max, s_max, v_max), thresh);
	//LOGD("inRange() costs %d ms", getTimeInterval(t));

	t = getTimeMs();
	contour_input = thresh.clone();

	contours.clear();
	accepted_targets.clear();
	rejected_targets.clear();

	cv::findContours(contour_input, contours, cv::RETR_EXTERNAL,
					cv::CHAIN_APPROX_NONE);

	std::vector<std::vector<cv::Point>> blah = std::vector<std::vector<cv::Point>>();

	cv::drawContours(input, contours, -1, cv::Scalar(255, 0, 0));

	int i = 1;
	for (auto &contour : contours) {
		convex_contour.clear();

		//double epsilon = 0.03 * cv::arcLength(contour, true);
		//cv::approxPolyDP(contour, output, epsilon, true);

		cv::Point bottom = contour[0];
		cv::Point top = contour[0];
		cv::Point left = contour[0];
		cv::Point right = contour[0];

		for (size_t cP = 0; cP < contour.size(); cP++) {
			cv::Point p = contour[cP];

			if (p.y > top.y) top = p;
			if (p.y < bottom.y) bottom = p;
			if (p.x > right.x) right = p;
			if (p.x < left.x) left = p;
		}

		LOGD("Point BOTTOM x: %d y: %d", bottom.x, bottom.y);
		LOGD("Point 2 x: %d y: %d", left.x, left.y);
		LOGD("Point TOP x: %d y: %d", top.x, top.y);
		LOGD("Point 4 x: %d y: %d", right.x, right.y);

		cv::drawMarker(input, bottom, cv::Scalar(0, 255, 0));
		cv::drawMarker(input, top, cv::Scalar(0, 255, 0));
		cv::drawMarker(input, left, cv::Scalar(0, 255, 0));
		cv::drawMarker(input, right, cv::Scalar(0, 255, 0));

		std::vector<cv::Point> vectorA = std::vector<cv::Point>();



		vectorA.push_back(bottom);
		//vectorA.push_back(left);
		vectorA.push_back(top);
		//vectorA.push_back(right);

		blah.push_back(vectorA);

		//cv::drawContours(input, cv::InputArrayOfArrays(output), -1, cv::Scalar(0, 255, 0));
		LOGD("Contour analysis costs %d ms", getTimeInterval(t));
		i++;
	}

	t = getTimeMs();
	//cv::drawContours(input, contours, 0, cv::Scalar(0, 0, 255), 2

	bool leftFirst = false;

	cv::Mat rvec;
	cv::Mat tvec;

	TargetInfo target = TargetInfo(0, 0, 0, 0, 0, 0);

	if (blah.size() == 2) {

		if (blah.at(0).at(0).x < blah.at(1).at(0).x) leftFirst = true;

		if (!leftFirst) std::swap(blah.at(1), blah.at(0));

		std::vector<cv::Point2d> image_points = std::vector<cv::Point2d>();

		for (std::vector<cv::Point> &yoo : blah) {
			for (cv::Point &point : yoo) {
				image_points.push_back(cv::Point2d(point.x, point.y));
			}
		}

		std::vector<cv::Point3f> model_points = std::vector<cv::Point3f>();

		model_points.push_back(cv::Point3f(-5.9375f, 31.5625f, 0.0f));
		//model_points.push_back(cv::Point3d(-4.0f, 31.0f, 0.0f));
		model_points.push_back(cv::Point3f(-5.625f, 25.9375f, 0.0f));
		// model_points.push_back(cv::Point3d(-7.5f, 26.5f, 0.0f));
		model_points.push_back(cv::Point3f(5.9375f, 31.375f, 0.0f));
		//model_points.push_back(cv::Point3d(7.5f, 26.5f, 0.0f));
		model_points.push_back(cv::Point3f(5.625f, 25.75f, 0.0f));
		//model_points.push_back(cv::Point3d(4.0f, 31.0f, 0.0f))

		if (image_points.size() == 4) {
			cv::solvePnPRansac(model_points, image_points, camera_matrix, dist_coeffs, rvec, tvec);

			LOGD("ROLL: %f, PITCH %f, YAW: %f", rvec.at<double>(0, 0), rvec.at<double>(1, 0), rvec.at<double>(2, 0));
			LOGD("X: %f, Y: %f, Z: %f", tvec.at<double>(0, 0), tvec.at<double>(1, 0), tvec.at<double>(2, 0));
			LOGD("solvePnP costs %d ms", getTimeInterval(t));

			//Roll Pitch Yaw X Y Z -> 
			//Yaw Roll Pitch X Y Z
			target = TargetInfo(rvec.at<double>(2, 0), rvec.at<double>(0, 0), rvec.at<double>(1, 0),
				tvec.at<double>(0, 0), tvec.at<double>(1, 0), tvec.at<double>(2, 0));
		}
	}

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texOut);
	t = getTimeMs();
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, w, h, GL_RGBA, GL_UNSIGNED_BYTE,
					input.data);
	return target;
}
static bool sFieldsRegistered = false;

static jfieldID yaw, roll, pitch, x, y, z;

static void ensureJniRegistered(JNIEnv *env) {
	if (sFieldsRegistered) {
		return;
	}
	sFieldsRegistered = true;
	jclass targetsInfoClass =
		env->FindClass("com/team254/cheezdroid/NativePart$TargetsInfo");
  
	yaw = env->GetFieldID(targetsInfoClass, "yaw", "D");
	roll = env->GetFieldID(targetsInfoClass, "roll", "D");
	pitch = env->GetFieldID(targetsInfoClass, "pitch", "D");
	x = env->GetFieldID(targetsInfoClass, "x", "D");
	y = env->GetFieldID(targetsInfoClass, "y", "D");
	z = env->GetFieldID(targetsInfoClass, "z", "D");
}

extern "C" void processFrame(JNIEnv *env, int tex1, int tex2, int w, int h,
                             int mode, int h_min, int h_max, int s_min,
                             int s_max, int v_min, int v_max,
                             jobject destTargetInfo) {
  
	TargetInfo targets = processImpl(w, h, tex2, static_cast<DisplayMode>(mode), h_min,
                             h_max, s_min, s_max, v_min, v_max);
	ensureJniRegistered(env);
  
	if (targets.z == 0) {
		env->SetDoubleField(destTargetInfo, yaw, 0.0);
		env->SetDoubleField(destTargetInfo, roll, 0.0);
		env->SetDoubleField(destTargetInfo, pitch, 0.0);
		env->SetDoubleField(destTargetInfo, x, 0.0);
		env->SetDoubleField(destTargetInfo, y, 0.0);
		env->SetDoubleField(destTargetInfo, z, 0.0);
	}
	else {
		env->SetDoubleField(destTargetInfo, yaw, targets.yaw);
		env->SetDoubleField(destTargetInfo, roll, targets.roll);
		env->SetDoubleField(destTargetInfo, pitch, targets.pitch);
		env->SetDoubleField(destTargetInfo, x, targets.x);
		env->SetDoubleField(destTargetInfo, y, targets.y);
		env->SetDoubleField(destTargetInfo, z, targets.z);
	}

}
