package com.team254.cheezdroid.comm;

import android.util.Log;

import com.team254.cheezdroid.NativePart;

import org.json.JSONException;
import org.json.JSONObject;

public class CameraLocationInfo {
    protected com.team254.cheezdroid.NativePart.TargetsInfo info;

    public CameraLocationInfo(NativePart.TargetsInfo info) {
        this.info = info;
    }

    public JSONObject toJson() {
        JSONObject j = new JSONObject();
        try {
            j.put("y", info.y);
            j.put("z", info.z);
            j.put("x", info.x);
            j.put("roll", info.roll);
            j.put("pitch", info.pitch);
            j.put("yaw", info.yaw);
        } catch (JSONException e) {
            Log.e("CameraLocationInfo", "Could not encode Json");
        }
        return j;
    }
}