package com.team254.cheezdroid.comm;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class VisionUpdate {
    protected CameraLocationInfo cameraLoc;
    protected long m_captured = 0;

    public VisionUpdate(long capturedAtTimestamp, CameraLocationInfo cameraLoc) {
        m_captured = capturedAtTimestamp;
        this.cameraLoc = cameraLoc;
    }

    public String getSendableJsonString(long timestamp) {
        long captured_ago = (timestamp - m_captured) / 1000000L;  // nanos to millis
        JSONObject j = new JSONObject();
        try {
            j.put("capturedAgoMs", captured_ago);
            j.put("locInfo", cameraLoc.toJson());
//            JSONArray arr = new JSONArray();
//            for (CameraLocationInfo t : m_targets) {
//                if (t != null) {
//                    arr.put(t.toJson());
//                }
//            }
//            j.put("targets", arr);
        } catch (JSONException e) {
            Log.e("VisionUpdate", "Could not encode JSON");
        }

        return j.toString();
    }
}
