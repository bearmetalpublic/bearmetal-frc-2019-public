package org.tahomarobotics.robot.vision;

public interface VisionCommandListener {
    void handleMessage(VisionCommandMessage msg);

}
