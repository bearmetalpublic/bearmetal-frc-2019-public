/*
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.vision;

import java.nio.ByteBuffer;

/**
 * Status message to be sent from the Camera Phone (Bear-Vision) to the roboRio (Robot)
 */
public class VisionStatusMessage {

    public enum CalibrationMode {
        UNCALIBRATED, CALIBRATING, CALIBRATED;
    }

    public class CameraPose {
        public double yaw;
        public double rmsReproductionError;
        public double distance;
        public double centroidX;
        public double centroidY;
    }
    // time of command in seconds
    public double time;

    // command request Id which has been processed
    public int requestId;

    // calibration mode
    public CalibrationMode calibrationMode = CalibrationMode.UNCALIBRATED;

    // indicates that the camera created a valid camera pose from the target
    public boolean cameraPoseValidity;

    public final CameraPose cameraPose = new CameraPose();

    //A stack of bytes, huh
    public static final int MESSAGE_BYTE_SIZE = 10 * 8;

    /**
     * Marshall the provided VisionStatusMessage into a ByteBuffer buffer to be sent to the Robot application.
     *
     * @param outputBuffer - provided ByteBuffer to marshall into
     * @param msg          - provided VisionStatusMessage to marshall to the buffer
     */
    public static void marshall(ByteBuffer outputBuffer, VisionStatusMessage msg) {

        outputBuffer.putDouble(msg.time);

        // 8 byte alignment (padded out for performance)
        outputBuffer.putInt(msg.requestId);
        outputBuffer.put((byte) msg.calibrationMode.ordinal());
        outputBuffer.put((byte) (msg.cameraPoseValidity ? 1 : 0));
        outputBuffer.put((byte) 0); // padding
        outputBuffer.put((byte) 0); // padding
        outputBuffer.putDouble(msg.cameraPose.yaw);
        outputBuffer.putDouble(msg.cameraPose.distance);
        outputBuffer.putDouble(msg.cameraPose.centroidX);
        outputBuffer.putDouble(msg.cameraPose.centroidY);
        outputBuffer.putDouble(msg.cameraPose.rmsReproductionError);
    }


    /**
     * Unmarshall a message's byte buffer into the provided VisionStatusMessage
     *
     * @param inputBuffer - ByteBuffer buffer received from the Camera
     * @param msg         - VisionStatusMessage to be update with message content
     */
    public static void unmarshall(ByteBuffer inputBuffer, VisionStatusMessage msg) {

        msg.time = inputBuffer.getDouble();

        // 8 byte alignment (padded out for performance)
        msg.requestId = inputBuffer.getInt();
        msg.calibrationMode = CalibrationMode.values()[inputBuffer.get()];
        msg.cameraPoseValidity = inputBuffer.get() == (byte) 1;
        inputBuffer.get(); // padding
        inputBuffer.get(); // padding

        msg.cameraPose.yaw = inputBuffer.getDouble();
        msg.cameraPose.distance = inputBuffer.getDouble();
        msg.cameraPose.centroidX = inputBuffer.getDouble();
        msg.cameraPose.centroidY = inputBuffer.getDouble();
        msg.cameraPose.rmsReproductionError = inputBuffer.getDouble();
    }
}