package org.tahomarobotics.robot.vision;

public interface VisionStatusListener {
    void handleMessage(VisionStatusMessage msg);
}
