package org.tahomarobotics.robot.vision;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public abstract class AbstractSocket {

    final String host;
    final int port;
    final int SYNC = 0x2046;
    volatile boolean connected = false;
    Socket socket;
    DataOutputStream sendStream = null;
    DataInputStream receiveStream = null;

    public AbstractSocket(String host, int port) {
        this.host = host;
        this.port = port;
    }

    protected void startThreads() {
        sendThread.start();
        receiveThread.start();
    }

    public boolean isConnected() {
        return connected;
    }

    abstract void reconnect();

    protected Thread receiveThread = new Thread(this::receiveThreadMethod);
    protected Thread sendThread = new Thread(this::sendThreadMethod);

    abstract void sendThreadMethod();
    abstract void receiveThreadMethod();

}
