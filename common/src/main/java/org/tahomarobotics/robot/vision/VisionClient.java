package org.tahomarobotics.robot.vision;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class VisionClient extends AbstractSocket {

    //private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private final BlockingQueue<VisionStatusMessage> sendQueue;
    private final ArrayList<VisionCommandListener> listeners;

    public VisionClient(String host, int port) {
        super(host, port);
        sendQueue = new LinkedBlockingQueue<>();
        listeners = new ArrayList<>();

        startThreads();
    }

    public void addListener(VisionCommandListener listener){
        listeners.add(listener);
    }

    public void sendMessage(VisionStatusMessage message){
        sendQueue.offer(message);
    }

    void sendThreadMethod() {
        ByteBuffer sendBuffer = ByteBuffer.allocate(300);

        while (true) {
            try {
                VisionStatusMessage message;
                message = sendQueue.poll(250, TimeUnit.MILLISECONDS);

                if (!connected || message == null) {
                    //LOGGER.error("Not connected, dropping message");
                    continue;
                }
                ((Buffer)sendBuffer).clear();
                sendBuffer.putInt(SYNC);
                sendBuffer.putInt(0); // Padding

                VisionStatusMessage.marshall(sendBuffer, message);
                int len = sendBuffer.position();
                byte dst[] = new byte[len];
                ((Buffer)sendBuffer).rewind();
                sendBuffer.get(dst);
                sendStream.write(dst, 0, len);
            } catch (InterruptedException | IOException e) {
                // do nothing
                e.getStackTrace();
            }
        }
    }

    void receiveThreadMethod(){
        byte buf[] = new byte[VisionCommandMessage.MESSAGE_BYTE_SIZE];
        ByteBuffer receiveBuffer = ByteBuffer.wrap(buf);
        int len = VisionCommandMessage.MESSAGE_BYTE_SIZE;
        VisionCommandMessage message = new VisionCommandMessage();

        while (true) {
            try {
                if (!connected) {
                    reconnect();
                }

                int sync = receiveStream.readInt();
                if (sync == SYNC) {
                    receiveStream.readInt(); //Padding

                    ((Buffer)receiveBuffer).clear();
                    receiveStream.readFully(buf, 0, len);
                    ((Buffer)receiveBuffer).limit(len);

                    VisionCommandMessage.unmarshall(receiveBuffer, message);

                    listeners.forEach(listener -> {
                       listener.handleMessage(message);
                    });

                }
            } catch (IOException e) {
                System.out.println("Connection lost w/EOF: " + e.getMessage());
                connected = false;
            }
        }
    }

    @Override
    void reconnect() {
        while (true) {
            connected = false;
            if (socket != null) {
                if (!socket.isClosed()) {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            try {
                socket = new Socket(host, port);
                socket.setTcpNoDelay(true);
                break;

            } catch (SocketException e1) {
                // do nothing
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }

        try {
            sendStream = new DataOutputStream(socket.getOutputStream());
            receiveStream = new DataInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        connected = true;

    }
}