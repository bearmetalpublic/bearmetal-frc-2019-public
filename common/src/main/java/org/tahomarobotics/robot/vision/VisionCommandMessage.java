/*
 * Copyright 2018 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
package org.tahomarobotics.robot.vision;

import java.nio.ByteBuffer;

/**
 * Command message to be sent from the roboRio (Robot) to the Camera Phone (Bear-Vision)
 */
public class VisionCommandMessage {

    public enum Sorting {
        LEFTMOST,
        CENTERMOST,
        RIGHTMOST,
        CLOSEST,
        FARTHEST,
        SCREEN_CENTER
    }

    static public class Target {
        public double x;
        public double y;
        public double z;
    }

    public class ImageProcessingAttributes {
        public int hueLowerBound;
        public int hueUpperBound;
        public int saturationLowerBound;
        public int saturationUpperBound;
        public int levelLowerBound;
        public int levelUpperBound;
        public long exposure;
    }

    // time of command in seconds
    public double time;

    // sequential request Id to indicate change
    // or match request with status response
    public int requestId;

    // used to initiate a calibration of the camera
    public boolean recalibrateCamera;

    // used to indicate that target processing should commence
    public boolean targetValid;

    // 0 = sort left, 1 = sort center, 2 = sort right
    public Sorting sortingIndex;

    // list of target to match
    public final Target[] targets = new Target[8];

    // image attributes to process raw image with
    public final ImageProcessingAttributes imageProcessingAttributes = new ImageProcessingAttributes();

    public static final int MESSAGE_BYTE_SIZE = 31 * 8;

    /**
     * Marshall the provided VisionCommandMessage into a ByteBuffer buffer to be sent to the Bear-Vision application.
     *
     * @param outputBuffer - provided ByteBuffer to marshall into
     * @param msg - provided VisionCommandMessage to marshall to the buffer
     */
    public static void marshall(ByteBuffer outputBuffer, VisionCommandMessage msg) {

        outputBuffer.putDouble(msg.time);

        // 8 byte alignment (padded out for performance)
        outputBuffer.putInt(msg.requestId);
        outputBuffer.put((byte)(msg.recalibrateCamera ? 1 : 0));
        outputBuffer.put((byte)(msg.targetValid ? 1 : 0));
        outputBuffer.put((byte)0); // padding
        outputBuffer.put((byte)0); // padding

        for (Target target : msg.targets) {
            outputBuffer.putDouble(target.x);
            outputBuffer.putDouble(target.y);
            outputBuffer.putDouble(target.z);
        }

        outputBuffer.putInt(msg.imageProcessingAttributes.hueLowerBound);
        outputBuffer.putInt(msg.imageProcessingAttributes.hueUpperBound);
        outputBuffer.putInt(msg.imageProcessingAttributes.saturationLowerBound);
        outputBuffer.putInt(msg.imageProcessingAttributes.saturationUpperBound);
        outputBuffer.putInt(msg.imageProcessingAttributes.levelLowerBound);
        outputBuffer.putInt(msg.imageProcessingAttributes.levelUpperBound);
        outputBuffer.putLong(msg.imageProcessingAttributes.exposure);

        outputBuffer.putInt(msg.sortingIndex.ordinal());
        outputBuffer.putInt(0); // padding

    }

    /**
     * Unmarshall a message's byte buffer into the provided VisionCommandMessage
     *
     * @param inputBuffer - ByteBuffer buffer received from the Robot
     * @param msg - VisionCommandMessage to be update with message content
     */
    public static void unmarshall(ByteBuffer inputBuffer, VisionCommandMessage msg) {

        msg.time = inputBuffer.getDouble();

        // 8 byte alignment (padded out for performance)
        msg.requestId =inputBuffer.getInt();
        msg.recalibrateCamera = inputBuffer.get() == (byte)1;
        msg.targetValid = inputBuffer.get() == (byte)1;
        inputBuffer.get(); // padding
        inputBuffer.get(); // padding

        for (Target target : msg.targets) {
            target = new Target();
            target.x = inputBuffer.getDouble();
            target.y = inputBuffer.getDouble();
            target.z = inputBuffer.getDouble();
        }

        msg.imageProcessingAttributes.hueLowerBound = inputBuffer.getInt();
        msg.imageProcessingAttributes.hueUpperBound = inputBuffer.getInt();
        msg.imageProcessingAttributes.saturationLowerBound = inputBuffer.getInt();
        msg.imageProcessingAttributes.saturationUpperBound = inputBuffer.getInt();
        msg.imageProcessingAttributes.levelLowerBound = inputBuffer.getInt();
        msg.imageProcessingAttributes.levelUpperBound = inputBuffer.getInt();
        msg.imageProcessingAttributes.exposure = inputBuffer.getLong();

        msg.sortingIndex = Sorting.values()[inputBuffer.getInt()];
        inputBuffer.get(); // padding
    }
}
