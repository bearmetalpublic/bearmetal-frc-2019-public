package org.tahomarobotics.robot.vision;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class RoborioServer extends AbstractSocket {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private final BlockingQueue<VisionCommandMessage> sendQueue;
    private final ArrayList<VisionStatusListener> listeners;
    private ServerSocket serverSocket;
    public boolean adbNotConnect = true;

    public RoborioServer(String host, int port) {
        super(host, port);
        sendQueue = new LinkedBlockingQueue<>();
        listeners = new ArrayList<>();
        startThreads();
    }

    public void addListener(VisionStatusListener listener){
        listeners.add(listener);
    }

    public void sendMessage(VisionCommandMessage message){
        sendQueue.offer(message);
    }

    protected void sendThreadMethod() {
        //Size of the message plus the sync byte
        ByteBuffer sendBuffer = ByteBuffer.allocate(VisionCommandMessage.MESSAGE_BYTE_SIZE + 8);

        while (true) {
            try {

                VisionCommandMessage message;

                if(LOGGER == null){
                    LOGGER.info("SendQueue is nulL!");
                    continue;
                }
                message = sendQueue.poll(250, TimeUnit.MILLISECONDS);

                if(message == null) continue;

                if (!connected) {
                    LOGGER.error("Not connected, dropping message");
                    continue;
                }
                sendBuffer.clear();
                sendBuffer.putInt(SYNC);
                sendBuffer.putInt(0); // Padding
                VisionCommandMessage.marshall(sendBuffer, message);
                int len = sendBuffer.position();
                byte dst[] = new byte[len];
                sendBuffer.rewind();
                sendBuffer.get(dst);
                sendStream.write(dst, 0, len);
            } catch (InterruptedException | IOException e) {
                // do nothing
            }
        }
    }

    protected void receiveThreadMethod() {
        byte buf[] = new byte[VisionStatusMessage.MESSAGE_BYTE_SIZE];
        ByteBuffer receiveBuffer = ByteBuffer.wrap(buf);
        int len = VisionStatusMessage.MESSAGE_BYTE_SIZE;
        VisionStatusMessage recieveMessage = new VisionStatusMessage();

        while (true) {
            try {
                if (!connected) {
                    reconnect();
                }


                int sync = receiveStream.readInt();
                if (sync == SYNC) {

                    receiveStream.readInt(); //Padding

                    receiveBuffer.clear();
                    receiveStream.readFully(buf, 0, len);
                    receiveBuffer.limit(len);

                    VisionStatusMessage.unmarshall(receiveBuffer, recieveMessage);

                    listeners.forEach(listener -> {
                       listener.handleMessage(recieveMessage);
                    });

                }
            } catch (IOException e) {
                System.out.println("Connection lost w/EOF: " + e.getMessage());
                connected = false;
                adbNotConnect = true;
            }
        }
    }

    protected void reconnect() {
        connected = false;

        if (serverSocket == null || serverSocket.isClosed()) {
            try {
                serverSocket = new ServerSocket(port, 1);
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }

        if (socket != null) {
            if (!socket.isClosed()) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        try {
            while(adbNotConnect) {
                //stall
            }

            LOGGER.info("Trying to connect");

            socket = serverSocket.accept();
            socket.setTcpNoDelay(true);
            sendStream = new DataOutputStream(socket.getOutputStream());
            receiveStream = new DataInputStream(socket.getInputStream());
            LOGGER.info("Phone Connected");
            connected = true;
            return;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}